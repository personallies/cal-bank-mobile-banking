package com.itconsortiumgh.cal.redis.queue;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.listener.ChannelTopic;
import org.springframework.stereotype.Component;

import com.itconsortiumgh.cal.model.UssdSession;

@Component
public class BalancePublisher implements MessagePublisher {
	@Autowired
	private RedisTemplate<String, Object> redisTemplate;
	@Autowired
	private ChannelTopic balanceTopic;

	public BalancePublisher() {
	}

	public BalancePublisher(final RedisTemplate<String, Object> redisTemplate,
			final ChannelTopic topic) {
		this.redisTemplate = redisTemplate;
		this.balanceTopic = topic;
	}

	public void publish(final String message) {
		redisTemplate.convertAndSend(balanceTopic.getTopic(), message);
	}

}
