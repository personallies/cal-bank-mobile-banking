package com.itconsortiumgh.cal.redis.queue;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.connection.MessageListener;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.itconsortiumgh.cal.model.Action;
import com.itconsortiumgh.cal.model.BankToOwnWalletRequest;
import com.itconsortiumgh.cal.model.MDRPResponse;
import com.itconsortiumgh.cal.model.RequestPaymentRequestSdp;
import com.itconsortiumgh.cal.model.ResponseCode;
import com.itconsortiumgh.cal.model.TransactionResponse;
import com.itconsortiumgh.cal.model.logs.MobileMoneyLogs;
import com.itconsortiumgh.cal.repository.MobileMoneyLogRepository;
import com.itconsortiumgh.cal.repository.RequestPaymentRequestSdpRepository;
import com.itconsortiumgh.cal.utils.GeneralUtils;
import com.itconsortiumgh.cal.utils.JsonUtility;
import com.itconsortiumgh.cal.utils.properties.ApplicationProperties;
import com.itconsortiumgh.cal.utils.properties.MenuProperties;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
@Data
public class RequestPaymentSubscriber implements MessageListener {
	@Autowired
	ApplicationProperties applicationProperties;

	@Autowired
	RestTemplate restTemplate;

	@Autowired
	MobileMoneyLogRepository mobileMoneyLogRepository;

	@Autowired
	RequestPaymentRequestSdpRepository repository;
	@Autowired
	GeneralUtils generalUtils;
	
	@Autowired
	MenuProperties menuProperties;
	public void onMessage(final Message incomingmessage, final byte[] pattern) {
		String message = new String(incomingmessage.getBody());
		processMessage(message);
	}

	public void processMessage(String message) {

		try {
			RequestPaymentRequestSdp requestPaymentRequestSdp = JsonUtility.fromJson(message, RequestPaymentRequestSdp.class);
			repository.save(requestPaymentRequestSdp);
			MDRPResponse response = generalUtils.executeRequestPayment(requestPaymentRequestSdp);
			if(!"1000".equals(response.getResponseCode())){
				log.info("response after calling requestpayment with parameters {}: {}", requestPaymentRequestSdp, response);
				String from = menuProperties.getSmsSender().trim();
				String to = requestPaymentRequestSdp.getMsisdn().trim();
				generalUtils.sendSMS(from, to, response.getResponseMessage());
			}else{
				log.info("response after calling requestpayment with parameters {}: {}", requestPaymentRequestSdp, response);
			}
		} catch (Exception e) {
			log.error("{}", e);
		}
	}
}
