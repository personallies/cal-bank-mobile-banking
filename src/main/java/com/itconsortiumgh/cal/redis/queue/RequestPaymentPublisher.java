package com.itconsortiumgh.cal.redis.queue;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.listener.ChannelTopic;
import org.springframework.stereotype.Service;

import com.itconsortiumgh.cal.model.UssdSession;
import com.itconsortiumgh.cal.utils.properties.ApplicationProperties;

@Service
public class RequestPaymentPublisher implements MessagePublisher{


	@Autowired
	private RedisTemplate<String, Object> redisTemplate;
	@Autowired
	private ChannelTopic requestPaymentTopic;
	@Autowired 
	ApplicationProperties applicationProperties;
	
	public RequestPaymentPublisher() {
		
	}

	public RequestPaymentPublisher(final RedisTemplate<String, Object> redisTemplate,
			final ChannelTopic topic) {
		this.redisTemplate = redisTemplate;
		this.requestPaymentTopic = topic;
	}

	@Override
	public void publish(final String message) {
		redisTemplate.convertAndSend(requestPaymentTopic.getTopic(), message);
		System.out.println("after convert and send ");
	}


}
