package com.itconsortiumgh.cal.redis.queue;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.listener.ChannelTopic;
import org.springframework.stereotype.Component;



@Component
public class AirtimePublisher implements MessagePublisher {
	@Autowired
	private RedisTemplate<String, Object> redisTemplate;
	@Autowired
	private ChannelTopic airtimeTopic;

	public AirtimePublisher() {
	}

	public AirtimePublisher(final RedisTemplate<String, Object> redisTemplate,
			final ChannelTopic topic) {
		this.redisTemplate = redisTemplate;
		this.airtimeTopic = topic;
	}

	public void publish(final String message) {
		redisTemplate.convertAndSend(airtimeTopic.getTopic(), message);
	}


}
