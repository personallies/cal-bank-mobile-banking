package com.itconsortiumgh.cal.redis.queue;

import java.text.MessageFormat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.connection.MessageListener;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.itconsortiumgh.cal.model.Action;
import com.itconsortiumgh.cal.model.AirtimePurchaseRequest;
import com.itconsortiumgh.cal.model.BalanceRequest;
import com.itconsortiumgh.cal.model.BalanceResponse;
import com.itconsortiumgh.cal.model.BankToOwnWalletRequest;
import com.itconsortiumgh.cal.model.GeneralRequests;
import com.itconsortiumgh.cal.model.ResponseCode;
import com.itconsortiumgh.cal.model.TransactionResponse;
import com.itconsortiumgh.cal.model.TransactionType;
import com.itconsortiumgh.cal.model.UssdSession;
import com.itconsortiumgh.cal.model.logs.MobileMoneyLogs;
import com.itconsortiumgh.cal.model.logs.TransactionLogs;
import com.itconsortiumgh.cal.repository.MobileMoneyLogRepository;
import com.itconsortiumgh.cal.repository.TransactionLogRepository;
import com.itconsortiumgh.cal.service.BrokerApiFacade;
import com.itconsortiumgh.cal.utils.GeneralUtils;
import com.itconsortiumgh.cal.utils.JsonUtility;
import com.itconsortiumgh.cal.utils.properties.ApplicationProperties;
import com.itconsortiumgh.cal.utils.properties.BrokerProperties;
import com.itconsortiumgh.cal.utils.properties.MenuProperties;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
@Data

public class AirtimeSubscriber implements MessageListener {
	@Autowired
	ApplicationProperties applicationProperties;

	@Autowired
	RestTemplate restTemplate;
    @Autowired
    TransactionLogRepository transactionLogRepository;
	@Autowired
	MobileMoneyLogRepository mobileMoneyLogRepository;
	@Autowired
	MenuProperties menuProperties;
	@Autowired
	GeneralUtils generalUtils;
	@Autowired
	BrokerProperties brokerProperties;
	@Autowired
	BrokerApiFacade brokerApiFacade;
	 
	public void onMessage(final Message incomingmessage, final byte[] pattern) {
		String message = new String(incomingmessage.getBody());
		processMessage(message);
	}
	
	
	public void processMessage(String message) {
		UssdSession ussdSession = JsonUtility.fromJson(message, UssdSession.class);
		TransactionLogs transactionLogs=new TransactionLogs(ussdSession);
		AirtimePurchaseRequest airtimePurchaseRequest =transactionLogs.setAirtimeRequest(ussdSession);
		transactionLogRepository.save(transactionLogs);

		try {
			//TransactionResponse response = brokerApiFacade.purchaseAirtime(airtimePurchaseRequest);
			String from = menuProperties.getSmsSender();
			String to = airtimePurchaseRequest.getMobile();
			String failureNotice = "Airtime purchase not successful";
			Boolean truth=true;
			if(truth){
				//generalUtils.sendSMS(from, to, failureNotice);	
				transactionLogs.setComment("Transaction Successful");
				transactionLogRepository.save(transactionLogs);
				
			}else{
				
				//log airtime purchase transaction
//				if(!ResponseCode.CODE_01_OK.equalsIgnoreCase(response.getResponseCode())){
//					generalUtils.sendSMS(from, to, failureNotice);	
					transactionLogs.setComment("Transaction Failed");
					transactionLogRepository.save(transactionLogs);
					
			}
		} catch (Exception e) {
			log.error("{}", e);
		}
	}
}