package com.itconsortiumgh.cal.redis.queue;

import java.text.MessageFormat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.connection.MessageListener;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.itconsortiumgh.cal.model.FundsTransferOperation;
import com.itconsortiumgh.cal.model.FundsTransferRequest;
import com.itconsortiumgh.cal.model.FundsTransferResponse;
import com.itconsortiumgh.cal.model.ResponseCode;
import com.itconsortiumgh.cal.model.UssdSession;
import com.itconsortiumgh.cal.model.logs.TransactionLogs;
import com.itconsortiumgh.cal.utils.GeneralUtils;
import com.itconsortiumgh.cal.utils.JsonUtility;
import com.itconsortiumgh.cal.utils.properties.ApplicationProperties;
import com.itconsortiumgh.cal.utils.properties.MenuProperties;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
@Data
public class FundsTransferSubscriber implements MessageListener {
	@Autowired
	ApplicationProperties applicationProperties;

	@Autowired
	RestTemplate restTemplate;

	@Autowired
	MenuProperties menuProperties;
	@Autowired
	GeneralUtils generalUtils;

	public void onMessage(final Message incomingmessage, final byte[] pattern) {
		String message = new String(incomingmessage.getBody());
		processMessage(message);
	}

	public void processMessage(String message) {
		UssdSession ussdSession = JsonUtility.fromJson(message, UssdSession.class);
		String transferTemplate = "";

		if (FundsTransferOperation.TRANSFER_FOR_MYSELF.equals(ussdSession.getFundsTransferOperation())) {
			transferTemplate = menuProperties.getTransferToSelfSmsTemplate();
		} else {
			transferTemplate = menuProperties.getTransferToOtherSmsTemplate();
		}

		TransactionLogs transactionLogs = new TransactionLogs(ussdSession);
		FundsTransferRequest fundsTransferRequest = transactionLogs.setFundsTransferRequest(ussdSession);
		String url = applicationProperties.getTransferEndpoint();
		log.info("Transfer URL>>>> {}", url);
		log.info("Transfer Request: {}", fundsTransferRequest);
		FundsTransferResponse transferResponse = null;
		String from = menuProperties.getSmsSender().trim();
		String to = fundsTransferRequest.getMsisdn().trim();

		try {
			log.info("Funds Transfer Request : {}",fundsTransferRequest);
//			transferResponse = restTemplate.postForObject(url, fundsTransferRequest, FundsTransferResponse.class);
//			log.info("Transfer Response: {}", transferResponse);
//
//			if (ResponseCode.CODE_01_OK.equalsIgnoreCase(transferResponse.getResponseCode())) {
//				String transferMessage = MessageFormat.format(transferTemplate, fundsTransferRequest.getAmount(),
//						fundsTransferRequest.getDebitAccountLabel(), fundsTransferRequest.getCreditAccountLabel())
//						.trim();
//				generalUtils.sendSMS(from, to, transferMessage);
//			} else {
//				generalUtils.sendSMS(from, to, "The transfer failed at the bank. Please contact customer support");
//			}
		} catch (Exception e) {
//			generalUtils.sendSMS(from, to, "The transfer failed at the bank. Please contact customer support");
		}
	}
}