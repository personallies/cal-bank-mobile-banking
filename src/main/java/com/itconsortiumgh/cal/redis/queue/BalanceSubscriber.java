package com.itconsortiumgh.cal.redis.queue;

import java.text.MessageFormat;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.connection.MessageListener;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.itconsortiumgh.cal.model.Action;
import com.itconsortiumgh.cal.model.BalanceRequest;
import com.itconsortiumgh.cal.model.BalanceResponse;
import com.itconsortiumgh.cal.model.BankToOwnWalletRequest;
import com.itconsortiumgh.cal.model.ResponseCode;
import com.itconsortiumgh.cal.model.TransactionResponse;
import com.itconsortiumgh.cal.model.UssdSession;
import com.itconsortiumgh.cal.model.logs.MobileMoneyLogs;
import com.itconsortiumgh.cal.model.logs.TransactionLogs;
import com.itconsortiumgh.cal.repository.MobileMoneyLogRepository;
import com.itconsortiumgh.cal.repository.TransactionLogRepository;
import com.itconsortiumgh.cal.utils.GeneralUtils;
import com.itconsortiumgh.cal.utils.JsonUtility;
import com.itconsortiumgh.cal.utils.properties.ApplicationProperties;
import com.itconsortiumgh.cal.utils.properties.MenuProperties;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
@Data
public class BalanceSubscriber implements MessageListener {
	@Autowired
	ApplicationProperties applicationProperties;

	@Autowired
	RestTemplate restTemplate;
	@Autowired
	TransactionLogRepository transactionLogRepository;

	@Autowired
	MobileMoneyLogRepository mobileMoneyLogRepository;
	@Autowired
	MenuProperties menuProperties;
	@Autowired
	GeneralUtils generalUtils;

	public void onMessage(final Message incomingmessage, final byte[] pattern) {
		String message = new String(incomingmessage.getBody());
		processMessage(message);
	}

	public void processMessage(String message) {

		
		UssdSession ussdSession=JsonUtility.fromJson(message, UssdSession.class);
		TransactionLogs transactionLogs = new TransactionLogs(ussdSession);
		BalanceRequest balanceRequest= new BalanceRequest(ussdSession);
		transactionLogRepository.save(transactionLogs);
		try {
			String balanceTemplate = menuProperties.getBalanceSmsTemplate();
		String url = applicationProperties.getBalanceEnpoint();
		//BalanceResponse balanceResponse = restTemplate.postForObject(url, balanceRequest, BalanceResponse.class);
//			String balance = balanceResponse.getAvailableBalance().toPlainString().trim();
//			String balanceMessage = MessageFormat.format(balanceTemplate, balanceRequest.getAccountLabel(), balance).trim();
//			String from = menuProperties.getSmsSender().trim();
//			String to = balanceRequest.getMsisdn().trim();
//			generalUtils.sendSMS(from, to, balanceMessage);
			transactionLogs.setComment("Transaction Successful");
			transactionLogs.setUpdated(new Date());
			transactionLogRepository.save(transactionLogs);
			
		} catch (Exception e) {
			log.error("{}", e);
		}
	}
}