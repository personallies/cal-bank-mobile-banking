package com.itconsortiumgh.cal.redis.queue;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.connection.MessageListener;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.itconsortiumgh.cal.model.BankToOwnWalletRequest;
import com.itconsortiumgh.cal.model.ResponseCode;
import com.itconsortiumgh.cal.model.TransactionResponse;
import com.itconsortiumgh.cal.repository.MobileMoneyLogRepository;
import com.itconsortiumgh.cal.utils.JsonUtility;
import com.itconsortiumgh.cal.utils.properties.ApplicationProperties;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
@Data
public class WalletToBankSubscriber implements MessageListener{
	@Autowired
	ApplicationProperties applicationProperties;
	@Autowired
	RestTemplate restTemplate;
	@Autowired
	MobileMoneyLogRepository mobileMoneyLogRepository;

	public void onMessage(final Message incomingmessage, final byte[] pattern) {
		String message = new String(incomingmessage.getBody());
		processMessage(message);
	}
	
	
	public void processMessage(String message) {

		try {
			log.info("===================================successful");
//			BankToWalletRequest bankToWalletRequest = JsonUtility.fromJson(message, BankToWalletRequest.class);
//			String url = applicationProperties.getBankToWalletEndpoint();
//			TransactionResponse transactionResponse = restTemplate.postForObject(url, bankToWalletRequest,
//					TransactionResponse.class);
//			if (transactionResponse.getResponseCode().equalsIgnoreCase(ResponseCode.CODE_01_OK)) {
//				log.info("===================================successful");
//			} else {
//				log.info("===================================failure");
//			}
		} catch (Exception e) {
			log.error("{}", e);
		}
	}
	
	
	
	
	

}
