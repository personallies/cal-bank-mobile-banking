package com.itconsortiumgh.cal.redis.queue;

import com.itconsortiumgh.cal.model.UssdSession;

public interface MessagePublisher {

    void publish(final String message);


}