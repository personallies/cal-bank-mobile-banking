package com.itconsortiumgh.cal.redis.queue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.listener.ChannelTopic;
import org.springframework.stereotype.Component;

import com.itconsortiumgh.cal.model.UssdSession;

@Component
public class FundsTransferPublisher implements MessagePublisher {
	@Autowired
	private RedisTemplate<String, Object> redisTemplate;
	@Autowired
	private ChannelTopic transferTopic;

	public FundsTransferPublisher() {
	}

	public FundsTransferPublisher(final RedisTemplate<String, Object> redisTemplate,
			final ChannelTopic topic) {
		this.redisTemplate = redisTemplate;
		this.transferTopic = topic;
	}

	public void publish(final String message) {
		redisTemplate.convertAndSend(transferTopic.getTopic(), message);
	}


}
