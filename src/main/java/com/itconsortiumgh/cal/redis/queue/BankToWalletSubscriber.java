package com.itconsortiumgh.cal.redis.queue;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.connection.MessageListener;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;
import com.itconsortiumgh.cal.model.Action;
import com.itconsortiumgh.cal.model.BalanceRequest;
import com.itconsortiumgh.cal.model.BankToOwnWalletRequest;
import com.itconsortiumgh.cal.model.BankToWalletRequest;
import com.itconsortiumgh.cal.model.MDRPResponse;
import com.itconsortiumgh.cal.model.MakeDepositRequestSdp;
import com.itconsortiumgh.cal.model.ResponseCode;
import com.itconsortiumgh.cal.model.TransactionResponse;
import com.itconsortiumgh.cal.model.UssdSession;
import com.itconsortiumgh.cal.model.logs.MobileMoneyLogs;
import com.itconsortiumgh.cal.model.logs.TransactionLogs;
import com.itconsortiumgh.cal.repository.MobileMoneyLogRepository;
import com.itconsortiumgh.cal.utils.GeneralUtils;
import com.itconsortiumgh.cal.utils.JsonUtility;
import com.itconsortiumgh.cal.utils.TransactionIdGenerator;
import com.itconsortiumgh.cal.utils.properties.ApplicationProperties;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class BankToWalletSubscriber implements MessageListener {
	@Autowired
	ApplicationProperties applicationProperties;

	@Autowired
	RestTemplate restTemplate;

	@Autowired
	MobileMoneyLogRepository mobileMoneyLogRepository;

	@Autowired
	GeneralUtils generalUtils;

	public void onMessage(final Message incomingmessage, final byte[] pattern) {
		String message = new String(incomingmessage.getBody());
		processMessage(message);
	}

	public void processMessage(String message) {
		UssdSession ussdSession=JsonUtility.fromJson(message, UssdSession.class);
		MobileMoneyLogs mobileMoneyLogs=new MobileMoneyLogs(ussdSession);
	    BankToWalletRequest bankToWalletRequest=new BankToWalletRequest(ussdSession);
		mobileMoneyLogRepository.save(mobileMoneyLogs);

		try {
			log.info("===================================successful");
			//String url = applicationProperties.getBankToWalletEndpoint();
		//	TransactionResponse response = generalUtils.executeBankToWalletAtBank(bankToWalletRequest);
			Boolean isValid=true;
			if (isValid) {
//				MakeDepositRequestSdp makeDepositRequestSdp = new MakeDepositRequestSdp();
//				String amount = bankToWalletRequest.getAmount().toPlainString();
//				makeDepositRequestSdp.setAmount(amount);
//				makeDepositRequestSdp.setCurrency(applicationProperties.getCurrency());
//				makeDepositRequestSdp.setImsiNum("");
//				makeDepositRequestSdp.setMsisdn(bankToWalletRequest.getMsisdn());
//				makeDepositRequestSdp.setNarration("Withdrawing from yello account");
//				makeDepositRequestSdp.setProcessingNumber(TransactionIdGenerator.nextId().toString());
//				makeDepositRequestSdp.setServiceId(applicationProperties.getMdServiceId());
//				makeDepositRequestSdp.setSpId(applicationProperties.getMdSpId());
//				makeDepositRequestSdp.setSpPassword(applicationProperties.getMdSpPassword());
//				MDRPResponse mdrpResponse = generalUtils.executeDepositToWallet(makeDepositRequestSdp);
//				
//				if(!ResponseCode.CODE_01_OK.equalsIgnoreCase(mdrpResponse.getResponseCode())){
//
//				}
				mobileMoneyLogs.setUpdated(new Date());
				mobileMoneyLogRepository.save(mobileMoneyLogs);
//				
			} 
			
			else {
				log.info("===================================failure");

			}
		} catch (Exception e) {
			log.error("{}", e);
		}
	}
}