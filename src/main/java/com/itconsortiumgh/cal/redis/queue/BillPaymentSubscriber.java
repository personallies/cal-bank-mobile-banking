package com.itconsortiumgh.cal.redis.queue;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.connection.MessageListener;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.itconsortiumgh.cal.model.BillPaymentRequest;
import com.itconsortiumgh.cal.model.ResponseCode;
import com.itconsortiumgh.cal.model.TransactionResponse;
import com.itconsortiumgh.cal.model.UssdSession;
import com.itconsortiumgh.cal.model.logs.TransactionLogs;
import com.itconsortiumgh.cal.repository.MobileMoneyLogRepository;
import com.itconsortiumgh.cal.repository.TransactionLogRepository;
import com.itconsortiumgh.cal.service.BillPaymentFacade;
import com.itconsortiumgh.cal.utils.GeneralUtils;
import com.itconsortiumgh.cal.utils.JsonUtility;
import com.itconsortiumgh.cal.utils.properties.ApplicationProperties;
import com.itconsortiumgh.cal.utils.properties.MenuProperties;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
@Data
public class BillPaymentSubscriber implements MessageListener {
	@Autowired
	ApplicationProperties applicationProperties;

	@Autowired
	RestTemplate restTemplate;
	@Autowired
	TransactionLogRepository transactionLogRepository;

	@Autowired
	MobileMoneyLogRepository mobileMoneyLogRepository;
	@Autowired
	MenuProperties menuProperties;
	@Autowired
	GeneralUtils generalUtils;
	@Autowired
	BillPaymentFacade billPaymentFacade;

	public void onMessage(final Message incomingmessage, final byte[] pattern) {
		String message = new String(incomingmessage.getBody());
		processMessage(message);
	}

	public void processMessage(String message) {

		
		UssdSession ussdSession=JsonUtility.fromJson(message, UssdSession.class);
		TransactionLogs transactionLogs = new TransactionLogs(ussdSession);
		BillPaymentRequest dstvbillPaymentRequest=transactionLogs.setdstvBillPaymentRequest(ussdSession);
		transactionLogRepository.save(transactionLogs);
		try {
			TransactionResponse response = billPaymentFacade.dstvBillPayment(dstvbillPaymentRequest);
			if(ResponseCode.CODE_01_OK.equalsIgnoreCase(response.getResponseCode())){
				transactionLogs.setComment("Transaction Successful");
				log.info("translogs{}",transactionLogs);
				transactionLogRepository.save(transactionLogs);
			}
			else{
				transactionLogs.setComment("Transaction Failed");
				transactionLogRepository.save(transactionLogs);
			}
		
		}
		 catch (Exception e) {
			log.error("{}", e);
		}
	
}}