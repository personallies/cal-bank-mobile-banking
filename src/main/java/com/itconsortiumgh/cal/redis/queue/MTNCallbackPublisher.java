package com.itconsortiumgh.cal.redis.queue;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.listener.ChannelTopic;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.itconsortiumgh.cal.model.UssdSession;
import com.itconsortiumgh.cal.utils.properties.ApplicationProperties;

@Component
public class MTNCallbackPublisher implements MessagePublisher{


	@Autowired
	private RedisTemplate<String, Object> redisTemplate;
	@Autowired
	private ChannelTopic mtnCallbacktopic;
	
	public MTNCallbackPublisher() {
		
	}

	public MTNCallbackPublisher(final RedisTemplate<String, Object> redisTemplate,
			final ChannelTopic topic) {
		this.redisTemplate = redisTemplate;
		this.mtnCallbacktopic = topic;
	}

	@Override
	public void publish(final String message) {
		redisTemplate.convertAndSend(mtnCallbacktopic.getTopic(), message);
	}



}
