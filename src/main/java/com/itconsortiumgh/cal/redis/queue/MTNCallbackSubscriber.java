package com.itconsortiumgh.cal.redis.queue;

import java.io.IOException;
import java.text.MessageFormat;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.connection.MessageListener;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.itconsortiumgh.cal.model.Action;
import com.itconsortiumgh.cal.model.BankToOwnWalletRequest;
import com.itconsortiumgh.cal.model.DialectPaymentBean;
import com.itconsortiumgh.cal.model.MDRPResponse;
import com.itconsortiumgh.cal.model.MobileMoneyOperation;
import com.itconsortiumgh.cal.model.RequestPaymentRequestSdp;
import com.itconsortiumgh.cal.model.ResponseCode;
import com.itconsortiumgh.cal.model.TransactionResponse;
import com.itconsortiumgh.cal.model.UssdSession;
import com.itconsortiumgh.cal.model.WalletToBankRequest;
import com.itconsortiumgh.cal.model.logs.MobileMoneyLogs;
import com.itconsortiumgh.cal.repository.MobileMoneyLogRepository;
import com.itconsortiumgh.cal.repository.RequestPaymentRequestSdpRepository;
import com.itconsortiumgh.cal.repository.redis.RedisUssdSessionRepository;
import com.itconsortiumgh.cal.utils.GeneralUtils;
import com.itconsortiumgh.cal.utils.JsonUtility;
import com.itconsortiumgh.cal.utils.properties.ApplicationProperties;
import com.itconsortiumgh.cal.utils.properties.MenuProperties;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
@Data
public class MTNCallbackSubscriber implements MessageListener {
	@Autowired
	ApplicationProperties applicationProperties;

	@Autowired
	RestTemplate restTemplate;

	@Autowired
	MobileMoneyLogRepository mobileMoneyLogRepository;

	@Autowired
	RequestPaymentRequestSdpRepository repository;
	@Autowired
	GeneralUtils generalUtils;
	@Autowired
	MenuProperties menuProperties;
	@Autowired
	RedisUssdSessionRepository sessionRepository;
	public void onMessage(final Message incomingmessage, final byte[] pattern) {
		String message = new String(incomingmessage.getBody());
		processMessage(message);
	}

	public void processMessage(String message) {

		try {
			String paymentEndpoint = applicationProperties.getWalletToBankEndpoint();
			WalletToBankRequest walletToBankRequest = JsonUtility.fromJson(message, WalletToBankRequest.class);
			UssdSession ussdSession = new UssdSession();
//			ussdSession.setSessionId(walletToBankRequest.getSessionId());
			ussdSession = sessionRepository.get(ussdSession);
			walletToBankRequest.setAmount(ussdSession.getAmount());
			walletToBankRequest.setMobile(ussdSession.getMsisdn());
			walletToBankRequest.setTestFlag(applicationProperties.getTestFlag());
			walletToBankRequest.setWalletProvider(ussdSession.getWalletProvider());
			String amount = walletToBankRequest.getAmount().toPlainString().trim();
			String from=menuProperties.getSmsSender();
			String to = walletToBankRequest.getMobile();
			TransactionResponse response = null;
			if(walletToBankRequest.getMobileMoneyOperation().equals(MobileMoneyOperation.WALLET_TO_OWN_ACCOUNT)){
				walletToBankRequest.setCreditAccountLabel(ussdSession.getCreditAccountLabel());
				response = generalUtils.executeWalletToOwnAccount(walletToBankRequest);
				if(StringUtils.equalsIgnoreCase(response.getResponseCode(), ResponseCode.CODE_01_OK)){
					String smsMessage = MessageFormat.format(menuProperties.getSuccessfulwalletToOwnTemplate(), amount, walletToBankRequest.getCreditAccountLabel());
					generalUtils.sendSMS(from, to, smsMessage);
				}else{
					String smsMessage = menuProperties.getTransactionFailedAtBank();
					generalUtils.sendSMS(from, to, smsMessage);
					MDRPResponse reversalResponse = generalUtils.executeReversalOfRequestPayment(walletToBankRequest.getTransactionId());
				}
			}else if(walletToBankRequest.getMobileMoneyOperation().equals(MobileMoneyOperation.WALLET_TO_OTHER_ACCOUNT)){
				walletToBankRequest.setCreditAccountNumber(ussdSession.getCreditAccountNumber());
				response = generalUtils.executeWalletToOtherAccount(walletToBankRequest);
				if(StringUtils.equalsIgnoreCase(response.getResponseCode(), ResponseCode.CODE_01_OK)){
					String smsMessage = MessageFormat.format(menuProperties.getSuccessfulwalletToOwnTemplate(), amount, walletToBankRequest.getCreditAccountNumber());
					generalUtils.sendSMS(from, to, smsMessage);
				}else{
					String smsMessage = menuProperties.getTransactionFailedAtBank();
					generalUtils.sendSMS(from, to, smsMessage);
					MDRPResponse reversalResponse = generalUtils.executeReversalOfRequestPayment(walletToBankRequest.getTransactionId());
				}
			}
		}catch(Exception e) {
			log.error("{}", e);
		}
	}
}
