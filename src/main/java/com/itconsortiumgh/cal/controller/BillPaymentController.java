package com.itconsortiumgh.cal.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.itconsortiumgh.cal.model.SessionStatus;
import com.itconsortiumgh.cal.model.UssdRequest;
import com.itconsortiumgh.cal.model.UssdResponse;
import com.itconsortiumgh.cal.model.UssdSession;
import com.itconsortiumgh.cal.repository.redis.RedisUssdSessionRepository;
import com.itconsortiumgh.cal.service.BillPaymentBusinessLogic;

@RestController
@RequestMapping("/billPayment")
public class BillPaymentController {
	@Autowired
	BillPaymentBusinessLogic billPaymentBusinessLogic;
	@Autowired
	RedisUssdSessionRepository redisUssdSessionRepository;

	@PostMapping(value = "/process")
	public UssdResponse ussdResponse(@RequestBody UssdRequest ussdRequest) {
		if (ussdRequest != null) {
			UssdSession tmpUssdSession = new UssdSession();
			tmpUssdSession.setSessionId(ussdRequest.getSessionId());
			UssdSession existingSession = redisUssdSessionRepository.get(tmpUssdSession);
			UssdSession ussdSession = null;
			if (existingSession != null) {
				ussdSession = existingSession;
			} else {
				ussdSession = new UssdSession();
				ussdSession.setSessionId(ussdRequest.getSessionId());
				ussdSession.setStatus(SessionStatus.ACTIVE);
			}
			return billPaymentBusinessLogic.processRequest(ussdRequest, ussdSession);
		} else {
			return null;
		}

	}
}
