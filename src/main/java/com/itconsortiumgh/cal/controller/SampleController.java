package com.itconsortiumgh.cal.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.itconsortiumgh.cal.model.UssdRequest;
import com.itconsortiumgh.cal.model.UssdResponse;
import com.itconsortiumgh.cal.service.SampleBusinessLogic;

@RestController
public class SampleController {
	
	@Autowired
	SampleBusinessLogic sampleBusinessLogic;
	
	@PostMapping(value = "/trywork", consumes = MediaType.APPLICATION_JSON_VALUE)
	public UssdResponse processSample(@RequestBody UssdRequest ussdRequest) {
		return sampleBusinessLogic.tryWork(ussdRequest);
	}	
	
	@PostMapping(value = "/sectrywork", consumes = MediaType.APPLICATION_JSON_VALUE)
	public UssdResponse processSecondSample(@RequestBody UssdRequest ussdRequest) {
		return sampleBusinessLogic.trySecWork(ussdRequest);
	}
	
	@PostMapping(value = "/thirdtrywork")
	public UssdResponse processThirdSample(@RequestBody UssdRequest  ussdRequest){
		return sampleBusinessLogic.tryThirdWork(ussdRequest);
	}
	

}
