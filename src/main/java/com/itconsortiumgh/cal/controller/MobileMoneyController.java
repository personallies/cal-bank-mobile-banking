package com.itconsortiumgh.cal.controller;

import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.itconsortiumgh.cal.model.MobileMoneyOperation;
import com.itconsortiumgh.cal.model.ResponseCode;
import com.itconsortiumgh.cal.model.SessionStatus;
import com.itconsortiumgh.cal.model.UssdRequest;
import com.itconsortiumgh.cal.model.UssdResponse;
import com.itconsortiumgh.cal.model.UssdSession;
import com.itconsortiumgh.cal.model.WalletToBankRequest;
import com.itconsortiumgh.cal.redis.queue.MTNCallbackPublisher;
import com.itconsortiumgh.cal.repository.redis.RedisUssdSessionRepository;
import com.itconsortiumgh.cal.service.MobileMoneyBusinessLogic;
import com.itconsortiumgh.cal.utils.GeneralUtils;
import com.itconsortiumgh.cal.utils.JsonUtility;
import com.itconsortiumgh.cal.utils.SoapMessageReader;
import com.itconsortiumgh.cal.utils.XMLFormatter;
import com.itconsortiumgh.cal.utils.properties.MenuProperties;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/mobileMoney")
@Slf4j
public class MobileMoneyController {
	@Autowired
	MobileMoneyBusinessLogic mobileMoneyBusinessLogic;
	@Autowired
	MenuProperties menuProperties;
	@Autowired
	GeneralUtils generalUtils;
	@Autowired
	RedisUssdSessionRepository redisUssdSessionRepository;
	@Autowired
	MTNCallbackPublisher mtnCallbackPublisher;

	@RequestMapping(value = "/process", method = RequestMethod.POST)
	public UssdResponse ussdResponse(@RequestBody UssdRequest ussdRequest) {
		if (ussdRequest != null) {
			UssdSession tmpUssdSession = new UssdSession();
			tmpUssdSession.setSessionId(ussdRequest.getSessionId());
			UssdSession existingSession = redisUssdSessionRepository.get(tmpUssdSession);
			UssdSession ussdSession = null;
			if (existingSession != null) {
				ussdSession = existingSession;
			} else {
				ussdSession = new UssdSession();
				ussdSession.setSessionId(ussdRequest.getSessionId());
				ussdSession.setStatus(SessionStatus.ACTIVE);
			}
			return mobileMoneyBusinessLogic.processRequest(ussdSession, ussdRequest);
		} else {
			return null;
		}
	}

	@RequestMapping(value = "/callback", method = RequestMethod.POST, consumes = { MediaType.APPLICATION_XML_VALUE,
			MediaType.TEXT_XML_VALUE })
	public String callback(@RequestBody String sdpRequest, HttpServletRequest request) {
		log.info("here is the callback request{}", XMLFormatter.format(sdpRequest));

		String sourceIP = request.getRemoteAddr();
		String response = "";
		String thirdPartyAcctRef = SoapMessageReader.findSOAPField(sdpRequest, "ns3:ThirdPartyAcctRef");
		String processingNumber = SoapMessageReader.findSOAPField(sdpRequest, "ns3:ProcessingNumber");
		String momTransactionID = SoapMessageReader.findSOAPField(sdpRequest, "ns3:MOMTransactionID");
		String statusCode = SoapMessageReader.findSOAPField(sdpRequest, "ns3:StatusCode").trim();
		String statusDesc = SoapMessageReader.findSOAPField(sdpRequest, "ns3:StatusDesc");
		StringTokenizer tokens = new StringTokenizer(thirdPartyAcctRef, "_");
		String operation = tokens.nextToken().trim();
		String msisdn = tokens.nextToken().trim();
		String sessionId = tokens.nextToken().trim();

		if (StringUtils.equalsIgnoreCase("TARGET_AUTHORIZATION_ERROR", statusCode)) {
			String insufficientFunds = menuProperties.getInsufficientFundsTemplate();
			String from = menuProperties.getSmsSender();
			generalUtils.sendSMS(from, msisdn, insufficientFunds);
		} else {
			if (StringUtils.equalsIgnoreCase(ResponseCode.CODE_01_OK, statusCode)) {
				WalletToBankRequest walletToBankRequest = new WalletToBankRequest();
				walletToBankRequest.setMobileMoneyOperation(Enum.valueOf(MobileMoneyOperation.class, operation));
//				walletToBankRequest.setMomTransactionId(momTransactionID);
//				walletToBankRequest.setSessionId(sessionId);
//				walletToBankRequest.setResponseMessage(statusDesc);
//				walletToBankRequest.setResponseCode(statusCode);
				walletToBankRequest.setTransactionId(processingNumber);
				mtnCallbackPublisher.publish(JsonUtility.toJson(walletToBankRequest));
				response = generalUtils.setupRequestPaymentCompletedResponse(ResponseCode.CODE_01_OK);
			} else {
				response = generalUtils.setupRequestPaymentCompletedResponse(statusCode);
			}
		}
		return response;
	}
}
