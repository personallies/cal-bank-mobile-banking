package com.itconsortiumgh.cal.utils;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtilsBean2;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.itconsortiumgh.cal.model.PaymentRequestModel;
import com.itconsortiumgh.cal.model.ProcessRequest;




public class ListMapBeanUtility {
Logger log = Logger.getLogger(getClass());
	public PaymentRequestModel listToConfirmationReqModel(ProcessRequest parameters){
		PaymentRequestModel model = new PaymentRequestModel();
		Map<String, String> requestMap = new HashMap<String, String>();
		List<Parameter> requestList = parameters.getParameter();
		log.info("--------------START PARAMETERS STRAIGHT FROM SDP--------------");
		for (Iterator<Parameter> iterator = requestList.iterator(); iterator.hasNext();) {
			Parameter parameter = iterator.next();
			log.info("name: "+ parameter.getName() + " == value: " + parameter.getValue());
			requestMap.put(StringUtils.uncapitalize(parameter.getName()), parameter.getValue());
		}
		log.info("--------------END PARAMETERS STRAIGHT FROM SDP--------------");
		try {
			BeanUtilsBean2.getInstance().populate(model, requestMap);
		} catch (IllegalAccessException | InvocationTargetException e) {
			e.printStackTrace();
		}
		return model;
	}
	
	public QueryRequestModel listToQueryReqModel(ProcessRequest parameters){
		QueryRequestModel model = new QueryRequestModel();
		Map<String, String> requestMap = new HashMap<String, String>();
		List<Parameter> requestList = parameters.getParameter();
		log.info("--------------START PARAMETERS STRAIGHT FROM SDP--------------");
		for (Iterator<Parameter> iterator = requestList.iterator(); iterator.hasNext();) {
			Parameter parameter = iterator.next();
			log.info("name: "+ parameter.getName() + " == value: " + parameter.getValue());
			requestMap.put(StringUtils.uncapitalize(parameter.getName()), parameter.getValue());
		}
		log.info("--------------END PARAMETERS STRAIGHT FROM SDP--------------");
		try {
			BeanUtilsBean2.getInstance().populate(model, requestMap);
		} catch (IllegalAccessException | InvocationTargetException e) {
			e.printStackTrace();
		}
		return model;
	}

}
