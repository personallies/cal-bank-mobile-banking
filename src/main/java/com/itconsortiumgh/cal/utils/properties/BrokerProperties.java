package com.itconsortiumgh.cal.utils.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import lombok.Data;

@Data
@Configuration
@PropertySource("classpath:broker.properties")
@ConfigurationProperties
public class BrokerProperties {
	private String smsEndpoint;
	private String clientSecret;
	private String clientId;
	private String brokerEndpoint;
	private String airtimeEndpoint;
	private String apiToken;
	public final static String SERVICE_AIRTIME="airtime";
	public final static String SERVICE_DSTV="dstv";
}
