package com.itconsortiumgh.cal.utils.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import lombok.Data;

@Data
@Configuration
@PropertySource("classpath:redis-topic.properties")
@ConfigurationProperties
public class TopicProperties {
	private String bankToWalletTopic;
	private String walletToOtherAccountTopic;
	private String walletToBankTopic;
	private String balanceTopic;
	private String requestPaymentTopic;
	private String transferTopic;
	private String mtnCallbackTopic;
	private String airtimeTopic;
	private String billPaymentTopic;
}
