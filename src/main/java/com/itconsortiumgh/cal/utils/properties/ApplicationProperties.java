package com.itconsortiumgh.cal.utils.properties;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import com.itconsortiumgh.cal.model.InvoiceContainer;
import com.itconsortiumgh.cal.model.MerchantsContainer;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Component
@Data
@Slf4j
@ConfigurationProperties
public class ApplicationProperties {
	private String selfServiceEndpoint;
	private String opFundTrans;
	private String appSrc;
	private String alertnumbers;
	private String dataSource;
	private String currency;
	private String vendorCode;
	private String pathBoAmountProcessing;
	private String delaybeforepush;
	private String loggingEndpoint;
	private String transflowResponseLoggingEndpoint;
	private String transflowURL;
	private String multichoiceExchange;
	private boolean hardcodedMultichoiceResponse;
	public Integer ussdsessionTimeout;
	private Boolean testFlag;
	private String mobileMoneyNetworks;
	public String billPaymentMerchants;
	public String dstvInvoice;
	private String transactionNumber;
	private String paymentDescription;
	private String productUserKey;
	private String methodOfPayment;
	private BigDecimal amount;
	private String invoicePeriod;
	private String businessUnit;
	private String language;
	private String ipAddress;
	private String paymentVendorCode;

	//ENDPOINTS
	private String bankToWalletEndpoint;
	private String walletToOtherAccountEndpoint;
	private String walletToOwnAccountEndpoint;
	private String walletToBankEndpoint;
	private String numberPortabilityEndpoint;
	private String transferEndpoint;
	private String pinValidationEndpoint;
	private String balanceEnpoint;
	private String makeDepositEndpoint;
	private String requestPaymentEndpoint;
	private String smsghBrokerEndpoint;
	private String smsghBrokerToken;
	
	//SDP PROPERTIES
	private boolean serviceIdInUATMode;
	private String rpServiceIdUAT;
	private String rpServiceId;
	private String rpSpId;
	private String rpSpPassword;
	
	private String MdServiceId;
	private String MdSpId;
	private String MdSpPassword;

	// TOPICS
	private String walletToBankTopic;
	private String bankToWalletTopic;

	// MAPS
	public Map<Integer, String> merchantMap = new HashMap<Integer, String>();
	public Map<Integer, String> networkMap = new HashMap<Integer, String>();
	public Map<Integer,String> invoiceMap=new HashMap<Integer,String>();

	public Map<Integer, String> retrieveMobileMoneyNetworks() {
		String newtorkArray[] = mobileMoneyNetworks.split(",");
		for (int i = 0; i < newtorkArray.length; i++) {
			networkMap.put(i, newtorkArray[i]);
		}
		return networkMap;
	}
	
	public MerchantsContainer retrieveBillPaymentMerchants() {
		String merchantArray[] = billPaymentMerchants.split(",");
		String merchantStr = "";
		for(int i =0; i< merchantArray.length;i++){
			merchantStr = merchantStr + (i+1) + ". " + merchantArray[i] + "\n";
			merchantMap.put((i+1), merchantArray[i]);
		}
		MerchantsContainer merchantsContainer = new MerchantsContainer();
		merchantsContainer.setMerchantsMap(merchantMap);
		merchantsContainer.setMerchantsMenu(merchantStr);
		return merchantsContainer;

	}
	
	public InvoiceContainer retrieveInvoicePeriod(){
		String invoiceArray[]=dstvInvoice.split(",");
		String invoiceStr="";
		for (int i = 0; i < invoiceArray.length; i++) {
			
			invoiceStr = invoiceStr + (i+1) + ". " + invoiceArray[i] + "\n";
			invoiceMap.put((i+1), invoiceArray[i]);
		}
		InvoiceContainer invoiceContainer=new InvoiceContainer();
		invoiceContainer.setInvoiceMap(invoiceMap);
		invoiceContainer.setInvoiceMenu(invoiceStr);
		return invoiceContainer;
	}
	
	
}
