package com.itconsortiumgh.cal.utils.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import lombok.Data;

@Data
@Configuration
@PropertySource("classpath:menu.properties")
@ConfigurationProperties
public class MenuProperties {
	private String rootDisplay;
	private String unregisteredDisplay;
	private String enterAccountNumber;
	private String enterPin;
	private String incorrectPin;
	private String enterDestinationAccount;
	private String successMessage;
	private String chooseWalletProvider;
	private String selectAccount;
	private String enterAmount;
	private String processingRequest;
	private String mobileBankingOperation;
	private String defaultMessage;
    private String enterValidAmount;
    private String chooseBuyFor;
	private String chooseNetwork;
    private String failureMessage;
    private String networkDoesNotExist;
    private String enterPhoneNumber;
    private String transactionProcessing;
    private String smsSender;
    private String transferNarration;
    private String transferToSelfSmsTemplate;
    private String transferToOtherSmsTemplate;
    private String balanceSmsTemplate;
    private String insufficientFundsTemplate;
    private String successfulwalletToOwnTemplate;
    private String successfulwalletToOtherTemplate;
    private String transactionFailedAtBank;
    private String transferTo;
    private String countryCode;
	private String unknownNumber;
	private String noAccount;
	private String chooseMerchant;
	private String enterCustomer;
	private String displayCustomer;
	private String chooseInvoicePeriod;
	private String cancelMessage;
	private String sameAccountLabel;
	
	//Confirm Messages
	private String confirmAirtimePurchase;
	private String confirmFundTransferToSelf;
	private String confirmFundTransferToOther;
	private String confirmBankToWallet;
	private String confirmWalletToSelf;
	private String confirmWalletToOther;
	private String confirmBillPayment;
}
