package com.itconsortiumgh.cal.utils;

public class MessageAirtime {
	public static final String MTN = "Please enter an MTN phone number ";
	public static final String VODAFONE = "Please enter an Vodafone phone number ";
	public static final String GLO = "Please enter an Glo phone number ";
	public static final String EXPRESSO = "Please enter an Expresso phone number ";
	public static final String TIGO = "Please enter an Tigo phone number ";
	public static final String AIRTEL = "Please enter an Airtel phone number ";

}
