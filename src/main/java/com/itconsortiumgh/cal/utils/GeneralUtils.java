package com.itconsortiumgh.cal.utils;

import java.io.IOException;
import java.io.StringReader;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.itconsortiumgh.cal.model.BankToWalletRequest;
import com.itconsortiumgh.cal.model.ClientState;
import com.itconsortiumgh.cal.model.CustomerAccountsContainer;
import com.itconsortiumgh.cal.model.DialectPaymentBean;
import com.itconsortiumgh.cal.model.MDRPResponse;
import com.itconsortiumgh.cal.model.MakeDepositRequestSdp;
import com.itconsortiumgh.cal.model.MessageType;
import com.itconsortiumgh.cal.model.PaymentRequestModel;
import com.itconsortiumgh.cal.model.ProcessRequest;
import com.itconsortiumgh.cal.model.ProcessRequestObjectFactory;
import com.itconsortiumgh.cal.model.RequestPaymentRequestSdp;
import com.itconsortiumgh.cal.model.ResponseCode;
import com.itconsortiumgh.cal.model.TransactionResponse;
import com.itconsortiumgh.cal.model.UssdRequest;
import com.itconsortiumgh.cal.model.UssdResponse;
import com.itconsortiumgh.cal.model.UssdSession;
import com.itconsortiumgh.cal.model.WalletProviderContainer;
import com.itconsortiumgh.cal.model.WalletToBankRequest;
import com.itconsortiumgh.cal.repository.CustomerRepository;
import com.itconsortiumgh.cal.repository.RequestPaymentRequestSdpRepository;
import com.itconsortiumgh.cal.repository.redis.RedisUssdSessionRepository;
import com.itconsortiumgh.cal.service.CustomerServices;
import com.itconsortiumgh.cal.utils.properties.ApplicationProperties;
import com.itconsortiumgh.cal.utils.properties.BrokerProperties;
import com.itconsortiumgh.cal.utils.properties.MenuProperties;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class GeneralUtils {
	// public static void main(String[] args) {
	// GeneralUtils generalUtils = new GeneralUtils();
	// generalUtils.sendSMS("CAL BANK", "233243576745", "Hello World");
	// }

	@Autowired
	ApplicationProperties applicationProperties;
	@Autowired
	MenuProperties menuProperties;
	@Autowired
	BrokerProperties brokerProperties;
	@Autowired
	CustomerRepository customerRepository;
	@Autowired
	RedisUssdSessionRepository redisUssdSessionRepository;
	@Autowired
	CustomerServices customerServices;
	@Autowired
	RequestPaymentRequestSdpRepository requestPaymentRequestSdpRepository;

	@Autowired
	RestTemplate restTemplate;

	public WalletProviderContainer retrieveWalletProviders() {
		String walletProviderMenu = "";
		String walletProviderStr = applicationProperties.getMobileMoneyNetworks();
		String walletProviderArray[] = walletProviderStr.split(",");
		Map<Integer, String> walletProviderMap = new HashMap<Integer, String>();
		Integer i = 1;
		for (int j = 0; j < walletProviderArray.length; j++) {
			walletProviderMenu = walletProviderMenu + (j + 1) + ". " + walletProviderArray[j] + "\n";
			walletProviderMap.put(j, walletProviderArray[j]);
		}
		WalletProviderContainer walletProviderContainer = new WalletProviderContainer();
		walletProviderContainer.setWalletProviderMap(walletProviderMap);
		walletProviderContainer.setWalletProviderMenu(walletProviderMenu);
		return walletProviderContainer;
	}

	/**
	 * If customer has only one account setup, this method returns that account
	 * label. The ClientState is set to
	 * {@link com.itconsortiumgh.cal.utils.ClientState#ENTER_AMOUNT} If customer
	 * has multiple accounts setup, this method persists a map of the account
	 * labels into the session object. The customer's choice will subsequently
	 * be picked from this map
	 * 
	 * @param mobile
	 * @param ussdSession
	 * @return
	 */
	public UssdResponse selectAcountLabel(String mobile, UssdSession ussdSession) {
		CustomerAccountsContainer customerAccountsContainer = customerServices.retrieveCustomerAccounts(mobile);
		Map<String, String> accountsMap = customerAccountsContainer.getAccountsMap();
		UssdResponse ussdResponse = new UssdResponse();

		if (accountsMap.size() == 1) {
			log.info("accountsMap size >>> {}", accountsMap.size());
			String accountLabel = accountsMap.get("1");
			log.info("accountLabel >>> {}", accountLabel);
			ussdResponse.setClientState(ClientState.ENTER_AMOUNT);
			ussdResponse.setMessage(menuProperties.getEnterAmount());
			ussdResponse.setType(MessageType.RESPONSE);
			ussdSession.setAccountLabel(accountLabel);
			ussdSession.setClientState(ClientState.ENTER_AMOUNT);

			redisUssdSessionRepository.put(ussdSession);
			return ussdResponse;

		} else if (accountsMap.size() > 1) {
			log.info("accountsMap size =========>>>{}", accountsMap.size());
			String selectAccountMenu = menuProperties.getSelectAccount() + "\n"
					+ customerAccountsContainer.getAccountsMenu();
			ussdResponse.setMessage(selectAccountMenu);
			ussdResponse.setClientState(ClientState.CHOOSE_ACCOUNT);
			ussdResponse.setType(MessageType.RESPONSE);
			ussdSession.setAccountLabelMapJson(JsonUtility.mapToJson(accountsMap));
			ussdSession.setClientState(ClientState.CHOOSE_ACCOUNT);
			redisUssdSessionRepository.put(ussdSession);
			return ussdResponse;
		} else {
			log.info("accountsMap size =========>>>{}", accountsMap.size());
			ussdResponse.setClientState(ClientState.FINAL);
			ussdResponse.setMessage(menuProperties.getNoAccount());
			ussdResponse.setType(MessageType.RELEASE);
			return ussdResponse;
		}
	}

	public Boolean checkIfNetworkExists(UssdRequest ussdRequest) {
		WalletProviderContainer walletProviderContainer = this.retrieveWalletProviders();
		Map<Integer, String> walletProviderMap = walletProviderContainer.getWalletProviderMap();
		Boolean checker = false;

		for (String network : walletProviderMap.values()) {
			if (network.equalsIgnoreCase(ussdRequest.getOperator())) {
				// checker = true;
				return true;
			}
		}
		return checker;

	}

	public void sendSMS(String from, String to, String message) {
		OkHttpClient client = new OkHttpClient();
		Request request = new Request.Builder()
				.url(brokerProperties.getSmsEndpoint() + "send?" + "From=" + from.trim() + "&" + "To=" + to.trim() + "&"
						+ "Content=" + message.trim() + "&" + "ClientId=" + brokerProperties.getClientId() + "&"
						+ "ClientSecret=" + brokerProperties.getClientSecret())
				.get().addHeader("cache-control", "no-cache")
				.addHeader("postman-token", "e8562911-659d-45da-c398-ce1b90816e43").build();
		log.info("{}", request.httpUrl());
		try {
			Response response = client.newCall(request).execute();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public MDRPResponse executeDepositToWallet(MakeDepositRequestSdp depositBean) {
		String amt = depositBean.getAmount();
		String amount = new BigDecimal(amt).multiply(new BigDecimal("100")).toPlainString();
		depositBean.setAmount(amount);
		ObjectMapper mapper = new ObjectMapper();
		MDRPResponse response = null;
		try {
			String request = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(depositBean);
			response = restTemplate.postForObject(applicationProperties.getMakeDepositEndpoint(), depositBean,
					MDRPResponse.class);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return response;
	}

	public MDRPResponse executeReversalOfRequestPayment(String oldTransactionId) {
		RequestPaymentRequestSdp requestPaymentRequestSdp = requestPaymentRequestSdpRepository
				.findByProcessingNumber(oldTransactionId);
		MakeDepositRequestSdp makeDepositRequestSdp = new MakeDepositRequestSdp();
		String amount = new BigDecimal("100").multiply(new BigDecimal(requestPaymentRequestSdp.getAmount()))
				.toPlainString();
		makeDepositRequestSdp.setAmount(amount);
		makeDepositRequestSdp.setCurrency(requestPaymentRequestSdp.getCurrency());
		makeDepositRequestSdp.setMsisdn(requestPaymentRequestSdp.getMsisdn());
		makeDepositRequestSdp.setNarration("reversal of " + oldTransactionId);
		makeDepositRequestSdp.setProcessingNumber(TransactionIdGenerator.nextId().toString());
		makeDepositRequestSdp.setServiceId(applicationProperties.getRpServiceId());
		makeDepositRequestSdp.setSpId(applicationProperties.getRpSpId());
		makeDepositRequestSdp.setSpPassword(applicationProperties.getRpSpPassword());
		ObjectMapper mapper = new ObjectMapper();
		MDRPResponse response = null;
		try {
			String request = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(makeDepositRequestSdp);
			log.info("reversal request {}", makeDepositRequestSdp);
			response = restTemplate.postForObject(applicationProperties.getMakeDepositEndpoint(), makeDepositRequestSdp,
					MDRPResponse.class);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return response;
	}

	public MDRPResponse executeRequestPayment(RequestPaymentRequestSdp requestPaymentRequestSdp) {
		BigDecimal amount = new BigDecimal(requestPaymentRequestSdp.getAmount());
		String momoAmount = amount.multiply(new BigDecimal("100")).toPlainString();
		requestPaymentRequestSdp.setAmount(momoAmount.trim());
		requestPaymentRequestSdp.setBalance(momoAmount.trim());
		requestPaymentRequestSdp.setImsiNum("1");
		requestPaymentRequestSdp.setMinDueAmount(momoAmount.trim());
		ObjectMapper mapper = new ObjectMapper();
		MDRPResponse response = null;
		try {
			String request = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(requestPaymentRequestSdp);
			response = restTemplate.postForObject(applicationProperties.getRequestPaymentEndpoint(),
					requestPaymentRequestSdp, MDRPResponse.class);
		} catch (JsonProcessingException e) {
			log.error("{}", e);
		}
		return response;
	}

	public PaymentRequestModel convertSdpRequestToRequestModel(String sdpRequest) {
		String processRequestStr;
		PaymentRequestModel requestModel = null;
		try {
			processRequestStr = SoapMessageReader.findSubXML(sdpRequest, "ns2:processRequest");
			System.out.println(processRequestStr);
			StringReader sr = new StringReader(processRequestStr);
			// JAXBContext jaxbContext =
			// JAXBContext.newInstance(ProcessRequest.class);
			JAXBContext jaxbContext = JAXBContext.newInstance(ProcessRequestObjectFactory.class);
			Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
			ProcessRequest processRequest = ((JAXBElement<ProcessRequest>) unmarshaller.unmarshal(sr)).getValue();
			ListMapBeanUtility beanUtility = new ListMapBeanUtility();
			requestModel = beanUtility.listToConfirmationReqModel(processRequest);
			String serviceId = SoapMessageReader.findSOAPField(sdpRequest, "serviceId");
			requestModel.setServiceId(serviceId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return requestModel;
	}

	public String setupSoapResponse(PaymentRequestModel requestModel, String statusCode, String statusDescription) {

		OMFactory fac = OMAbstractFactory.getOMFactory();
		OMNamespace omNs = fac.createOMNamespace("http://b2b.mobilemoney.mtn.zm_v1.0/", "b2b");

		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = null;
		try {
			builder = factory.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		DOMImplementation impl = builder.getDOMImplementation();

		Document message = impl.createDocument(null, null, null);
		Element envelope = message.createElement("soapenv:Envelope");
		envelope.setAttribute("xmlns:soapenv", "http://schemas.xmlsoap.org/soap/envelope/");
		envelope.setAttribute("xmlns:b2b", "http://b2b.mobilemoney.mtn.zm_v1.0/");
		message.appendChild(envelope);

		Element header = message.createElement("soapenv:Header");
		envelope.appendChild(header);

		Element body = message.createElement("soapenv:Body");
		envelope.appendChild(body);

		Element b2b_processRequestResponse = message.createElement("b2b:processRequestResponse");
		body.appendChild(b2b_processRequestResponse);

		Element returnProcessingNumber = message.createElement("return");
		b2b_processRequestResponse.appendChild(returnProcessingNumber);

		Element processingNumberName = message.createElement("name");
		returnProcessingNumber.appendChild(processingNumberName);
		processingNumberName.setTextContent("ProcessingNumber");

		Element processingNumberValue = message.createElement("value");
		returnProcessingNumber.appendChild(processingNumberValue);
		processingNumberValue.setTextContent(requestModel.getProcessingNumber());

		Element returnStatusCode = message.createElement("return");
		b2b_processRequestResponse.appendChild(returnStatusCode);

		Element StatusCodeName = message.createElement("name");
		returnStatusCode.appendChild(StatusCodeName);
		StatusCodeName.setTextContent("StatusCode");

		Element StatusCodeValue = message.createElement("value");
		returnStatusCode.appendChild(StatusCodeValue);
		StatusCodeValue.setTextContent(statusCode);

		Element returnStatusDesc = message.createElement("return");
		b2b_processRequestResponse.appendChild(returnStatusDesc);

		Element StatusDescName = message.createElement("name");
		returnStatusDesc.appendChild(StatusDescName);
		StatusDescName.setTextContent("StatusDesc");

		Element StatusDescValue = message.createElement("value");
		returnStatusDesc.appendChild(StatusDescValue);
		StatusDescValue.setTextContent(statusDescription);

		Element returnThirdPartyAcctRef = message.createElement("return");
		b2b_processRequestResponse.appendChild(returnThirdPartyAcctRef);

		Element ThirdPartyAcctRefName = message.createElement("name");
		returnThirdPartyAcctRef.appendChild(ThirdPartyAcctRefName);
		ThirdPartyAcctRefName.setTextContent("ThirdPartyAcctRef");

		Element ThirdPartyAcctRefValue = message.createElement("value");
		returnThirdPartyAcctRef.appendChild(ThirdPartyAcctRefValue);
		ThirdPartyAcctRefValue.setTextContent("2342343");

		Element returnToken = message.createElement("return");
		b2b_processRequestResponse.appendChild(returnToken);

		Element TokenName = message.createElement("name");
		returnToken.appendChild(TokenName);
		TokenName.setTextContent("Token");

		Element TokenValue = message.createElement("value");
		returnToken.appendChild(TokenValue);
		// TokenValue.setTextContent("");

		DocumentToStringXMLConverter docConverter = new DocumentToStringXMLConverter();
		String xml = docConverter.convertDocumentToStringXML(message);
		return xml;
	}

	public DialectPaymentBean createAndPopulateDialectPaymentBean(PaymentRequestModel requestModel, String sourceIp) {
		DialectPaymentBean payment = new DialectPaymentBean();
		payment.setAccountRef(requestModel.getAcctRef());
		// payment.setAccountRef(requestModel.getPaymentRef());

		Long fundamoAmount = new Long(requestModel.getRequestAmount());
		String customerName = requestModel.getCustName();
		String fundamoTransactionID = requestModel.getProcessingNumber();
		String msisdn = requestModel.getmOMAcctNum();
		String operation = applicationProperties.getOpFundTrans();
		String paymentRef = "";
		if (StringUtils.isEmpty(requestModel.getPaymentRef()) || StringUtils.isBlank(requestModel.getPaymentRef())) {
			paymentRef = "na";
		} else {
			paymentRef = requestModel.getPaymentRef();
		}
		String source = applicationProperties.getAppSrc();
		String thirdartyTransactionID = requestModel.getThirdPartyTransactionID();
		String sourceIP = sourceIp;
		String accountRef = replaceDotWithSpace(requestModel.getAcctRef());

		// Set Bean Properties
		BigDecimal amount = new BigDecimal(fundamoAmount.toString());
		payment.setAmount(amount);
		payment.setMomTransactionId(fundamoTransactionID);
		payment.setMobileNumber(msisdn);
		payment.setTransflowTransactionId(thirdartyTransactionID);
		payment.setResponseCode(requestModel.getStatusCode());
		payment.setSourceIp(sourceIP);
		payment.setAccountRef(accountRef);
		payment.setPaymentRef(paymentRef);
		return payment;
	}

	private String replaceDotWithSpace(String stringWithDot) {
		String stringWithSpace = stringWithDot.replace(".", " ");
		return stringWithSpace;
	}

	private String getRequestPaymentCompletedPrefix(String sdpRequest) {
		String rp[] = sdpRequest.split("requestPaymentCompleted>");
		String stringWithPrefixBegining = rp[1];

		String pf[] = stringWithPrefixBegining.split(":");
		String prefix = pf[0];
		prefix = prefix.replace("<", "").trim();
		return prefix;
	}

	public String findPropertyInReqPayCompletedXML(String sdpRequest, String property) {
		String prefix = getRequestPaymentCompletedPrefix(sdpRequest);
		String prefixedProperty = prefix + ":" + property;
		String results = "";
		try {
			results = SoapMessageReader.findSOAPField(sdpRequest, prefixedProperty);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return results;
	}

	public TransactionResponse executeWalletToOwnAccount(WalletToBankRequest walletToBankRequest) {
		TransactionResponse response = null;
		try {
			String endpoint = applicationProperties.getWalletToOwnAccountEndpoint().trim();
			response = restTemplate.postForObject(endpoint, walletToBankRequest, TransactionResponse.class);
		} catch (Exception e) {
			log.error("{}", e);
		}
		return response;
	}

	public TransactionResponse executeWalletToOtherAccount(WalletToBankRequest walletToBankRequest) {
		TransactionResponse response = null;
		try {
			String endpoint = applicationProperties.getWalletToOtherAccountEndpoint().trim();
			response = restTemplate.postForObject(endpoint, walletToBankRequest, TransactionResponse.class);
		} catch (Exception e) {
			log.error("{}", e);
		}
		return response;
	}

	public TransactionResponse executeBankToWalletAtBank(BankToWalletRequest bankToWalletRequest) {
		TransactionResponse response = null;
		try {
			String endpoint = applicationProperties.getBankToWalletEndpoint();
			response = restTemplate.postForObject(endpoint, bankToWalletRequest, TransactionResponse.class);
		} catch (Exception e) {
			log.error("{}", e);
		}
		return response;
	}

	public String setupRequestPaymentCompletedResponse(String statusCode) {

		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = null;
		try {
			builder = factory.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		DOMImplementation impl = builder.getDOMImplementation();

		Document message = impl.createDocument(null, null, null);
		Element envelope = message.createElement("S:Envelope");
		envelope.setAttribute("xmlns:S", "http://schemas.xmlsoap.org/soap/envelope/");
		message.appendChild(envelope);

		Element body = message.createElement("S:Body");
		envelope.appendChild(body);

		Element requestPaymentResponse = message.createElement("ns3:requestPaymentCompletedResponse");
		requestPaymentResponse.setAttribute("xmlns:ns2", "http://www.huawei.com.cn/schema/common/v2_1");
		requestPaymentResponse.setAttribute("xmlns:ns3", "http://www.csapi.org/schema/momopayment/local/v1_0");
		body.appendChild(requestPaymentResponse);

		Element ns3Result = message.createElement("ns3:result");
		requestPaymentResponse.appendChild(ns3Result);

		if (StringUtils.equalsIgnoreCase(ResponseCode.CODE_01_OK, statusCode)) {
			Element resultCode = message.createElement("resultCode");
			ns3Result.appendChild(resultCode);
			resultCode.setTextContent("00000000");

			Element resultDescription = message.createElement("resultDescription");
			ns3Result.appendChild(resultDescription);
			resultDescription.setTextContent("success");
		} else {
			Element resultCode = message.createElement("resultCode");
			ns3Result.appendChild(resultCode);
			resultCode.setTextContent("10000000");

			Element resultDescription = message.createElement("resultDescription");
			ns3Result.appendChild(resultDescription);
			resultDescription.setTextContent("failure");
		}

		Element extensionInfo = message.createElement("ns3:extensionInfo");
		requestPaymentResponse.appendChild(extensionInfo);

		Element item = message.createElement("item");
		extensionInfo.appendChild(item);

		Element key = message.createElement("key");
		item.appendChild(key);
		key.setTextContent("?");

		Element value = message.createElement("value");
		item.appendChild(value);
		value.setTextContent("?");

		DocumentToStringXMLConverter docConverter = new DocumentToStringXMLConverter();
		String xml = docConverter.convertDocumentToStringXML(message);
		return xml;
	}

}
