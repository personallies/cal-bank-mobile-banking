package com.itconsortiumgh.cal.utils;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class QueryRequestModel {
	private String processingNumber, senderID, acctRef, prefLang, opCoID;
	private int serviceId;
	public String getProcessingNumber() {
		return processingNumber;
	}
	public void setProcessingNumber(String processingNumber) {
		this.processingNumber = processingNumber;
	}
	public String getSenderID() {
		return senderID;
	}
	public void setSenderID(String senderID) {
		this.senderID = senderID;
	}
	public String getAcctRef() {
		return acctRef;
	}
	public void setAcctRef(String acctRef) {
		this.acctRef = acctRef;
	}
	public String getPrefLang() {
		return prefLang;
	}
	public void setPrefLang(String prefLang) {
		this.prefLang = prefLang;
	}
	public String getOpCoID() {
		return opCoID;
	}
	public void setOpCoID(String opCoID) {
		this.opCoID = opCoID;
	}
	public int getServiceId() {
		return serviceId;
	}
	public void setServiceId(int serviceId) {
		this.serviceId = serviceId;
	}
	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this, ToStringStyle.MULTI_LINE_STYLE);
	}
}
