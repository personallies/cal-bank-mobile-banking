package com.itconsortiumgh.cal.utils.dbconverter;

import java.io.IOException;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.itconsortiumgh.cal.model.TransactionDetails;
@Converter(autoApply=true)
public class TransactionDetailsJsonConverter implements AttributeConverter<TransactionDetails, String>{

	@Override
	public String convertToDatabaseColumn(TransactionDetails transactionDetails) {
		ObjectMapper mapper = new ObjectMapper();
		String json = "";
		try {
			json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(transactionDetails);
			return json;
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}		
		return json;
	}

	@Override
	public TransactionDetails convertToEntityAttribute(String databaseValue) {
		TransactionDetails transactionDetails = null;
		 if( databaseValue == null )
	            return null;

	        ObjectMapper mapper = new ObjectMapper();
	        try {
	        	transactionDetails= mapper.readValue(databaseValue, TransactionDetails.class);
			} catch (JsonParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JsonMappingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	        return transactionDetails;
	}
}
