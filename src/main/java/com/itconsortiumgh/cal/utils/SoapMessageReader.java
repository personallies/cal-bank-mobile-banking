package com.itconsortiumgh.cal.utils;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class SoapMessageReader {
	
	public static String findSOAPField(Document doc, String elementName)throws Exception{
		String response = "";
		try{

			NodeList list = doc.getElementsByTagName(elementName);
			if(list.getLength() > 0){
				Node node = list.item(0);
				response = node.getTextContent();
			}else{

			}
		}catch(Exception e){

		}finally{
			//			in.close();
		}
		return response;
	}

	public static String findSOAPField(String request, String elementName) {
		InputStream in = new ByteArrayInputStream(request.getBytes());

		DocumentBuilderFactory docFactory = null;  
		DocumentBuilder docBuilder = null;  
		Document doc = null;  
		docFactory = DocumentBuilderFactory.newInstance();  
		String response = "";

		try{		
			docBuilder = docFactory.newDocumentBuilder();  
			doc = docBuilder.parse(in); 
			
			NodeList list = doc.getElementsByTagName(elementName);
			if(list.getLength() > 0){
				Node node = list.item(0);
				response = node.getTextContent();
			}else{

			}
		}catch(Exception e){

		}finally{
			//			in.close();
		}
		return response;
	}
	public static String findSubXML(String request, String elementName) throws Exception{
		InputStream in = new ByteArrayInputStream(request.getBytes());

		DocumentBuilderFactory docFactory = null;  
		DocumentBuilder docBuilder = null;  
		Document doc = null;  
		docFactory = DocumentBuilderFactory.newInstance();  
		docBuilder = docFactory.newDocumentBuilder();  
		doc = docBuilder.parse(in); 
		String response = "";

		try{		
			NodeList list = doc.getElementsByTagName(elementName);
			if(list.getLength() > 0){
				Node node = list.item(0);
//				response = node.getNodeValue();
				Document newDocument = docBuilder.newDocument();
				Node applesCopy = newDocument.importNode(node, true);
				newDocument.appendChild(applesCopy);
				response = DocumentToStringXMLConverter.convertDocumentToStringXML(newDocument);
			}else{

			}
		}catch(Exception e){

		}finally{
			//			in.close();
		}
		return response;
	}
}