package com.itconsortiumgh.cal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.listener.ChannelTopic;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import org.springframework.web.client.RestTemplate;

import com.itconsortiumgh.cal.redis.queue.AirtimeSubscriber;
import com.itconsortiumgh.cal.redis.queue.BalanceSubscriber;
import com.itconsortiumgh.cal.redis.queue.BankToWalletSubscriber;
import com.itconsortiumgh.cal.redis.queue.BillPaymentSubscriber;
import com.itconsortiumgh.cal.redis.queue.MTNCallbackSubscriber;
import com.itconsortiumgh.cal.redis.queue.FundsTransferSubscriber;
import com.itconsortiumgh.cal.redis.queue.WalletToBankSubscriber;
import com.itconsortiumgh.cal.redis.queue.RequestPaymentSubscriber;
import com.itconsortiumgh.cal.utils.properties.TopicProperties;

@SpringBootApplication
public class CalMobileServicesApplication {

	public static void main(String[] args) {
		SpringApplication.run(CalMobileServicesApplication.class, args);
	}

	@Autowired
	JedisConnectionFactory jedisConnectionFactory;
	@Autowired
	BankToWalletSubscriber bankToWalletSubscriber;
	@Autowired
	BalanceSubscriber balanceSubscriber;
	@Autowired
	FundsTransferSubscriber transferSubscriber;
	@Autowired
	WalletToBankSubscriber walletToBankSubscriber;
	@Autowired
	RequestPaymentSubscriber requestPaymentSubscriber;
	@Autowired
	AirtimeSubscriber airtimeSubscriber;
	@Autowired
	BillPaymentSubscriber billPaymentSubscriber;
	@Autowired
	TopicProperties topicProperties;
	
	
	@Autowired
	MTNCallbackSubscriber mTNCallbackSubscriber;
	@Bean
	RedisTemplate< String, Object > redisTemplate() { 
		final RedisTemplate< String, Object > template =  new RedisTemplate< String, Object >();
		template.setConnectionFactory( jedisConnectionFactory);
		template.setKeySerializer( new StringRedisSerializer());
		template.setDefaultSerializer(new StringRedisSerializer());
		template.setEnableDefaultSerializer(true);
		return template;
	}

	@Bean
	RedisMessageListenerContainer redisContainer() {
		final RedisMessageListenerContainer container = new RedisMessageListenerContainer();
		container.setConnectionFactory(jedisConnectionFactory);
		container.addMessageListener(bankToWalletSubscriber, bankToWalletTopic());
		container.addMessageListener(balanceSubscriber, balanceTopic());
		container.addMessageListener(balanceSubscriber, balanceTopic());
		container.addMessageListener(transferSubscriber, transferTopic());
		container.addMessageListener(walletToBankSubscriber, walletToBankTopic());
		container.addMessageListener(requestPaymentSubscriber, requestPaymentTopic());
		container.addMessageListener(airtimeSubscriber, airtimeTopic());
		container.addMessageListener(mTNCallbackSubscriber, mtnCallbacktopic());
		container.addMessageListener(billPaymentSubscriber, billPaymentTopic());
		return container;
	}

	@Bean
	ChannelTopic bankToWalletTopic() {
		return new ChannelTopic(topicProperties.getBankToWalletTopic());
	}
	@Bean
	ChannelTopic walletToBankTopic() {
		return new ChannelTopic(topicProperties.getWalletToBankTopic());
	}

	@Bean
	ChannelTopic requestPaymentTopic() {
		return new ChannelTopic(topicProperties.getRequestPaymentTopic());
	}

	@Bean
	ChannelTopic balanceTopic() {
		return new ChannelTopic(topicProperties.getBalanceTopic());
	}
	@Bean
	ChannelTopic transferTopic() {
		return new ChannelTopic(topicProperties.getTransferTopic());
	}
	@Bean
	ChannelTopic airtimeTopic() {
		return new ChannelTopic(topicProperties.getAirtimeTopic());
	}
	@Bean
	ChannelTopic mtnCallbacktopic() {
		return new ChannelTopic(topicProperties.getMtnCallbackTopic());
	}
	@Bean
	ChannelTopic billPaymentTopic(){
		return new ChannelTopic(topicProperties.getBillPaymentTopic());
	}

	@Bean
	RestTemplate restTemplate(){
		return new RestTemplate();
	}
}
