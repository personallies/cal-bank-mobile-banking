package com.itconsortiumgh.cal.repository.redis;

import java.util.List;

import com.itconsortiumgh.cal.model.DomainObject;


public interface RedisRepository<V extends DomainObject>{
	 void put(V obj);
	 V get(V key);
	 void delete(V key);
	 List<V> getObjects();
}
