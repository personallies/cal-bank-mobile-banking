package com.itconsortiumgh.cal.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.itconsortiumgh.cal.model.RequestPaymentRequestSdp;
@Repository
public interface RequestPaymentRequestSdpRepository extends CrudRepository<RequestPaymentRequestSdp, Long> {
	public RequestPaymentRequestSdp findByProcessingNumber(String processingNumber);

}
