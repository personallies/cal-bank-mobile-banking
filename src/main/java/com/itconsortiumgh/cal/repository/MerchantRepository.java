package com.itconsortiumgh.cal.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.itconsortiumgh.cal.model.Merchant;

@Repository
public interface MerchantRepository extends CrudRepository<Merchant, Long>{
	Merchant findByid(Long id);

}
