package com.itconsortiumgh.cal.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import com.itconsortiumgh.cal.model.Customer;


@Repository
public interface CustomerRepository extends CrudRepository<Customer, Long>{
	Customer findByMsisdn(String msisdn);
}
