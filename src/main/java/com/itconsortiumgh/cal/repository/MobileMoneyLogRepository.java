package com.itconsortiumgh.cal.repository;

import org.springframework.data.repository.CrudRepository;

import com.itconsortiumgh.cal.model.logs.MobileMoneyLogs;

public interface MobileMoneyLogRepository extends CrudRepository<MobileMoneyLogs, Long> {

}
