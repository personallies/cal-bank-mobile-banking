package com.itconsortiumgh.cal.repository;

import org.springframework.data.repository.CrudRepository;

import com.itconsortiumgh.cal.model.logs.TransactionLogs;



public interface TransactionLogRepository extends CrudRepository<TransactionLogs, Long>{

}
