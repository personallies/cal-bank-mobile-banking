package com.itconsortiumgh.cal.repository;

import org.springframework.data.repository.CrudRepository;

import com.itconsortiumgh.cal.model.CustomerAccount;


public interface CustomerAccountRepository extends CrudRepository<CustomerAccount, Long>{
}