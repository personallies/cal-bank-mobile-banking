package com.itconsortiumgh.cal.model;

import org.springframework.stereotype.Component;

import lombok.Data;

@Component
@Data
public class Account 
{
	protected  String afterDue121To150;
	protected  String afterDue151To180;
	protected  String afterDue180UpField;
	protected  String afterDue1to30Field;
	protected  String afterDue31to60Field;
	protected  String afterDue61to90Field;
	protected  String afterDue91to120Field;
	protected  String currency;
	protected  String currentAmount;
	protected  String defaultCurrencytotalBalance;
	protected  String defaultCurrencyCode;
	protected  String invoicePeriod;
	protected  String isPrimary;
	protected  String lastInvoiceAmount;
	protected  String lastInvoiceDate;
	protected  String methodOfPayment;
	protected  String number;
	protected  String paymentDueDate;
	protected  String segmentation;
	protected  String status;
	protected  String totalBalance;
	protected  String type;
}
