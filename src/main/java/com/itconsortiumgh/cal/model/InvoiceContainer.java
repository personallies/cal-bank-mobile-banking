package com.itconsortiumgh.cal.model;
import java.util.Map;
import lombok.Data;
@Data
public class InvoiceContainer {
	private String invoiceMenu;
	private Map<Integer, String> invoiceMap;

}
