package com.itconsortiumgh.cal.model;

public enum FundsTransferOperation {
	TRANSFER_FOR_MYSELF,
	TRANSFER_FOR_OTHER
}
