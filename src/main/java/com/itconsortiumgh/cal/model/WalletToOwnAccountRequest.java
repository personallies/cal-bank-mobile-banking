package com.itconsortiumgh.cal.model;

import java.math.BigDecimal;

import lombok.Data;
@Data
public class WalletToOwnAccountRequest {
	private String transactionId;
	private BigDecimal amount;
	private String accountLabel;
	private String msisdn;
	private String walletProvider;
	private String pin;
	private Boolean testFlag;
	public WalletToOwnAccountRequest() {
		super();
		// TODO Auto-generated constructor stub
	}
	public WalletToOwnAccountRequest(String transactionId, BigDecimal amount, String accountLabel, String msisdn,
			String walletProvider, String pin, Boolean testFlag) {
		super();
		this.transactionId = transactionId;
		this.amount = amount;
		this.accountLabel = accountLabel;
		this.msisdn = msisdn;
		this.walletProvider = walletProvider;
		this.pin = pin;
		this.testFlag = testFlag;
	}
}
