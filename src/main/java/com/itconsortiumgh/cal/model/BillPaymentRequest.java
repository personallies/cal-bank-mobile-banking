package com.itconsortiumgh.cal.model;
import java.math.BigDecimal;
import lombok.Data;

@Data
public class BillPaymentRequest {
	private String customerNumber;
	private String dataSource;
	private String paymentVendorCode;
	private String transactionNumber;
	private String paymentDescription;
	private String productUserKey;
	private String methodOfPayment;
	private BigDecimal amount;
	private String invoicePeriod;
	private String currency;
	private String businessUnit;
	private String vendorCode;
	private String language;
	private String ipAddress;
	private String pin;



	public BillPaymentRequest(){
	
	}
	
	public BillPaymentRequest(UssdSession ussdSession){
		this.setCustomerNumber(ussdSession.getCustomerNumber());
		this.setInvoicePeriod(ussdSession.getInvoicePeriod());
		this.setPin(ussdSession.getPin());
		
	}
	


}

