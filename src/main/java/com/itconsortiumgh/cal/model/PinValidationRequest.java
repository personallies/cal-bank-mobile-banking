package com.itconsortiumgh.cal.model;

import lombok.Data;

@Data
public class PinValidationRequest {
	private String enteredPin; 
	private String mobileNumber;
	public PinValidationRequest(String enteredPin, String mobileNumber) {
		super();
		this.enteredPin = enteredPin;
		this.mobileNumber = mobileNumber;
	}
	public PinValidationRequest() {
		super();
		// TODO Auto-generated constructor stub
	} 
}
