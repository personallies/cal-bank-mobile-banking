package com.itconsortiumgh.cal.model;

import java.util.Map;

import lombok.Data;
@Data
public class CustomerAccountsContainer {
	private String accountsMenu;
	private Map<String, String> accountsMap;

}
