package com.itconsortiumgh.cal.model;
import java.math.BigDecimal;
import lombok.Data;
@Data
public class GeneralRequests {
	private String mobile;
	private AirtimeNetwork airtimeNetwork;
	private BigDecimal amount;
	private Boolean testFlag=false;
	private String accountLabel;
	private String pin;

}
