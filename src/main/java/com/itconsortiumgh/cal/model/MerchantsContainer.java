package com.itconsortiumgh.cal.model;

import java.util.Map;

import lombok.Data;

@Data
public class MerchantsContainer {
	private String merchantsMenu;
	private Map<Integer, String> merchantsMap;
}
