package com.itconsortiumgh.cal.model;

public enum Action {
	BANK2WALLET,
	WALLET2BANK,
	BALANCE_ENQ,
	AIRTIMEPURCHASE,
	TRANSFER
}
