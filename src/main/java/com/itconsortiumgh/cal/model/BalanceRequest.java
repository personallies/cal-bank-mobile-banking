package com.itconsortiumgh.cal.model;
import org.springframework.beans.factory.annotation.Autowired;

import com.itconsortiumgh.cal.utils.properties.ApplicationProperties;

import lombok.Data;


@Data
public class BalanceRequest {
	@Autowired
	ApplicationProperties applicationProperties;
	
	
	private Boolean testFlag=false;
	private String accountLabel;
	private String mobile; 
	private String pin;

	
	public BalanceRequest()
	{
		
	}
	public BalanceRequest(UssdSession session)
	{
		this.setAccountLabel(session.getDebitAccountLabel());
		this.setMobile(session.getMsisdn());
		this.setPin(session.getPin());
	}
}

