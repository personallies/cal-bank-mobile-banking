package com.itconsortiumgh.cal.model;

public class ClientState {
	public static final String ROOT = "root";
	public static final String ENTER_ACCOUNT_NUMBER = "enterAccountNumber";
	public static final String ENTER_PIN = "enterPin";
	public static final String ENTER_SOURCE_ACCOUNT = "sourceAccount";
	public static final String ENTER_DESTINATION_ACCOUNT = "destAccount";
	public static final String ENTER_AMOUNT = "enterAmount";
	public static final String CHOOSE_ACCOUNT = "chooseAccount";
	public static final String BUY_FOR = "buyFor";
	public static final String ENTER_PHONE_NUMBER = "enterPhoneNumber";
	public static final String PIN_VALIDATION = "pinvalidation";
	public static final String BROKER_API = "brokerApi";
	public static final String CHOOSE_OPERATION = "chooseOperation";
	public static final String BANK_TO_WALLET = "bankToWallet";
	public static final String CHOOSE_WALLET_PROVIDER = "walletProvider";
	public static final String CHOOSE_AIRTIME_NETWORK = "airtimeNetwork";
	public static final String FINAL = "final";
	public static final String TRANSFER_TO = "transferTo";
	public static final String CHOOSE_MERCHANT = "chooseMerchant";
	public static final String ENTER_CUSTOMER = "enterCustomer";
	public static final String DISPLAY_CUSTOMER = "displayCustomer";
	public static final String CHOOSE_INVOICE_PERIOD = "chooseInvoicePeriod";
	public static final String CONFIRM_MESSAGE = "confirmMessage";
}
