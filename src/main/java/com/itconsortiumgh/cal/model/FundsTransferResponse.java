package com.itconsortiumgh.cal.model;

import lombok.Data;

@Data
public class FundsTransferResponse extends TransactionResponse {
	String bankTransactionId;
	String transflowTransactionId;
}
