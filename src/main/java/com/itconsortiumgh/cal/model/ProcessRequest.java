
package com.itconsortiumgh.cal.model;

import java.io.ByteArrayOutputStream;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.itconsortiumgh.cal.utils.Parameter;
import com.itconsortiumgh.cal.utils.ProcessRequestResponse;


/**
 * <p>Java class for processRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="processRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="serviceId" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="parameter" type="{http://b2b.mobilemoney.mtn.zm_v1.0/}parameter" maxOccurs="20" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "processRequest", propOrder = {
		"serviceId",
		"parameter"
})
public class ProcessRequest {
	
//	public static void main(String args[]) throws Exception {
////		
//		
//		String xml = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\"\n" + 
//				"	xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">\n" + 
//				"	<soapenv:Header>\n" + 
//				"		<ns1:NotifySOAPHeader xmlns:ns1=\"http://www.huawei.com.cn/schema/common/v2_1\">\n" + 
//				"			<ns1:traceUniqueID>504021505821207281129180006002</ns1:traceUniqueID>\n" + 
//				"		</ns1:NotifySOAPHeader>\n" + 
//				"	</soapenv:Header>\n" + 
//				"	<soapenv:Body>\n" + 
//				"		<ns2:processRequest xmlns:ns2=\"http://b2b.mobilemoney.mtn.zm_v1.0/\">\n" + 
//				"			<serviceId>101</serviceId>\n" + 
//				"			<parameter>\n" + 
//				"				<name>ProcessingNumber</name>\n" + 
//				"				<value>12121212</value>\n" + 
//				"			</parameter>\n" + 
//				"			<parameter>\n" + 
//				"				<name>senderID</name>\n" + 
//				"				<value>MOM</value>\n" + 
//				"			</parameter>\n" + 
//				"			<parameter>\n" + 
//				"				<name>AcctRef</name>\n" + 
//				"				<value>112233</value>\n" + 
//				"			</parameter>\n" + 
//				"			<parameter>\n" + 
//				"				<name>RequestAmount</name>\n" + 
//				"				<value>1212</value>\n" + 
//				"			</parameter>\n" + 
//				"			<parameter>\n" + 
//				"				<name>paymentRef</name>\n" + 
//				"				<value>12121</value>\n" + 
//				"			</parameter>\n" + 
//				"			<parameter>\n" + 
//				"				<name>ThirdPartyTransactionID</name>\n" + 
//				"				<value>12112</value>\n" + 
//				"			</parameter>\n" + 
//				"			<parameter>\n" + 
//				"				<name>MOMAcctNum</name>\n" + 
//				"				<value>121212</value>\n" + 
//				"			</parameter>\n" + 
//				"			<parameter>\n" + 
//				"				<name>CustName</name>\n" + 
//				"				<value>121212</value>\n" + 
//				"			</parameter>\n" + 
//				"			<parameter>\n" + 
//				"				<name>TXNType</name>\n" + 
//				"				<value>12121</value>\n" + 
//				"			</parameter>\n" + 
//				"			<parameter>\n" + 
//				"				<name>StatusCode</name>\n" + 
//				"				<value>12121</value>\n" + 
//				"			</parameter>\n" + 
//				"			<parameter>\n" + 
//				"				<name>OpCoID</name>\n" + 
//				"				<value>0</value>\n" + 
//				"			</parameter>\n" + 
//				"		</ns2:processRequest>\n" + 
//				"	</soapenv:Body>\n" + 
//				"</soapenv:Envelope>";
//		
//		String processRequestStr = SoapMessageReader.findSubXML(xml, "ns2:processRequest");
////		System.out.println(processRequestStr);
//		
////		StringReader sr = new StringReader(processRequestStr);
//////		JAXBContext jaxbContext = JAXBContext.newInstance(ProcessRequest.class);
////		JAXBContext jaxbContext = JAXBContext.newInstance(ProcessRequestObjectFactory.class);
////		Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
////		ProcessRequest response =  ((JAXBElement<ProcessRequest>)unmarshaller.unmarshal(sr)).getValue();
////		System.out.println(response);
////		JAXBContext jaxbContext = JAXBContext.newInstance(ProcessRequestObjectFactory.class);
////		DocumentType documentType = ((JAXBElement<DocumentType>) jaxbContext.createUnmarshaller().unmarshal(inputStream)).getValue();
//
//		processRequestStr = SoapMessageReader.findSubXML(xml, "ns2:processRequest");
////		System.out.println(processRequestStr);
//		StringReader sr = new StringReader(processRequestStr);
//		//				JAXBContext jaxbContext = JAXBContext.newInstance(ProcessRequest.class);
//		JAXBContext jaxbContext = JAXBContext.newInstance(ProcessRequestObjectFactory.class);
//		Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
//		ProcessRequest processRequest =  ((JAXBElement<ProcessRequest>)unmarshaller.unmarshal(sr)).getValue();
//		ListMapBeanUtility beanUtility = new ListMapBeanUtility();
//		PaymentRequestModel requestModel = beanUtility.listToConfirmationReqModel(processRequest);
//		
//		ProcessRequestResponse response = setupResponse(requestModel);
//		JAXBElement<ProcessRequestResponse>  responseJaxB =new ProcessRequestObjectFactory().createProcessRequestResponse(response);
//		JAXBContext jaxbContext2 = JAXBContext.newInstance(ProcessRequestObjectFactory.class);
//		Marshaller marshaller = jaxbContext2.createMarshaller();
//		marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
//		
//		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
//		marshaller.marshal(responseJaxB, byteArrayOutputStream);
//
//		String charset="UTF-8";
//		String resultingXML = byteArrayOutputStream.toString(charset);
//		System.out.println(resultingXML);
//		
//		
//		
//		
////		ProcessRequest processRequest = new ProcessRequest();
////		processRequest.setServiceId(123);
////		Parameter param1 = new Parameter();
////		param1.setName("hello");
////		param1.setValue("world");
////		processRequest.getParameter().add(param1);
////		JAXBElement<ProcessRequest> req = new ProcessRequestObjectFactory().createProcessRequest(processRequest);
////		JAXBContext jaxbContext = JAXBContext.newInstance(ProcessRequest.class);
////		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
//		
////		jaxbMarshaller.marshal(req, System.out);
//	}

	protected int serviceId;
	protected List<Parameter> parameter;

	/**
	 * Gets the value of the serviceId property.
	 * 
	 */
	public int getServiceId() {
		return serviceId;
	}

	/**
	 * Sets the value of the serviceId property.
	 * 
	 */
	public void setServiceId(int value) {
		this.serviceId = value;
	}

	/**
	 * Gets the value of the parameter property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list,
	 * not a snapshot. Therefore any modification you make to the
	 * returned list will be present inside the JAXB object.
	 * This is why there is not a <CODE>set</CODE> method for the parameter property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * <pre>
	 *    getParameter().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link Parameter }
	 * 
	 * 
	 */
	public List<Parameter> getParameter() {
		if (parameter == null) {
			parameter = new ArrayList<Parameter>();
		}
		return this.parameter;
	}
	@Override
	public String toString() {
		//		return ReflectionToStringBuilder.toString(this, ToStringStyle.MULTI_LINE_STYLE);
		String s = "serviceId = " + this.serviceId;
		s = s+ "\nNOW THE PARAMETERS";
		s = s+ "\n--------------------------------------------";
		for(Parameter p : this.parameter ){
			s = s+ "\nName: " + p.getName() + " value: " + p.getValue();
		}
		s = s+ "\n--------------------------------------------";
		s = s+ "\nEND OF THE PARAMETERS";
		
		s = s+ "\n--------------------------------------------";
		s = s+ "\nALL OTHER THINGS";
		s = s+ReflectionToStringBuilder.toString(this, ToStringStyle.MULTI_LINE_STYLE);
		return s;
	}
	
	private static ProcessRequestResponse  setupResponse(PaymentRequestModel requestModel) {
		ProcessRequestResponse response =  new ProcessRequestObjectFactory().createProcessRequestResponse();
		Parameter processingNumber = new ProcessRequestObjectFactory().createParameter();
		processingNumber.setName("ProcessingNumber");
		processingNumber.setValue(requestModel.getProcessingNumber());
		response.getReturn().add(processingNumber);

		Parameter thirdPartyAcctRef = new ProcessRequestObjectFactory().createParameter();
		thirdPartyAcctRef.setName("ThirdPartyAcctRef");
		thirdPartyAcctRef.setValue("111222");
		response.getReturn().add(thirdPartyAcctRef);

		Parameter senderID = new ProcessRequestObjectFactory().createParameter();
		senderID.setName("senderID");
		senderID.setValue(requestModel.getSenderID());
		response.getReturn().add(senderID);

		Parameter statusCode = new ProcessRequestObjectFactory().createParameter();
		statusCode.setName("StatusCode");
		statusCode.setValue(ResponseCode.CODE_01_OK);
		response.getReturn().add(statusCode);

		Parameter statusDesc = new ProcessRequestObjectFactory().createParameter();
		statusDesc.setName("StatusDesc");
		statusDesc.setValue("successful");
		response.getReturn().add(statusDesc);

		Parameter mOMTransactionID = new ProcessRequestObjectFactory().createParameter();
		mOMTransactionID.setName("MOMTransactionID");
		mOMTransactionID.setValue(requestModel.getProcessingNumber());
		response.getReturn().add(mOMTransactionID);
		return response;
	}
}
