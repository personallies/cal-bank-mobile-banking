package com.itconsortiumgh.cal.model;

public class ResponseCode {
		public static final String CODE_01_OK = "01";
		public static final String CODE_101_INSUFFICIENT_FUNDS = "101";
		public static final String CODE_100_GENERAL_FAILURE= "100";
}
