package com.itconsortiumgh.cal.model;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Component
@Data
public class CustomerDetails 
{
	protected String dateOfBirth;
	protected String firstName;
	protected String mgIndicator;
	protected String priority;
	protected String reference;
	protected String cellNumber;
	protected String correspondence;
	protected String emailAddress;
	protected String faxNumber;
	protected String homeTelephone;
	protected String initials;
	protected String language;
	protected String number;
	protected String salutation;
	protected String status;
	protected String surname;
	protected String type;
	protected String typeName;
	@JsonIgnore
	@JsonProperty(value="vATRegNo")
	protected String vATRegNo;
	protected String workTelephone;
}
