package com.itconsortiumgh.cal.model;

import java.math.BigDecimal;

import lombok.Data;

@Data
public class BalanceResponse {
	private String accountName;  
	private String currency;
	private BigDecimal availableBalance;
	private String status;
	private String responseCode;
	private String responseMessage;
}
