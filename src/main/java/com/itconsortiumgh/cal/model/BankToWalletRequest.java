package com.itconsortiumgh.cal.model;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import com.itconsortiumgh.cal.utils.properties.ApplicationProperties;

import lombok.Data;
@Data
public class BankToWalletRequest {
	@Autowired
	ApplicationProperties applicationProperties;
	private String transactionId;
	private BigDecimal amount;
	private String debitAccountLabel;
	private String msisdn;
	private String walletProvider;
	private String pin;
	private Boolean testFlag;

	public BankToWalletRequest() {
		super();
	}	// TODO Auto-generated constructor stub

	public BankToWalletRequest(UssdSession ussdSession) {
//
		this.transactionId = ussdSession.getTransactionId();
		this.amount = ussdSession.getAmount();
		this.debitAccountLabel = ussdSession.getDebitAccountLabel();
		this.msisdn = ussdSession.getMsisdn();
		this.walletProvider = ussdSession.getWalletProvider();
		this.pin = ussdSession.getPin();
		//this.testFlag = applicationProperties.getTestFlag();
	}
	
	
	
	
	
	
}
