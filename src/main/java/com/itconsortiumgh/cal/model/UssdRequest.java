/**
 * 
 */
package com.itconsortiumgh.cal.model;

import lombok.Data;

@Data
public final class UssdRequest {
//	public static void main(String []args){
//		UssdRequest request = new UssdRequest();
//		request.setClientState(ClientState.ROOT);
//		request.setMessage("3");
//		request.setOperator("MTN");
//		request.setMobile("024675857");
//		request.setSessionId("1111");
//		request.setType(MessageType.RESPONSE);
//		System.out.println(JsonUtility.toJson(request));
//	}

    private String mobile;
    private String sessionId;
    private String type;
    private String message;
    private String operator;
    private Integer sequence;
    private String clientState;
    private String serviceCode;
  
}