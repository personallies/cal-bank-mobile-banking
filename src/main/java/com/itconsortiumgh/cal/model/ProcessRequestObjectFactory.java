
package com.itconsortiumgh.cal.model;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;

import com.itconsortiumgh.cal.utils.Parameter;
import com.itconsortiumgh.cal.utils.ProcessRequestResponse;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.itconsortiumgh.mtnmm.lonestar.multichoice package. 
 * <p>An ProcessRequestObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ProcessRequestObjectFactory {
//	<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:b2b="http://b2b.mobilemoney.mtn.zm_v1.0/">

    private final static QName _ProcessRequest_QNAME = new QName("http://b2b.mobilemoney.mtn.zm_v1.0/", "processRequest");
    private final static QName _ProcessRequestResponse_QNAME = new QName("http://b2b.mobilemoney.mtn.zm_v1.0/", "processRequestResponse");

    /**
     * Create a new ProcessRequestObjectFactory that can be used to create new instances of schema derived classes for package: com.itconsortiumgh.mtnmm.lonestar.multichoice
     * 
     */
    public ProcessRequestObjectFactory() {
    }

    /**
     * Create an instance of {@link ProcessRequestResponse }
     * 
     */
    public ProcessRequestResponse createProcessRequestResponse() {
        return new ProcessRequestResponse();
    }

    /**
     * Create an instance of {@link ProcessRequest }
     * 
     */
    public ProcessRequest createProcessRequest() {
        return new ProcessRequest();
    }

    /**
     * Create an instance of {@link Parameter }
     * 
     */
    public Parameter createParameter() {
        return new Parameter();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProcessRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://b2b.mobilemoney.mtn.zm_v1.0/", name = "processRequest")
    public JAXBElement<ProcessRequest> createProcessRequest(ProcessRequest value) {
        return new JAXBElement<ProcessRequest>(_ProcessRequest_QNAME, ProcessRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProcessRequestResponse }{@code >}}
     * 
     */
//    <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:b2b="http://b2b.mobilemoney.mtn.zm_v1.0/">

//    @XmlElementDecl(namespace = "http://b2b.mobilemoney.mtn.zm_v1.0/", name = "processRequestResponse")
    @XmlElementDecl(namespace = "http://schemas.xmlsoap.org/soap/envelope/", name = "processRequestResponse")
    public JAXBElement<ProcessRequestResponse> createProcessRequestResponse(ProcessRequestResponse value) {
        return new JAXBElement<ProcessRequestResponse>(_ProcessRequestResponse_QNAME, ProcessRequestResponse.class, null, value);
    }

}
