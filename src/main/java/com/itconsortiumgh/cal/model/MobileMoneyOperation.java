package com.itconsortiumgh.cal.model;

public enum MobileMoneyOperation {
	BANK_TO_WALLET,
	WALLET_TO_OWN_ACCOUNT,
	WALLET_TO_OTHER_ACCOUNT

}
