package com.itconsortiumgh.cal.model;
public enum SessionStatus {
	ACTIVE,
	INACTIVE
}