package com.itconsortiumgh.cal.model;

import lombok.Data;
@Data
public class MDRPResponse
{
	private String momTransactionId;
	private String responseCode;
	private String responseMessage;
	private String ProcessingNumber;
	private String subscriberID;
	private String amount;
	private String thirdpartyAccountRef;
}