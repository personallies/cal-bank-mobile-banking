package com.itconsortiumgh.cal.model;

import lombok.Data;

@Data
public class BankDepositRequest {
	private String transflowTransactionId;
	private String mobileNumber;
	private String amount;
	private String momTransactionId;
	private String responseCode;
	private String responseMessage;
}
