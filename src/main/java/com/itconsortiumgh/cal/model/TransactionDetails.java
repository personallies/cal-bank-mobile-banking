package com.itconsortiumgh.cal.model;

import lombok.Data;

@Data
public class TransactionDetails {
	private String sourceBranch;
	private String sourceAccount;
	private String destBranch;
	private String destAccount;
	private String amount;
}
