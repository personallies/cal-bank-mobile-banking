/**
 * 
 */
package com.itconsortiumgh.cal.model;

import lombok.Data;

@Data
public final class UssdResponse {

    private String type;
    private String message;
    private String clientState;
  }
