package com.itconsortiumgh.cal.model.logs;

import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.itconsortiumgh.cal.model.Action;
import com.itconsortiumgh.cal.model.BankToWalletRequest;
import com.itconsortiumgh.cal.model.MobileMoneyOperation;
import com.itconsortiumgh.cal.model.TransactionType;
import com.itconsortiumgh.cal.model.UssdSession;

import lombok.Data;

@Table
@Entity
@Data
public class MobileMoneyLogs {
	private static final long serialVersionUID = 1L; 
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	// START TRANSACTIONAL DATA
	@Column
	@Temporal(TemporalType.TIMESTAMP)
	private Date created;
	@Column
	@Temporal(TemporalType.TIMESTAMP)
	private Date updated;
	
	@Column(precision=12, scale=2)
	private BigDecimal amount;

	@Column(length=50)
	@Enumerated(EnumType.STRING)
	private MobileMoneyOperation mobileMoneyOperation;
	@Column(length=15)
	@Enumerated(EnumType.STRING)
	private Action action;
	@Column(length=50)
	private String transactionId;
	@Column(length=50)
	private String momTransactionId;
	@Column(length=50)
	private String responseCode;
	@Column(length=50)
	private String responseMessage;
	@Column(length=50)
	private String walletProvider;
	@Column(length=50)
	private String pin;
	
	
	public MobileMoneyLogs(UssdSession ussdSession){
		this.setAmount(ussdSession.getAmount());
		this.setWalletProvider(ussdSession.getWalletProvider());
		this.setTransactionId(ussdSession.getTransactionId());
		this.setMobileMoneyOperation(ussdSession.getMobileMoneyOperation());
		this.setPin(ussdSession.getPin());
		this.setCreated(new Date());
		
	}
	
	
	
	
	public BankToWalletRequest bankToWalletRequest(UssdSession ussdSession)
	{
		BankToWalletRequest bankToWalletRequest=new BankToWalletRequest();
		bankToWalletRequest.setAmount(ussdSession.getAmount());
		bankToWalletRequest.setMsisdn(ussdSession.getMsisdn());
	    bankToWalletRequest.setWalletProvider(ussdSession.getWalletProvider());
	    bankToWalletRequest.setDebitAccountLabel(ussdSession.getDebitAccountLabel());
	    bankToWalletRequest.setPin(ussdSession.getPin());
		return bankToWalletRequest;
	}
	
	
public MobileMoneyLogs(){
		
		
		
		
	}
	
	
	
	
	
	
	
}
