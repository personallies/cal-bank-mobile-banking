package com.itconsortiumgh.cal.model.logs;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.itconsortiumgh.cal.model.AirtimeNetwork;
import com.itconsortiumgh.cal.model.AirtimePurchaseRequest;
import com.itconsortiumgh.cal.model.BalanceRequest;
import com.itconsortiumgh.cal.model.BillPaymentRequest;
import com.itconsortiumgh.cal.model.FundsTransferRequest;
import com.itconsortiumgh.cal.model.TransactionType;
import com.itconsortiumgh.cal.model.UssdSession;
import com.itconsortiumgh.cal.model.WalletToBankRequest;
import com.itconsortiumgh.cal.utils.JsonUtility;
import com.itconsortiumgh.cal.utils.TransactionIdGenerator;
import com.itconsortiumgh.cal.utils.properties.ApplicationProperties;
import com.itconsortiumgh.cal.utils.properties.MenuProperties;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Table
@Data
@Entity
@Slf4j
public class TransactionLogs {
	
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	// START TRANSACTIONAL DATA
	@Column
	@Temporal(TemporalType.TIMESTAMP)
	private Date created;
	@Column
	@Temporal(TemporalType.TIMESTAMP)
	private Date updated;
	@Column(length = 50)
	private String msidn;
	@Column(precision = 12, scale = 2)
	private BigDecimal amount;
	@Column(length = 50)
	@Enumerated(EnumType.STRING)
	private TransactionType transactionType;
	@Column(length = 50)
	private String transactionId;
	@Column(length = 50)
	private String bankTransactionId;
	@Column(length = 50)
	private String thirdPartyTransactionId;
	@Column(length = 15)
	@Enumerated(EnumType.STRING)
	private AirtimeNetwork network;
	@Column(length = 50)
	private String status;
	@Column(length = 50)
	private String statusDescription;
	@Column(length = 50)
	private String comment;
	@Column(length = 50)
	private String creditAccountLabel;
	@Column(length = 50)
	private String creditAccountNumber;
	@Column(length = 50)
	private String debitAccountLabel;
	@Column(length = 50)
	private String merchantAccountLabel;
	@Column(length = 50)
	private String pin;
	@Column(length = 50)
	private String customerNumber;
	@Column(length = 50)
	private String InvoicePeriod;
	@Transient
	private UssdSession ussdSession;
	@Transient
	private ApplicationProperties applicationProperties;

	public TransactionLogs() {

	}

	public TransactionLogs(UssdSession session) {
		this.setAmount(session.getAmount());
		this.setMsidn(session.getMsisdn());
		this.setPin(session.getPin());
		this.setTransactionId(session.getSessionId());
		this.setDebitAccountLabel(session.getDebitAccountLabel());
		TransactionType transactionType = session.getTransactionType();
		this.setCreated(new Date());
		this.setTransactionType(transactionType);
		switch (transactionType) {
	    case AIRTIME:
        setAirtimeRequest(session);
		case TRANSFER:
		setFundsTransferRequest(session);
		case BILL_PAYMENT:
		setdstvBillPaymentRequest(session);
		case BALANCE:
		setBalanceRequest(session);	
		case REQ_CHEQUE_BK:
		}
	}
	

	public AirtimePurchaseRequest setAirtimeRequest(UssdSession session) {
		this.setAmount(session.getAmount());
		this.setNetwork(session.getAirtimeNetwork());
		this.setDebitAccountLabel(session.getDebitAccountLabel());
		AirtimePurchaseRequest airtimePurchaseRequest = new AirtimePurchaseRequest();
		airtimePurchaseRequest.setAmount(session.getAmount());
		airtimePurchaseRequest.setAirtimeNetwork(session.getAirtimeNetwork());
		airtimePurchaseRequest.setMobile(session.getMsisdn());
		this.setUssdSession(session);
		return airtimePurchaseRequest;
	}

	
	public BalanceRequest setBalanceRequest(UssdSession session){
		BalanceRequest balanceRequest=new BalanceRequest();
        balanceRequest.setAccountLabel(session.getDebitAccountLabel());
        balanceRequest.setMobile(session.getMsisdn());
     //   balanceRequest.setPin(session.getPin());
        this.setUssdSession(session);
		return balanceRequest;
	}
	
	public BillPaymentRequest setdstvBillPaymentRequest(UssdSession session){
		BillPaymentRequest billPaymentRequest=new BillPaymentRequest();
		this.setCustomerNumber(session.getCustomerNumber());
		this.setInvoicePeriod(session.getInvoicePeriod());
		this.setTransactionId(session.getSessionId());
		this.setDebitAccountLabel(session.getDebitAccountLabel());
		this.setMerchantAccountLabel(session.getMerchantAccountLabel());
		billPaymentRequest.setCustomerNumber(session.getCustomerNumber());
		billPaymentRequest.setInvoicePeriod(session.getInvoicePeriod());
        this.setUssdSession(session);
		return billPaymentRequest;
	}
	
	
	public FundsTransferRequest setFundsTransferRequest(UssdSession ussdSession) {
		FundsTransferRequest fundsTransferRequest = new FundsTransferRequest();
		MenuProperties menuProperties = new MenuProperties();
		ApplicationProperties applicationProperties = new ApplicationProperties();
		this.setTransactionId(TransactionIdGenerator.nextId().toString());
		fundsTransferRequest.setDebitAccountLabel(ussdSession.getDebitAccountLabel());
		fundsTransferRequest.setAmount(ussdSession.getAmount());
		log.info("destinationAccount >>>{}", fundsTransferRequest.getDebitAccountLabel());
		fundsTransferRequest.setCreditAccountNumber(ussdSession.getCreditAccountNumber());
		this.setCreditAccountNumber(ussdSession.getCreditAccountNumber());
		log.info("creditAccount >>>{}", fundsTransferRequest.getCreditAccountNumber());
		fundsTransferRequest.setMsisdn(ussdSession.getMsisdn());
		fundsTransferRequest.setNarration(menuProperties.getTransferNarration());
		fundsTransferRequest.setPin(ussdSession.getPin());
		fundsTransferRequest.setTestFlag(applicationProperties.getTestFlag());
		fundsTransferRequest.setTransflowTransactionId(TransactionIdGenerator.nextId().toString());
		String message = JsonUtility.toJson(fundsTransferRequest);
		log.info("transferRequest {}", message);

		return fundsTransferRequest;

	}

	public WalletToBankRequest setWalletToBankRequest(UssdSession ussdSession) {
		WalletToBankRequest walletToBankRequest = new WalletToBankRequest();

		walletToBankRequest.setAmount(ussdSession.getAmount());
		walletToBankRequest.setCreditAccountNumber(ussdSession.getCreditAccountNumber());
		walletToBankRequest.setDebitAccountLabel(ussdSession.getDebitAccountLabel());

		return walletToBankRequest;
	}

}
