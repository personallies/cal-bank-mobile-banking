package com.itconsortiumgh.cal.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Table
@Entity
public class RequestPaymentRequestSdp
{
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(length = 20)
	private String processingNumber;
	@Column(length = 20)
	private String imsiNum;
	@Column(length = 20)
	private String msisdn;
	@Column(length = 100)
	private String spId;
	@Column(length = 100)
	private String spPassword;
	@Column(length = 100)
	private String serviceId;
	@Column(length = 20)
	private String amount;
	@Column(length = 100)
	private String acctRef;
	@Column(length = 20)
	private String balance;
	@Column(length = 20)
	private String minDueAmount;
	@Column(length = 500)
	private String narration;
	@Column(length = 20)
	private String currency;

	//  public static void main(String[] args)
	//  {
	//    RequestPaymentRequestSdp bean = new RequestPaymentRequestSdp();
	//    try {
	//      bean.setAcctRef("test");
	//      bean.setAmount("100");
	//      bean.setBalance("100");
	//      bean.setImsiNum("234");
	//      bean.setMinDueAmount("100");
	//      bean.setMsisdn("234123");
	//      bean.setNarration("testnarration");
	//      bean.setProcessingNumber("ff33");
	//      bean.setServiceId("sdpitcbpp_push");
	//      bean.setSpId("2330110002172");
	//      bean.setSpPassword("7CE7DD6CF8D98BCDFB4E8EBD41D62195");
	//      ObjectMapper mapper = new ObjectMapper();
	//		System.out.println(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(bean));
	//    }
	//    catch (Exception e) {
	//      e.printStackTrace();
	//    }
	//  }

	
}