package com.itconsortiumgh.cal.model;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class PaymentRequestModel {
	private String processingNumber,senderID,acctRef,
	requestAmount,paymentRef, thirdPartyTransactionID,mOMAcctNum,custName,tXNType,statusCode,opCoID;
	private String serviceId;
	
	public String getSenderID() {
		return senderID;
	}
	public void setSenderID(String senderID) {
		this.senderID = senderID;
	}
	public String getAcctRef() {
		return acctRef;
	}
	public void setAcctRef(String acctRef) {
		this.acctRef = acctRef;
	}
	public String getRequestAmount() {
		return requestAmount;
	}
	public void setRequestAmount(String requestAmount) {
		this.requestAmount = requestAmount;
	}
	public String getPaymentRef() {
		return paymentRef;
	}
	public void setPaymentRef(String paymentRef) {
		this.paymentRef = paymentRef;
	}
	public String getThirdPartyTransactionID() {
		return thirdPartyTransactionID;
	}
	public void setThirdPartyTransactionID(String thirdPartyTransactionID) {
		this.thirdPartyTransactionID = thirdPartyTransactionID;
	}
	public String getmOMAcctNum() {
		return mOMAcctNum;
	}
	public void setmOMAcctNum(String mOMAcctNum) {
		this.mOMAcctNum = mOMAcctNum;
	}
	public String getCustName() {
		return custName;
	}
	public void setCustName(String custName) {
		this.custName = custName;
	}
	public String gettXNType() {
		return tXNType;
	}
	public void settXNType(String tXNType) {
		this.tXNType = tXNType;
	}
	public String getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	public String getOpCoID() {
		return opCoID;
	}
	public void setOpCoID(String opCoID) {
		this.opCoID = opCoID;
	}
	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this, ToStringStyle.MULTI_LINE_STYLE);
	}
	public String getProcessingNumber() {
		return processingNumber;
	}
	public void setProcessingNumber(String processingNumber) {
		this.processingNumber = processingNumber;
	}
	public String getServiceId() {
		return serviceId;
	}
	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}
}
