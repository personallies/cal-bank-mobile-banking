package com.itconsortiumgh.cal.model;

import java.math.BigDecimal;

import lombok.Data;

@Data
public class AirtimePurchaseRequest extends GeneralRequests {
	
	private String mobile;
	private AirtimeNetwork airtimeNetwork;
	private BigDecimal amount;
//	private String apiToken;
//	private String clientId;
//	private String clientSecret;
	
	public AirtimePurchaseRequest(UssdSession ussdSession){
		this.setAirtimeNetwork(ussdSession.getAirtimeNetwork());
		this.setAmount(ussdSession.getAmount());
		this.setMobile(ussdSession.getMsisdn());
	}
	
	public AirtimePurchaseRequest(){
		
	}
}
