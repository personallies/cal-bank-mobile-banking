package com.itconsortiumgh.cal.model;
import lombok.Data;
@Data
public class NumberPortabilityContainer {
	private String network;
	private AirtimeNetwork airtimeNetwork;
}
