package com.itconsortiumgh.cal.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.itconsortiumgh.cal.utils.dbconverter.PasswordConverter;

import lombok.Data;



@Entity
@Table
@Data
public class Customer {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id")
	private Long id;

	@OneToMany(mappedBy="customer", cascade=CascadeType.ALL)
	@JsonIgnore
	private List<CustomerAccount> customerAccounts = new ArrayList<CustomerAccount>();

	@Column(length=30)
	private String msisdn;
	@Column
	@Enumerated(EnumType.STRING)
	private CustomerStatus customerStatus;
	@Column(length = 100)
	private String firstName;
	@Column(length = 100)
	private String lastName;
	@Column(length = 100)
	private String otherNames;
}