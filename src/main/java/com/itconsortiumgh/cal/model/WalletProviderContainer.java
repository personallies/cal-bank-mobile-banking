package com.itconsortiumgh.cal.model;

import java.util.Map;

import lombok.Data;
@Data
public class WalletProviderContainer {
	private String walletProviderMenu;
	private Map<Integer, String> walletProviderMap;
}
