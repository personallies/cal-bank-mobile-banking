package com.itconsortiumgh.cal.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Entity
@Table
@Data
public class Merchant {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id")
	private Long id;
	@OneToMany(mappedBy="merchant", cascade=CascadeType.ALL)
	@JsonIgnore
	private List<MerchantAccount> merchantAccounts = new ArrayList<MerchantAccount>();
	@Column(length=30)
	private String productName;
}
