package com.itconsortiumgh.cal.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Data;


@Entity
@Table
@Data
public class UssdSession implements DomainObject{
	
	//Redis specific details
	private static final long serialVersionUID = 1L; 
	public static final String OBJECT_KEY = "USSD_SESSION"; 
	
	@Override
	public String getKey() {
		StringBuilder sb = new StringBuilder();
		return sb.append(sessionId).toString();
//		return senderAddress+dialogId;
	}
	public void setKey(String sessionId) {
		this.sessionId = sessionId;
	}
	@Override
	public String getObjectKey() {
		return OBJECT_KEY;
	}
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	// START TRANSACTIONAL DATA
	@Column
	@Temporal(TemporalType.TIMESTAMP)
	private Date created;
	@Column
	@Temporal(TemporalType.TIMESTAMP)
	private Date updated;
	@Column
	@Enumerated(EnumType.STRING)
	private SessionStatus status;
	//END TRANSACTIONAL DATA

	//START USSD SPECIFIC DATA
	@Column(length=5)
	private String newRequest;
	@Column(length=10)
	private String mode;
	@Column(length=12)
	private String msisdn;
	@Column(length=50)
	private String sessionId;
	@Column(length=20)
	private String subscriberInput;

	@Column
	private BigDecimal amount;

	@Column(length=20)
	private String clientState;

	@Column(length=20)
	private String outMenu;
	
	@Column(length=20)
	private String processingEndpoint;

	@Column
	@Enumerated(EnumType.STRING)
	private TransactionType transactionType;
	
	@Column(length=20)
	@Enumerated(EnumType.STRING)
	private AirtimeNetwork airtimeNetwork;
	
	@Column(length=15)
	private String accountTypeStatus;

	@Column(length=15)
	private String walletProvider;
	
	
	@Column(length=15)
	private String operator;
	
	@Column(length=15)
	private String network;
	
	@Column(length=15)
	private String pin;
	@Column(length=300)
	private TransactionDetails transactionDetails;

	@Column(length=100)
	private String account;
	
	@Column(length=100)
	private String invoicePeriod;
	
	@Column(length=15)
	private Boolean testFlag;
	@Column(length=100)
	private String accountOperation;
	
	@Column(length=200)
	private String accountLabelMapJson;
	@Column(length=200)
	private String walletProviderMap;

	@Column(length=200)
	private String transactionId;
	@Column(length=40)
	@Enumerated(EnumType.STRING)
	private MobileMoneyOperation mobileMoneyOperation;
	@Column(length=40)
	@Enumerated(EnumType.STRING)
	private FundsTransferOperation fundsTransferOperation;

	@Column(length=40)
	private String accountLabel;//For check balance
	@Column(length = 40)
	private String merchantAccountLabel;
	@Column(length = 40)
	private String customerNumber;
	@Column(length = 40)
	private String creditAccountLabel;//destination account label
	@Column(length = 40)
	private String creditAccountNumber; //destination account number 
	@Column(length = 40)
	private String debitAccountLabel;//source account label
	@Column(length = 40)
	private String debitAccountNumber;//source account number
	@Column(length = 40)
	private String transflowTransactionId;
	
}