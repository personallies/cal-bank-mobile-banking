package com.itconsortiumgh.cal.model;

import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Data;

@Table
@Entity
public class TransactionLog {
	private static final long serialVersionUID = 1L; 
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	// START TRANSACTIONAL DATA
	@Column
	@Temporal(TemporalType.TIMESTAMP)
	private Date created;
	@Column
	@Temporal(TemporalType.TIMESTAMP)
	private Date updated;
	
	@Column(precision=12, scale=2)
	private BigDecimal amount;
	
	@Column(length=15)
	@Enumerated(EnumType.STRING)
	private Action action;
	@Column(length=50)
	private String processingCode;
	@Column(length=50)
	private String momTransactionId;
	@Column(length=50)
	private String responseCode;
}
