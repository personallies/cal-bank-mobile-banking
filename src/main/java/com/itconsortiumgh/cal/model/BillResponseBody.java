package com.itconsortiumgh.cal.model;

import java.util.List;

import lombok.Data;

@Data
public class BillResponseBody {
	private List<Account> account;
	private CustomerDetails customerDetails;

}
