package com.itconsortiumgh.cal.model;
import java.math.BigDecimal;
import lombok.Data;
@ Data
public class WalletToBankRequest {
	private Boolean testFlag;
	private String transactionId;
	private BigDecimal amount;
	private String mobile;
	private String debitAccountLabel;
	private String creditAccountLabel;
	private MobileMoneyOperation mobileMoneyOperation;
	private String walletProvider;
	private String creditAccountNumber;
 

//	private String destinationBranch;
//	private String momTransactionId;
//	private String paymentRef;
//	private String sessionId;
//	private String responseCode;
//	private String responseMessage;
    
    
	public WalletToBankRequest(){
		super();
	}

	
	public WalletToBankRequest(String transactionId, BigDecimal amount, String debitAccountLabel, String mobile,
			String walletProvider,MobileMoneyOperation mobileMoneyOperation,String destAcctNumber){
		super();
		this.transactionId = transactionId;
		this.amount = amount;
		this.debitAccountLabel = debitAccountLabel;
		this.mobile = mobile;
		this.walletProvider = walletProvider;
		this.mobileMoneyOperation = mobileMoneyOperation;
	
	}
	
	
	
	
}

 
