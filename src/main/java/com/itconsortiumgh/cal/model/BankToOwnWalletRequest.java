package com.itconsortiumgh.cal.model;

import java.math.BigDecimal;

import lombok.Data;
@Data
public class BankToOwnWalletRequest {
	private String transactionId;
	private BigDecimal amount;
	private String accountLabel;
	private String mobile;
	private String walletProvider;
	private String pin;
	public BankToOwnWalletRequest() {
		super();
		// TODO Auto-generated constructor stub
	}
	public BankToOwnWalletRequest(String transactionId, BigDecimal amount, String accountLabel, String mobile,
			String walletProvider,String pin) {
		super();
		this.transactionId = transactionId;
		this.amount = amount;
		this.accountLabel = accountLabel;
		this.mobile = mobile;
		this.walletProvider = walletProvider;
		this.pin = pin;

	}
	
}
