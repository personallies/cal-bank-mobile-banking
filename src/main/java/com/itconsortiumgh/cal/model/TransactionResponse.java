package com.itconsortiumgh.cal.model;

import lombok.Data;

@Data
public class TransactionResponse {
	
	private String responseCode;
	private String responseMessage;
	private String thirdPartyTransactionId;
	public TransactionResponse(String responseCode, String responseMessage) {
		super();
		this.responseCode = responseCode;
		this.responseMessage = responseMessage;
	}
	public TransactionResponse() {
		super();
		// TODO Auto-generated constructor stub
	}

}
