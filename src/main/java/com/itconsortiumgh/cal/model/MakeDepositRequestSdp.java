package com.itconsortiumgh.cal.model;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.Data;
@Data
public class MakeDepositRequestSdp {
	  private String processingNumber;
	  private String imsiNum;
	  private String msisdn;
	  private String amount;
	  private String serviceId;
	  private String spId;
	  private String spPassword;
	  private String narration;
	  private String currency;
	
	public static void main(String []args) throws JsonProcessingException {
		MakeDepositRequestSdp md = new MakeDepositRequestSdp("1002", "233243576745", 
				"1", "sdpitcbpp_push", "2330110002172", "7CE7DD6CF8D98BCDFB4E8EBD41D62195", "rest test", "GHS");
//		RequestPaymentRequestSdp rp = new RequestPaymentRequestSdp("1312312", "233243576745", "1000", "serviceId", "the spid", "thespPassword", "accrRef", "GHS");
//		RequestPaymentRequestSdp rp = new RequestPaymentRequestSdp();
		ObjectMapper mapper = new ObjectMapper();
		System.out.println(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(md));
	}
	  
	public MakeDepositRequestSdp() {
		super();
		// TODO Auto-generated constructor stub
	}
	public MakeDepositRequestSdp(String processingNumber, String msisdn, String amount, String serviceId,
			String spId, String spPassword, String narration, String currency) {
		super();
		this.processingNumber = processingNumber;
		this.msisdn = msisdn;
		this.amount = amount;
		this.serviceId = serviceId;
		this.spId = spId;
		this.spPassword = spPassword;
		this.narration = narration;
		this.currency = currency;
	}
	
}
