package com.itconsortiumgh.cal.model;

import java.math.BigDecimal;

import lombok.Data;
@Data
public class WalletToOtherAccountRequest {
	private String transactionId;
	private BigDecimal amount;
	private String accountLabel;
	private String msisdn;
	private String walletProvider;
	private String pin;
	private Boolean testFlag;
	private String destinationAccount;
	private String destinationBranch;
	public WalletToOtherAccountRequest() {
		super();
		// TODO Auto-generated constructor stub
	}
	public WalletToOtherAccountRequest(String transactionId, BigDecimal amount, String accountLabel, String msisdn,
			String walletProvider, String pin, Boolean testFlag) {
		super();
		this.transactionId = transactionId;
		this.amount = amount;
		this.accountLabel = accountLabel;
		this.msisdn = msisdn;
		this.walletProvider = walletProvider;
		this.pin = pin;
		this.testFlag = testFlag;
	}
}
