package com.itconsortiumgh.cal.model;

import java.math.BigDecimal;

import lombok.Data;

@Data
public class FundsTransferRequest {
	private Boolean testFlag=false;
	private String creditAccountLabel;//for transfer to myself
	private String creditAccountNumber; //for transfer to other
	private String debitAccountLabel; 
	private String pin;
	private BigDecimal amount; 
	private String msisdn; 
	private String narration;
	private String transflowTransactionId;
	
//	private String userID; 
//	private String password;


	
}
