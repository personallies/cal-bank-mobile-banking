package com.itconsortiumgh.cal.model;

public enum TransactionType {
	BALANCE, 
	TRANSFER, 
	AIRTIME, 
	MOBILE_MONEY,
	BILL_PAYMENT,
	REQ_CHEQUE_BK,
	REQ_STATEMENT

}