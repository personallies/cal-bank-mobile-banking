package com.itconsortiumgh.cal.model;

public enum AirtimeNetwork {
	MTN,
	Airtel,
	Vodafone,
	Tigo,
	Expresso,
	Glo
}
