package com.itconsortiumgh.cal.model;
public enum CustomerStatus {
	ACTIVE,
	INACTIVE
}