package com.itconsortiumgh.cal.model;

import java.math.BigDecimal;

import org.springframework.stereotype.Component;

import lombok.Data;
@Component
@Data
public class DialectPaymentBean {
	private BigDecimal amount;
	private String responseCode;
	private String responseMessage;
	private String momTransactionId;
	private String transflowTransactionId;
	private String accountRef;
	private String mobileNumber;
	private String paymentRef;
	private String sourceIp;
}