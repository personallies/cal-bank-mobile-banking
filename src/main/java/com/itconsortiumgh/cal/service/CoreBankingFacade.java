package com.itconsortiumgh.cal.service;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.itconsortiumgh.cal.utils.properties.ApplicationProperties;

@Service
public class CoreBankingFacade {
	@Autowired
	ApplicationProperties applicationProperties;
	
	public BigDecimal getAccountBalance(String accountNumber, String branch) {
		BigDecimal balance = new BigDecimal("0");
		if(applicationProperties.isServiceIdInUATMode()) {
			balance = new BigDecimal("100");
		}else {
			//TO DO Implement the call to core-banking to determine balance on account
		}
		return balance;
	}
	
	
	
}
