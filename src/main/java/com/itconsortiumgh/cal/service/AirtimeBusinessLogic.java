package com.itconsortiumgh.cal.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.itconsortiumgh.cal.model.ClientState;
import com.itconsortiumgh.cal.model.UssdRequest;
import com.itconsortiumgh.cal.model.UssdResponse;
import com.itconsortiumgh.cal.model.UssdSession;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class AirtimeBusinessLogic {
	@Autowired
	AirtimeUserInputProcessor airtimeUserInputProcessor;

	public UssdResponse processRequest(UssdSession ussdSession, UssdRequest ussdRequest) {
		UssdResponse ussdResponse = new UssdResponse();
		String mobileNumber = ussdRequest.getMobile();
		String userInput = ussdRequest.getMessage();
		String clientState = ussdRequest.getClientState();

		switch (clientState) {
		case ClientState.ROOT:
			log.info("going to ask client who we are buying for");
			ussdResponse = airtimeUserInputProcessor.askUserToChooseBuyFor(ussdSession, mobileNumber);
			return ussdResponse;

		case ClientState.BUY_FOR:
			ussdResponse = airtimeUserInputProcessor.askUserForNetwork(ussdSession, ussdRequest);
			return ussdResponse;

		case ClientState.ENTER_PHONE_NUMBER:
			log.info(
					"---------------------------------we are being asked to enter the phone number---------------------------------");
			ussdResponse = airtimeUserInputProcessor.askForChooseAccount(ussdSession, userInput, mobileNumber);
			return ussdResponse;
			
		case ClientState.CHOOSE_ACCOUNT:
			ussdResponse = airtimeUserInputProcessor.askForAmount(ussdSession, userInput, mobileNumber);
			return ussdResponse;

		case ClientState.ENTER_AMOUNT:
			ussdResponse = airtimeUserInputProcessor.displayConfirmMessage(ussdSession, userInput, mobileNumber);
			return ussdResponse;
			
		case ClientState.CONFIRM_MESSAGE:
			ussdResponse = airtimeUserInputProcessor.askForPin(ussdSession, userInput);
			return ussdResponse;
			

		case ClientState.ENTER_PIN:
			log.info("=================we are going to check the pin============");
			ussdResponse = airtimeUserInputProcessor.checkpin(ussdSession, userInput, mobileNumber);
			return ussdResponse;
		}
		return ussdResponse;
	}
}