package com.itconsortiumgh.cal.service;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.itconsortiumgh.cal.model.BillPaymentRequest;
import com.itconsortiumgh.cal.model.BillResponseBody;
import com.itconsortiumgh.cal.model.CustomerDetails;
import com.itconsortiumgh.cal.model.ResponseCode;
import com.itconsortiumgh.cal.model.TransactionResponse;
import com.itconsortiumgh.cal.utils.JsonUtility;
import com.itconsortiumgh.cal.utils.properties.ApplicationProperties;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class BillPaymentFacade {
	@Autowired
	ApplicationProperties applicationProperties;
	public String customerNumberLookup(String userInput){
		String responseStr = "";
		log.info("Customer number before lookUp: {}",userInput);
		OkHttpClient client = new OkHttpClient();

		MediaType mediaType = MediaType.parse("application/json");
		RequestBody body = RequestBody.create(mediaType, "{\n\"dataSource\":\"Ghana_UAT\", \n"
				+ "\"customerNumber\":\""+userInput+"\",\n"
				+ "\"currencyCode\":\"GH\",\n"
				+ "\"businessUnit\":\"\",\n"
				+ "\"vendorCode\":\"MTN_Dstv\",\n"
				+ "\"language\": \"\",\n"
				+ "\"ipAddress\": \"\",\n"
				+ "\"interfaceType\":\"\"\n}");
		Request request = new Request.Builder()
		  //Jones endpoint
		  .url("http://172.18.12.55:8080/getCustomerDetailsByCustomerNumber")
		  //multichoice endpoint
//		  .url("http://68.169.63.40:8080/multichoice_ss/getCustomerDetailsByCustomerNumber")
		  .post(body)
		  .addHeader("accept", "application/json")
		  .addHeader("content-type", "application/json")
		  .addHeader("cache-control", "no-cache")
		  .build();

		try {
			Response response = client.newCall(request).execute();
			responseStr = response.body().string();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		BillResponseBody billResponseBody = JsonUtility.fromJson(responseStr, BillResponseBody.class);
		CustomerDetails customerDetails = billResponseBody.getCustomerDetails();
		String customerNumber = customerDetails.getNumber();
		return customerNumber;
		
	}
	
	
	public TransactionResponse dstvBillPayment(BillPaymentRequest billPaymentRequest){
		TransactionResponse transactionResponse=new TransactionResponse();
		String responseStr = "";
		OkHttpClient client = new OkHttpClient();

		MediaType mediaType = MediaType.parse("application/json");
//		log.info("invoice period{}",applicationProperties.getInvoicePeriod());
//		log.info("data source{}",applicationProperties.getDataSource());
		billPaymentRequest.setDataSource(applicationProperties.getDataSource());
		billPaymentRequest.setTransactionNumber(applicationProperties.getTransactionNumber());
		billPaymentRequest.setPaymentDescription(applicationProperties.getPaymentDescription());
		billPaymentRequest.setProductUserKey(applicationProperties.getProductUserKey());
		billPaymentRequest.setMethodOfPayment(applicationProperties.getMethodOfPayment());
		billPaymentRequest.setCurrency(applicationProperties.getCurrency());
		billPaymentRequest.setPaymentVendorCode(applicationProperties.getPaymentVendorCode());
		billPaymentRequest.setVendorCode(applicationProperties.getVendorCode());
		
		String message= "{\n\"dataSource\":\""+billPaymentRequest.getDataSource()+"\","
				+ " \n\"customerNumber\":\""+billPaymentRequest.getCustomerNumber()+"\",\n\"paymentVendorCode\":\""+billPaymentRequest.getPaymentVendorCode()+"\""
				+ ",\n\"transactionNumber\":\""+billPaymentRequest.getTransactionNumber()+"\",\n\"paymentDescription\":\""+billPaymentRequest.getPaymentDescription()+"\","
				+ "\n\"\":\"\",\n\"methodOfPayment\":\""+billPaymentRequest.getMethodOfPayment()+"\",\n\"amount\":\"1\","
				+ "\n\"invoicePeriod\":\""+billPaymentRequest.getInvoicePeriod()+"\",\n\"currency\":\""+billPaymentRequest.getCurrency()+"\",\n\"businessUnit\":\"\","
				+ "\n\"vendorCode\":\""+billPaymentRequest.getVendorCode()+"\",\n\"language\":\"\",\n\"ipAddress\": \"\"\n\n}";

		
		log.info("message{}",message);
		RequestBody body = RequestBody.create(mediaType, message);

		Request request = new Request.Builder()
		  //Jones endpoint
		  .url("http://172.18.12.55:8080/submitPayment")
		  .post(body)
		  .addHeader("accept", "application/json")
		  .addHeader("content-type", "application/json")
		  .addHeader("cache-control", "no-cache")
		  .build();

		try {
			Response response = client.newCall(request).execute();
			responseStr = response.body().string();
			log.info("responseString {}",responseStr);
			transactionResponse.setResponseCode(ResponseCode.CODE_01_OK);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return transactionResponse;
	}
	

}
