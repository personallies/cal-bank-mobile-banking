package com.itconsortiumgh.cal.service;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.Map;

import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.itconsortiumgh.cal.model.ClientState;
import com.itconsortiumgh.cal.model.CustomerAccountsContainer;
import com.itconsortiumgh.cal.model.InvoiceContainer;
import com.itconsortiumgh.cal.model.MerchantsContainer;
import com.itconsortiumgh.cal.model.MessageType;
import com.itconsortiumgh.cal.model.TransactionType;
import com.itconsortiumgh.cal.model.UssdRequest;
import com.itconsortiumgh.cal.model.UssdResponse;
import com.itconsortiumgh.cal.model.UssdSession;
import com.itconsortiumgh.cal.redis.queue.BillPaymentPublisher;
import com.itconsortiumgh.cal.repository.redis.RedisUssdSessionRepository;
import com.itconsortiumgh.cal.utils.JsonUtility;
import com.itconsortiumgh.cal.utils.properties.ApplicationProperties;
import com.itconsortiumgh.cal.utils.properties.MenuProperties;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class BillPaymentUserInputProcessor {
	@Autowired
	RedisUssdSessionRepository redisUssdSessionRepository;
	@Autowired
	MenuProperties menuProperties;
	@Autowired
	CustomerServices customerServices;
	@Autowired
	ApplicationProperties applicationProperties;
	@Autowired
	BillPaymentFacade billPaymentFacade;
	@Autowired
	BillPaymentPublisher billPaymentPublisher;

	public UssdResponse askUserToChooseAccount(UssdSession ussdSession, UssdRequest ussdRequest, String mobileNumber) {
		UssdResponse ussdResponse = new UssdResponse();

		// Customer Account Mapping
		CustomerAccountsContainer customerAccountsContainer = customerServices.retrieveCustomerAccounts(mobileNumber);
		Map<String, String> accountsMap = customerAccountsContainer.getAccountsMap();
		ussdSession.setTransactionType(TransactionType.BILL_PAYMENT);

		// Bill Payments Merchants Mapping
		MerchantsContainer merchantsContainer = applicationProperties.retrieveBillPaymentMerchants();
		String chooseMerchantMenu = menuProperties.getChooseMerchant() + "\n" + ""
				+ merchantsContainer.getMerchantsMenu();

		if (accountsMap.size() == 1) {
			ussdResponse.setType(MessageType.RESPONSE);
			ussdResponse.setMessage(chooseMerchantMenu);// Displaying Merchants
			ussdResponse.setClientState(ClientState.CHOOSE_MERCHANT);

			ussdSession.setClientState(ClientState.CHOOSE_MERCHANT);
			ussdSession.setDebitAccountLabel(accountsMap.get(1));// saving
																	// customers
																	// single
																	// account
																	// as debit
																	// account
																	// label
			redisUssdSessionRepository.put(ussdSession);
			return ussdResponse;
		} else if (accountsMap.size() > 1) {
			String selectAccountMenu = menuProperties.getSelectAccount() + "\n"
					+ customerAccountsContainer.getAccountsMenu();

			ussdResponse.setType(MessageType.RESPONSE);
			ussdResponse.setMessage(selectAccountMenu);// Displaying account
														// labels
			ussdResponse.setClientState(ClientState.CHOOSE_ACCOUNT);

			ussdSession.setClientState(ClientState.CHOOSE_ACCOUNT);
			ussdSession.setAccountLabelMapJson(JsonUtility.mapToJson(accountsMap));
			redisUssdSessionRepository.put(ussdSession);
			return ussdResponse;
		} else {
			ussdResponse.setType(MessageType.RELEASE);
			ussdResponse.setMessage(menuProperties.getNoAccount());
			return ussdResponse;
		}

	}

	public UssdResponse askUserToChooseMerchant(UssdSession ussdSession, String userInput, String mobileNumber) {
		UssdResponse ussdResponse = new UssdResponse();

		// Customer Accounts Mapping
		CustomerAccountsContainer customerAccountsContainer = customerServices.retrieveCustomerAccounts(mobileNumber);
		Map<String, String> accountsMap = customerAccountsContainer.getAccountsMap();
		String accountLabel = accountsMap.get(userInput);

		// Bill Payments Merchants Mapping
		MerchantsContainer merchantsContainer = applicationProperties.retrieveBillPaymentMerchants();
		String chooseMerchantMenu = menuProperties.getChooseMerchant() + "\n" + ""
				+ merchantsContainer.getMerchantsMenu();

		ussdResponse.setType(MessageType.RESPONSE);
		ussdResponse.setMessage(chooseMerchantMenu);
		ussdResponse.setClientState(ClientState.CHOOSE_MERCHANT);

		ussdSession.setClientState(ClientState.CHOOSE_MERCHANT);
		ussdSession.setDebitAccountLabel(accountLabel);// Saving account label
		redisUssdSessionRepository.put(ussdSession);
		return ussdResponse;
	}

	public UssdResponse askUserToEnterCustomerNumber(UssdSession ussdSession, String userInput) {
		UssdResponse ussdResponse = new UssdResponse();

		// Bill Payments Merchants Mapping
		MerchantsContainer merchantsContainer = applicationProperties.retrieveBillPaymentMerchants();
		Map<Integer, String> merchantMap = merchantsContainer.getMerchantsMap();
		String merchants = merchantMap.get(Integer.parseInt(userInput));

		ussdResponse.setType(MessageType.RESPONSE);
		ussdResponse.setMessage(menuProperties.getEnterCustomer());
		ussdResponse.setClientState(ClientState.ENTER_CUSTOMER);

		ussdSession.setClientState(ClientState.ENTER_CUSTOMER);
		ussdSession.setMerchantAccountLabel(merchants);
		redisUssdSessionRepository.put(ussdSession);
		return ussdResponse;
	}

	public UssdResponse displayCustomerNumber(UssdSession ussdSession, String userInput) {
		UssdResponse ussdResponse = new UssdResponse();
		String customerNumber = billPaymentFacade.customerNumberLookup(userInput);
		String dispCustomerNumber = MessageFormat.format(menuProperties.getDisplayCustomer(), customerNumber);

		ussdResponse.setType(MessageType.RESPONSE);
		ussdResponse.setMessage(dispCustomerNumber);
		ussdResponse.setClientState(ClientState.DISPLAY_CUSTOMER);

		ussdSession.setClientState(ClientState.DISPLAY_CUSTOMER);
		ussdSession.setCustomerNumber(customerNumber);
		redisUssdSessionRepository.put(ussdSession);
		return ussdResponse;

	}

	public UssdResponse askUserToEnterAmount(UssdSession ussdSession) {
		UssdResponse ussdResponse = new UssdResponse();
		InvoiceContainer invoiceContainer = applicationProperties.retrieveInvoicePeriod();
		String invoiceMenu = menuProperties.getChooseInvoicePeriod() + "\n" + "" + invoiceContainer.getInvoiceMenu();

		switch (ussdSession.getMerchantAccountLabel()) {
		case "DSTV":
			ussdResponse.setMessage(invoiceMenu);
			ussdResponse.setType(MessageType.RESPONSE);
			ussdResponse.setClientState(ClientState.CHOOSE_INVOICE_PERIOD);

			ussdSession.setClientState(ClientState.CHOOSE_INVOICE_PERIOD);
			redisUssdSessionRepository.put(ussdSession);
			return ussdResponse;

		case "ECG":
			ussdResponse.setType(MessageType.RESPONSE);
			ussdResponse.setMessage(menuProperties.getEnterAmount());
			ussdResponse.setClientState(ClientState.ENTER_AMOUNT);

			ussdSession.setClientState(ClientState.ENTER_AMOUNT);
			redisUssdSessionRepository.put(ussdSession);
			return ussdResponse;

		}
		redisUssdSessionRepository.put(ussdSession);
		return ussdResponse;
	}

	public UssdResponse askUserForInvoicePeriod(UssdSession ussdSession, String userInput) {
		UssdResponse ussdResponse = new UssdResponse();
		InvoiceContainer invoiceContainer = applicationProperties.retrieveInvoicePeriod();
		Map<Integer, String> invoiceMap = invoiceContainer.getInvoiceMap();
		String invoicePeriod = invoiceMap.get(Integer.parseInt(userInput));
		log.info("invoiceMap{}:", invoiceMap);

		ussdResponse.setType(MessageType.RESPONSE);
		ussdResponse.setMessage(menuProperties.getEnterAmount());
		ussdResponse.setClientState(ClientState.ENTER_AMOUNT);

		ussdSession.setClientState(ClientState.ENTER_AMOUNT);
		ussdSession.setInvoicePeriod(invoicePeriod);
		redisUssdSessionRepository.put(ussdSession);
		return ussdResponse;
	}

	public UssdResponse displayConfirmMessage(UssdSession ussdSession, String userInput, String mobileNumber) {
		UssdResponse ussdResponse = new UssdResponse();
		userInput = userInput.replaceFirst("^0\\.", ".");
		if (NumberUtils.isNumber(userInput)) {
			BigDecimal amount = new BigDecimal(userInput);
			ussdSession.setAmount(amount);
		} else {
			ussdResponse.setMessage(menuProperties.getEnterValidAmount());
			ussdResponse.setClientState(ClientState.ENTER_AMOUNT);
		}
		String confirmMessage = MessageFormat.format(menuProperties.getConfirmBillPayment(),
				ussdSession.getMerchantAccountLabel(),
				applicationProperties.getCurrency() + " " + ussdSession.getAmount(),
				ussdSession.getDebitAccountLabel());
		ussdResponse.setType(MessageType.RESPONSE);
		ussdResponse.setMessage(confirmMessage);
		ussdResponse.setClientState(ClientState.CONFIRM_MESSAGE);

		ussdSession.setClientState(ClientState.CONFIRM_MESSAGE);
		redisUssdSessionRepository.put(ussdSession);
		return ussdResponse;
	}

	public UssdResponse askForPin(UssdSession ussdSession, String userInput) {
		UssdResponse ussdResponse = new UssdResponse();
		log.info("The user entered :{}", userInput);
		
		switch (userInput) {
		case "1": {
			ussdResponse.setType(MessageType.RESPONSE);
			ussdResponse.setMessage(menuProperties.getEnterPin());
			ussdResponse.setClientState(ClientState.ENTER_PIN);

			ussdSession.setClientState(ClientState.ENTER_PIN);
			redisUssdSessionRepository.put(ussdSession);
			return ussdResponse;
		}

		case "2": {
			ussdResponse.setType(MessageType.RELEASE);
			ussdResponse.setMessage(menuProperties.getCancelMessage());
			ussdResponse.setClientState(ClientState.FINAL);

			ussdSession.setClientState(ClientState.FINAL);
			return ussdResponse;
		}
		}
		return ussdResponse;
	}

	public UssdResponse checkPin(UssdSession ussdSession, String userInput) {
		UssdResponse ussdResponse = new UssdResponse();
		ussdResponse.setType(MessageType.RELEASE);
		ussdResponse.setClientState(ClientState.FINAL);
		ussdSession.setTransactionType(TransactionType.BILL_PAYMENT);
		Boolean isPinValid = true;

		if (isPinValid) {
			ussdSession.setPin(userInput);
			String message = JsonUtility.toJson(ussdSession);
			log.info("about to publish balance request {}", message);
			billPaymentPublisher.publish(message);
			log.info("balance request published");
			ussdResponse.setMessage(menuProperties.getProcessingRequest());
			return ussdResponse;

		} else if (!isPinValid) {
			ussdResponse.setMessage(menuProperties.getIncorrectPin());
			return ussdResponse;
		} else {
			ussdResponse.setMessage(menuProperties.getFailureMessage());
			return ussdResponse;
		}
	}

}
