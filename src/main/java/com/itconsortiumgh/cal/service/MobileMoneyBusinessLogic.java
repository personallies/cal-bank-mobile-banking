package com.itconsortiumgh.cal.service;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.itconsortiumgh.cal.model.ClientState;
import com.itconsortiumgh.cal.model.MessageType;
import com.itconsortiumgh.cal.model.MobileMoneyOperation;
import com.itconsortiumgh.cal.model.RequestPaymentRequestSdp;
import com.itconsortiumgh.cal.model.UssdRequest;
import com.itconsortiumgh.cal.model.UssdResponse;
import com.itconsortiumgh.cal.model.UssdSession;
import com.itconsortiumgh.cal.model.WalletToOtherAccountRequest;
import com.itconsortiumgh.cal.model.WalletToOwnAccountRequest;
import com.itconsortiumgh.cal.redis.queue.RequestPaymentPublisher;
import com.itconsortiumgh.cal.redis.queue.WalletToBankPublisher;
import com.itconsortiumgh.cal.utils.JsonUtility;
import com.itconsortiumgh.cal.utils.properties.MenuProperties;

@Service
public class MobileMoneyBusinessLogic {

	@Autowired
	MenuProperties menuProperties;
	@Autowired
	MobileMoneyUserInputProcessor mobileMoneyUserInputProcessor;
	@Autowired
	RequestPaymentPublisher requestPaymentPublisher;
	@Autowired
	RequestPaymentPublisher walletToOtherAccountPublisher;
	public UssdResponse processRequest(UssdSession ussdSession, UssdRequest ussdRequest) {
		UssdResponse ussdResponse = new UssdResponse();
		String sessionId = ussdRequest.getSessionId();
		
		String clientState = ussdRequest.getClientState();
		String userInput = ussdRequest.getMessage();
		String mobileNumber = ussdRequest.getMobile();
		
		switch (clientState) {
		case ClientState.ROOT: 
			ussdResponse = mobileMoneyUserInputProcessor.askUserToChooseOperation(ussdSession, ussdRequest, userInput);
			break;
		case ClientState.CHOOSE_OPERATION: 
			mobileMoneyUserInputProcessor.saveOperationChosen(ussdSession, ussdRequest);
			ussdResponse = mobileMoneyUserInputProcessor.askUserToChooseAccount(ussdSession, ussdRequest, mobileNumber, userInput);
			break;
		case ClientState.CHOOSE_ACCOUNT:
			ussdResponse = mobileMoneyUserInputProcessor.askUserToEnterAmount(ussdSession, mobileNumber, userInput);
			break;
		case ClientState.ENTER_DESTINATION_ACCOUNT:
			ussdResponse = mobileMoneyUserInputProcessor.askUserToEnterAmount(ussdSession, userInput);
			break;
		case ClientState.ENTER_AMOUNT:
			ussdSession = mobileMoneyUserInputProcessor.saveAmount(ussdSession, ussdRequest);
			if(ussdSession.getMobileMoneyOperation().equals(MobileMoneyOperation.BANK_TO_WALLET)){
				ussdResponse = mobileMoneyUserInputProcessor.askUserToEnterPin(ussdSession, ussdRequest, userInput);
			}else {
				ussdResponse.setMessage(menuProperties.getProcessingRequest());
				ussdResponse.setType(MessageType.RELEASE);
				ussdResponse.setClientState(ClientState.FINAL);
				RequestPaymentRequestSdp requestPaymentRequestSdp = mobileMoneyUserInputProcessor.prepareRequestPayment(ussdSession, ussdRequest);
				String requestMessage = JsonUtility.toJson(requestPaymentRequestSdp);
				requestPaymentPublisher.publish(requestMessage);
			}
			break;

		case ClientState.ENTER_PIN:
			ussdResponse = mobileMoneyUserInputProcessor.checkPin(ussdSession, ussdRequest);
			break;
		default:
			ussdResponse.setMessage(menuProperties.getDefaultMessage());
		}
		return ussdResponse;
	}

}
