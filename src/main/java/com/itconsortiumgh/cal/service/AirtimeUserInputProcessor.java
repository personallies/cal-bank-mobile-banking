package com.itconsortiumgh.cal.service;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.Map;

import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.itconsortiumgh.cal.model.AirtimeNetwork;
import com.itconsortiumgh.cal.model.ClientState;
import com.itconsortiumgh.cal.model.CustomerAccountsContainer;
import com.itconsortiumgh.cal.model.MessageType;
import com.itconsortiumgh.cal.model.NumberPortabilityContainer;
import com.itconsortiumgh.cal.model.TransactionType;
import com.itconsortiumgh.cal.model.UssdRequest;
import com.itconsortiumgh.cal.model.UssdResponse;
import com.itconsortiumgh.cal.model.UssdSession;
import com.itconsortiumgh.cal.model.logs.TransactionLogs;
import com.itconsortiumgh.cal.redis.queue.AirtimePublisher;
import com.itconsortiumgh.cal.repository.CustomerRepository;
import com.itconsortiumgh.cal.repository.redis.RedisUssdSessionRepository;
import com.itconsortiumgh.cal.utils.GeneralUtils;
import com.itconsortiumgh.cal.utils.JsonUtility;
import com.itconsortiumgh.cal.utils.properties.ApplicationProperties;
import com.itconsortiumgh.cal.utils.properties.MenuProperties;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class AirtimeUserInputProcessor {
	// public static void main (String args[]){
	// String str = ".5".trim();
	//
	// System.out.println("str = "+ str + " " +NumberUtils.isNumber(str));
	// System.out.println("str = "+ str + " " +StringUtils.isNumeric(str));
	// }
	@Autowired
	NumberPortabilityFacade numberPortabilityFacade;
	@Autowired
	BrokerApiFacade brokerApiFacade;
	@Autowired
	MenuProperties menuProperties;
	// @Autowired
	// CustomerServices subscriberServices;
	@Autowired
	CustomerRepository subscriberRepository;
	@Autowired
	RedisUssdSessionRepository redisUssdSessionRepository;
	@Autowired
	CoreBankingFacade coreBankingFacade;
	@Autowired
	CustomerServices customerServices;
	@Autowired
	GeneralUtils generalUtils;
	@Autowired
	CustomerRepository customerRepository;
	@Autowired
	ApplicationProperties applicationProperties;
	@Autowired
	AirtimePublisher airtimePublisher;

	public UssdResponse askUserToChooseBuyFor(UssdSession ussdSession, String mobileNumber) {
		UssdResponse ussdResponse = new UssdResponse();

		ussdResponse.setType(MessageType.RESPONSE);
		ussdResponse.setMessage(menuProperties.getChooseBuyFor());
		ussdResponse.setClientState(ClientState.BUY_FOR);
		ussdSession.setClientState(ClientState.BUY_FOR);
		ussdSession.setMsisdn(mobileNumber);
		redisUssdSessionRepository.put(ussdSession);
		return ussdResponse;

	}

	public UssdResponse askUserForNetwork(UssdSession ussdSession, UssdRequest ussdRequest) {
		UssdResponse ussdResponse = new UssdResponse();
		String userInput = ussdRequest.getMessage();
		String mobileNumber = ussdRequest.getMobile();

		switch (userInput) {
		case "1":// buy for self
			ussdResponse = generalUtils.selectAcountLabel(mobileNumber, ussdSession);
			ussdSession.setAirtimeNetwork(Enum.valueOf(AirtimeNetwork.class, ussdRequest.getOperator()));
			redisUssdSessionRepository.put(ussdSession);
			return ussdResponse;

		case "2":// buy for another
			ussdResponse.setType(MessageType.RESPONSE);
			ussdResponse.setMessage(menuProperties.getEnterPhoneNumber());
			ussdResponse.setClientState(ClientState.ENTER_PHONE_NUMBER);
			ussdSession.setClientState(ClientState.ENTER_PHONE_NUMBER);
			redisUssdSessionRepository.put(ussdSession);
			return ussdResponse;
		}

		return ussdResponse;

	}

	public UssdResponse askForChooseAccount(UssdSession ussdSession, String userInput, String mobileNumber) {
		UssdResponse ussdResponse = new UssdResponse();
		NumberPortabilityContainer numberPortabilityContainer = numberPortabilityFacade
				.checkNetwork(userInput.replaceFirst("0", "+233"));
		if (numberPortabilityContainer.getNetwork().equalsIgnoreCase("")) {
			ussdResponse.setMessage(menuProperties.getUnknownNumber());
			ussdResponse.setType(MessageType.RELEASE);
			ussdResponse.setClientState(ClientState.FINAL);
			ussdSession.setClientState(ClientState.FINAL);
		} else {
			ussdSession.setAirtimeNetwork(Enum.valueOf(AirtimeNetwork.class,
					numberPortabilityContainer.getNetwork().replace("Ghana", "").trim()));
			ussdSession.setMsisdn(userInput);
			redisUssdSessionRepository.put(ussdSession);
			ussdResponse = generalUtils.selectAcountLabel(mobileNumber, ussdSession);
		}

		return ussdResponse;

	}

	public UssdResponse askForAmount(UssdSession ussdSession, String userInput, String mobileNumber) {
		UssdResponse ussdResponse = new UssdResponse();
		CustomerAccountsContainer customerAccountsContainer = customerServices.retrieveCustomerAccounts(mobileNumber);
		Map<String, String> accountsMap = customerAccountsContainer.getAccountsMap();
		String accountLabel = accountsMap.get(userInput);
		ussdResponse.setType(MessageType.RESPONSE);
		ussdSession.setClientState(ClientState.ENTER_AMOUNT);
		ussdResponse.setClientState(ClientState.ENTER_AMOUNT);
		ussdResponse.setMessage(menuProperties.getEnterAmount());
		ussdSession.setDebitAccountLabel(accountLabel);
		redisUssdSessionRepository.put(ussdSession);
		return ussdResponse;
	}

	public UssdResponse displayConfirmMessage(UssdSession ussdSession, String userInput, String mobileNumber) {
		UssdResponse ussdResponse = new UssdResponse();
		userInput = userInput.replaceFirst("^0\\.", ".");
		if (NumberUtils.isNumber(userInput)) {
			BigDecimal amount = new BigDecimal(userInput);
			ussdSession.setAmount(amount);
		} else {
			ussdResponse.setMessage(menuProperties.getEnterValidAmount());
			ussdResponse.setClientState(ClientState.ENTER_AMOUNT);
		}
		String confirmMessage = MessageFormat.format(menuProperties.getConfirmAirtimePurchase(), ussdSession.getAirtimeNetwork(),applicationProperties.getCurrency()+" "+ussdSession.getAmount(),ussdSession.getDebitAccountLabel(),ussdSession.getMsisdn());
		
		ussdResponse.setType(MessageType.RESPONSE);
		ussdResponse.setMessage(confirmMessage);
		ussdResponse.setClientState(ClientState.CONFIRM_MESSAGE);
		
		ussdSession.setClientState(ClientState.CONFIRM_MESSAGE);
		 
		return ussdResponse;
	}

	public UssdResponse askForPin(UssdSession ussdSession, String userInput) {
		UssdResponse ussdResponse = new UssdResponse();
		log.info("The user entered :{}",userInput);
		switch(userInput){
		case "1":{
			ussdResponse.setType(MessageType.RESPONSE);
			ussdResponse.setMessage(menuProperties.getEnterPin());
			ussdResponse.setClientState(ClientState.ENTER_PIN);
			
			ussdSession.setClientState(ClientState.ENTER_PIN);
			redisUssdSessionRepository.put(ussdSession);
			return ussdResponse;
		}
		
		case "2":{
			ussdResponse.setType(MessageType.RELEASE);
			ussdResponse.setMessage(menuProperties.getCancelMessage());
			ussdResponse.setClientState(ClientState.FINAL);

			ussdSession.setClientState(ClientState.FINAL);
			return ussdResponse;
		}
		}
		return ussdResponse;
	}

	public UssdResponse checkpin(UssdSession ussdSession, String userInput, String mobileNumber) {
		UssdResponse ussdResponse = new UssdResponse();
		ussdResponse.setClientState(ClientState.FINAL);
		ussdResponse.setType(MessageType.RELEASE);
		ussdSession.setTransactionType(TransactionType.AIRTIME);
		Boolean isPinValid = true;
		if (isPinValid) // customerServices.isPinValid(userInput,
						// ussdSession.getMsisdn()){
		{
			log.info("ussdSession{}", ussdSession);
			ussdSession.setPin(userInput);
			String message = JsonUtility.toJson(ussdSession);
			airtimePublisher.publish(message);
			ussdResponse.setMessage(menuProperties.getProcessingRequest());
			return ussdResponse;
		} else {
			ussdResponse.setMessage(menuProperties.getIncorrectPin());
			return ussdResponse;
		}
	}
}
