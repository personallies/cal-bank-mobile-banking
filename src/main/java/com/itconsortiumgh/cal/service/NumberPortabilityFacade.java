package com.itconsortiumgh.cal.service;
import java.io.IOException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.itconsortiumgh.cal.model.Currentcarrier;
import com.itconsortiumgh.cal.model.NumberPortabilityContainer;
import com.itconsortiumgh.cal.model.ResponseBody;
import com.itconsortiumgh.cal.utils.JsonUtility;
import com.itconsortiumgh.cal.utils.properties.ApplicationProperties;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class NumberPortabilityFacade {
//	public static void main(String []args){
//		NumberPortabilityFacade numberPortabilityFacade = new NumberPortabilityFacade();
//		System.out.println(numberPortabilityFacade.checkNetwork("+233243576745"));
//	}
	@Autowired
	ApplicationProperties applicationProperties;
	public NumberPortabilityContainer checkNetwork(String mobileNumber){
		NumberPortabilityContainer numberPortabilityContainer=new NumberPortabilityContainer();
		String apiResponse="";
//		String url=applicationProperties.getNumberPortabilityEndpoint();
		String url="https://api.smsgh.com/portability/mnplookup/";
		OkHttpClient client = new OkHttpClient();
		Request request = new Request.Builder()
				.url(url+mobileNumber)
				.get()
				.addHeader("authorization", "Basic bGlzZGt6ZXA6Z2FteG1wY20=")
				.addHeader("cache-control", "no-cache")
				.addHeader("postman-token", "f73a1bdd-f662-bcca-332c-eb5254f98f25")
				.build();

		Response response = null;
		try {
			response = client.newCall(request).execute();
			apiResponse=response.body().string();
			System.out.println(apiResponse);
		} catch (IOException e) {
			e.printStackTrace();
		}
		Gson gson = new Gson();
		ResponseBody responseBody = gson.fromJson(apiResponse, ResponseBody.class);
//		ResponseBody responseBody = JsonUtility.fromJson(apiResponse, ResponseBody.class);
		Currentcarrier currentcarrier = responseBody.getCurrent_carrier();
		numberPortabilityContainer.setNetwork(currentcarrier.getName());
		return numberPortabilityContainer;


	}

}


