package com.itconsortiumgh.cal.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.itconsortiumgh.cal.model.Customer;
import com.itconsortiumgh.cal.model.CustomerAccount;
import com.itconsortiumgh.cal.model.CustomerStatus;
import com.itconsortiumgh.cal.model.Merchant;
import com.itconsortiumgh.cal.model.MerchantAccount;
import com.itconsortiumgh.cal.model.UssdRequest;
import com.itconsortiumgh.cal.model.UssdResponse;
import com.itconsortiumgh.cal.repository.CustomerRepository;
import com.itconsortiumgh.cal.repository.MerchantRepository;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class SampleBusinessLogic {
	
	@Autowired
	CustomerRepository customerRepository;
	@Autowired
	MerchantRepository merchantRepository;
	
	public UssdResponse tryWork(UssdRequest ussdRequest){
		//testing Customer and Customer Account Relationship

		Customer customer =new Customer();
		
		customer.setMsisdn("0057453");
        customer.setFirstName("eyram");
        customer.setLastName("atakli");
        customer.setOtherNames("ama");
        customer.setCustomerStatus(CustomerStatus.ACTIVE);
   
		CustomerAccount customerAccount = new CustomerAccount();
		
		customerAccount.setCustomer(customer);
		customerAccount.setAccountLabel("Investment");
	
	
		CustomerAccount customerAccount2 = new CustomerAccount();
		
		customerAccount2.setCustomer(customer);
		customerAccount2.setAccountLabel("Trust");
	
		
     	customer.getCustomerAccounts().add(customerAccount);
	    customer.getCustomerAccounts().add(customerAccount2);
		
		customerRepository.save(customer);
		return null;
	
	}
	
	public UssdResponse trySecWork(UssdRequest ussdRequest ){
		//testing Customer and Customer Account Relationship
		String mobileNum = ussdRequest.getMobile();
				 
		Customer customer = customerRepository.findByMsisdn(mobileNum);
		log.info("Mobile Number {}",mobileNum);

//		List<CustomerAccount> customerList = customer.getCustomerAccounts();	
		log.info(" {}", customer);
		return null;
	}

	public UssdResponse tryThirdWork(UssdRequest ussdRequest){
		
		Merchant merchant = new Merchant();
		merchant.setProductName("Airtime Purchase");
		
		MerchantAccount merchantAccount = new MerchantAccount();
		merchantAccount.setDescription("Airtime Purchase for MTN");
		merchantAccount.setMerchantAccountLabel("MTN");
		merchantAccount.setMerchant(merchant);
		
		MerchantAccount merchantAccount2 = new MerchantAccount();
		merchantAccount.setDescription("Airtime Purchase for TIGO");
		merchantAccount.setMerchantAccountLabel("TIGO");
		merchantAccount.setMerchant(merchant);
		
		merchant.getMerchantAccounts().add(merchantAccount);
		merchant.getMerchantAccounts().add(merchantAccount2);
		
		merchantRepository.save(merchant);
		return null;
	}
	
	

}
