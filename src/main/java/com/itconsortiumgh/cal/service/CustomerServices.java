package com.itconsortiumgh.cal.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.itconsortiumgh.cal.model.Customer;
import com.itconsortiumgh.cal.model.CustomerAccount;
import com.itconsortiumgh.cal.model.CustomerAccountsContainer;
import com.itconsortiumgh.cal.model.PinValidationRequest;
import com.itconsortiumgh.cal.repository.CustomerRepository;
import com.itconsortiumgh.cal.repository.redis.RedisUssdSessionRepository;
import com.itconsortiumgh.cal.utils.properties.ApplicationProperties;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class CustomerServices {


	@Autowired
	CustomerRepository customerRepository;
	
	@Autowired
	RedisUssdSessionRepository redisUssdSessionRepository;

	@Autowired
	RestTemplate restTemplate;
	@Autowired
	ApplicationProperties applicationProperties;

	public boolean isRegistered(String mobileNumber) {
		Customer customer = customerRepository.findByMsisdn(mobileNumber);
//		if (customer != null) {
//			if (customer.getCustomerStatus().equals(CustomerStatus.ACTIVE)) {
//				return true;
//			}
//		} else {
//			return false;
//		}
		return true;
	}

	public boolean isPinValid(String enteredPin, String mobileNumber) {
		log.info("=================we are going to check the pin============");
		//TO DO: ENCRYPTED PIN VALIDATION SERVICE
		String url = applicationProperties.getPinValidationEndpoint();
		PinValidationRequest request = new PinValidationRequest(enteredPin, mobileNumber);
		Boolean result = restTemplate.postForObject(url, request, Boolean.class);
		log.info("=================pin check result {}============", result);
		return result;
	}


	public CustomerAccountsContainer retrieveCustomerAccounts(String mobilenumber)
	{
		
		Customer customer=customerRepository.findByMsisdn(mobilenumber);
		List<CustomerAccount> customerAccounts = customer.getCustomerAccounts();
		String menuString = "";
		Map<String, String> accountsMap = new HashMap<>();
		Integer i=1;
		for(CustomerAccount customerAccount: customerAccounts){
			menuString=menuString+i+". "+customerAccount.getAccountLabel()+"\n";
			accountsMap.put(i.toString(), customerAccount.getAccountLabel());
			i++;
		}
		CustomerAccountsContainer customerAccountsContainer = new CustomerAccountsContainer();
		customerAccountsContainer.setAccountsMap(accountsMap);
		customerAccountsContainer.setAccountsMenu(menuString);
		return customerAccountsContainer;
	}

}
