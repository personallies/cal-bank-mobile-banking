package com.itconsortiumgh.cal.service;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.itconsortiumgh.cal.model.ClientState;
import com.itconsortiumgh.cal.model.CustomerAccountsContainer;
import com.itconsortiumgh.cal.model.MessageType;
import com.itconsortiumgh.cal.model.MobileMoneyOperation;
import com.itconsortiumgh.cal.model.RequestPaymentRequestSdp;
import com.itconsortiumgh.cal.model.UssdRequest;
import com.itconsortiumgh.cal.model.UssdResponse;
import com.itconsortiumgh.cal.model.UssdSession;
import com.itconsortiumgh.cal.model.WalletToOtherAccountRequest;
import com.itconsortiumgh.cal.model.WalletToOwnAccountRequest;
import com.itconsortiumgh.cal.redis.queue.BankToWalletPublisher;
import com.itconsortiumgh.cal.redis.queue.WalletToBankPublisher;
import com.itconsortiumgh.cal.repository.MobileMoneyLogRepository;
import com.itconsortiumgh.cal.repository.redis.RedisUssdSessionRepository;
import com.itconsortiumgh.cal.utils.GeneralUtils;
import com.itconsortiumgh.cal.utils.JsonUtility;
import com.itconsortiumgh.cal.utils.TransactionIdGenerator;
import com.itconsortiumgh.cal.utils.properties.ApplicationProperties;
import com.itconsortiumgh.cal.utils.properties.MenuProperties;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class MobileMoneyUserInputProcessor {

	@Autowired
	MenuProperties menuProperties;
	@Autowired
	RedisUssdSessionRepository redisUssdSessionRepository;
	@Autowired
	CustomerServices customerServices;
	@Autowired
	GeneralUtils generalUtils;
	@Autowired
	BankToWalletPublisher bankToWalletPublisher;
	@Autowired
	WalletToBankPublisher walletToBankPublisher;

	@Autowired
	MobileMoneyLogRepository mobileMoneyLogRepository;
	@Autowired
	ApplicationProperties applicationProperties;

	public UssdResponse askUserToChooseOperation(UssdSession ussdSession, UssdRequest ussdRequest, String userInput) {
		UssdResponse ussdResponse = new UssdResponse();

		ussdResponse.setType(MessageType.RESPONSE);
		ussdResponse.setMessage(menuProperties.getMobileBankingOperation());
		ussdResponse.setClientState(ClientState.CHOOSE_OPERATION);

		ussdSession.setWalletProvider(ussdRequest.getOperator());
		ussdSession.setClientState(ClientState.CHOOSE_OPERATION);
		redisUssdSessionRepository.put(ussdSession);

		return ussdResponse;
	}

	public UssdResponse askUserToChooseAccount(UssdSession ussdSession, UssdRequest ussdRequest, String mobileNumber,
			String userInput) {
		UssdResponse ussdResponse = new UssdResponse();
		if (generalUtils.checkIfNetworkExists(ussdRequest) == true) {
			switch (userInput) {
			case "1":// bank to wallet
				ussdResponse = generalUtils.selectAcountLabel(mobileNumber, ussdSession);
				break;
			case "2":// wallet to own account
				ussdResponse = generalUtils.selectAcountLabel(mobileNumber, ussdSession);
				break;
			case "3":// wallet to other account
				ussdResponse.setMessage(menuProperties.getEnterDestinationAccount());
				ussdResponse.setType(MessageType.RESPONSE);
				ussdResponse.setClientState(ClientState.ENTER_DESTINATION_ACCOUNT);
				break;
			default:
				ussdResponse.setMessage(menuProperties.getDefaultMessage());
			}
		} else {
			ussdResponse.setMessage(ussdRequest.getOperator() + " " + menuProperties.getNetworkDoesNotExist());
		}
		return ussdResponse;
	}

	public UssdSession saveAmount(UssdSession ussdSession, UssdRequest ussdRequest) {
		String amount = ussdRequest.getMessage();
		ussdSession.setAmount(new BigDecimal(amount));
		redisUssdSessionRepository.put(ussdSession);
		return redisUssdSessionRepository.get(ussdSession);
	}

	public UssdSession saveOperationChosen(UssdSession ussdSession, UssdRequest ussdRequest) {
		String userInput = ussdRequest.getMessage();
		switch (userInput) {
		case "1":// bank to wallet
			ussdSession.setMobileMoneyOperation(MobileMoneyOperation.BANK_TO_WALLET);
			redisUssdSessionRepository.put(ussdSession);
			break;

		case "2":// wallet to own account
			ussdSession.setMobileMoneyOperation(MobileMoneyOperation.WALLET_TO_OWN_ACCOUNT);
			redisUssdSessionRepository.put(ussdSession);
			break;
		case "3":// wallet to other account
			ussdSession.setMobileMoneyOperation(MobileMoneyOperation.WALLET_TO_OTHER_ACCOUNT);
			ussdSession.setClientState(ClientState.ENTER_DESTINATION_ACCOUNT);
			redisUssdSessionRepository.put(ussdSession);
			break;
		default:
		}
		return redisUssdSessionRepository.get(ussdSession);
	}

	/**
	 * For bank to wallet and wallet to own account operations, the user is
	 * required to choose an account label before proceeding. This method takes
	 * the account map and compares it with the user input as key and saves the
	 * value in the repository.
	 * 
	 * @param ussdSession
	 * @param mobileNumber
	 * @param userInput
	 * @return ussdResponse
	 */
	public UssdResponse askUserToEnterAmount(UssdSession ussdSession, String mobileNumber, String userInput) {
		UssdResponse ussdResponse = new UssdResponse();
		CustomerAccountsContainer customerAccountsContainer = customerServices.retrieveCustomerAccounts(mobileNumber);
		Map<String, String> accountsMap = customerAccountsContainer.getAccountsMap();

		String debitAccountLabel = accountsMap.get(userInput);
		ussdResponse.setType(MessageType.RESPONSE);
		ussdResponse.setMessage(menuProperties.getEnterAmount());
		ussdResponse.setClientState(ClientState.ENTER_AMOUNT);

		ussdSession.setClientState(ClientState.ENTER_AMOUNT);
		ussdSession.setDebitAccountLabel(debitAccountLabel);
		redisUssdSessionRepository.put(ussdSession);
		return ussdResponse;
	}

	/**
	 * For wallet to other account operations the user is required to enter the
	 * destination account of the transaction. This method saves the destination
	 * account number in the repository
	 * 
	 * @param ussdSession
	 * @param userInput
	 * @return ussdResponse
	 */
	public UssdResponse askUserToEnterAmount(UssdSession ussdSession, String userInput) {
		UssdResponse ussdResponse = new UssdResponse();
		ussdResponse.setType(MessageType.RESPONSE);
		ussdResponse.setMessage(menuProperties.getEnterAmount());
		ussdResponse.setClientState(ClientState.ENTER_AMOUNT);
		ussdSession.setClientState(ClientState.ENTER_AMOUNT);
		ussdSession.setCreditAccountNumber(userInput);
		redisUssdSessionRepository.put(ussdSession);
		return ussdResponse;
	}

	public RequestPaymentRequestSdp prepareRequestPayment(UssdSession ussdSession, UssdRequest ussdRequest) {
		RequestPaymentRequestSdp paymentRequest = new RequestPaymentRequestSdp();
		if ("MTN".equalsIgnoreCase(ussdRequest.getOperator())) {
			String acctRef = ussdSession.getMobileMoneyOperation() + "_" + ussdSession.getMsisdn() + "_"
					+ ussdSession.getSessionId();
			paymentRequest.setAcctRef(acctRef);
			String amount = ussdSession.getSubscriberInput();
			paymentRequest.setAmount(amount);
			paymentRequest.setProcessingNumber(TransactionIdGenerator.nextId().toString());
			paymentRequest.setMsisdn(ussdSession.getMsisdn());
			paymentRequest.setNarration("Wallet To Bank");
			if (applicationProperties.isServiceIdInUATMode()) {
				paymentRequest.setServiceId(applicationProperties.getRpServiceIdUAT());
				log.info("we are in UATMode {}", applicationProperties.getRpServiceIdUAT());
			} else {
				paymentRequest.setServiceId(applicationProperties.getRpServiceId());
				log.info("we are NOT in UATMode {}", applicationProperties.getRpServiceId());
			}
			paymentRequest.setSpId(applicationProperties.getRpSpId());
			paymentRequest.setSpPassword(applicationProperties.getRpSpPassword());
			paymentRequest.setCurrency(applicationProperties.getCurrency());

		} else if ("Tigo".equalsIgnoreCase(ussdRequest.getOperator())) {
			// generate the requestPayment
		} else if ("Vodafone".equalsIgnoreCase(ussdRequest.getOperator())) {
			// generate the requestPayment
		} else if ("Airtel".equalsIgnoreCase(ussdRequest.getOperator())) {
			// generate the requestPayment
		}
		return paymentRequest;
	}

	public WalletToOwnAccountRequest prepareWalletToOwnAccountRequest(UssdSession ussdSession) {
		WalletToOwnAccountRequest walletToOwnAccountRequest = new WalletToOwnAccountRequest();
		walletToOwnAccountRequest.setAccountLabel(ussdSession.getDebitAccountLabel());
		walletToOwnAccountRequest.setAmount(ussdSession.getAmount());
		walletToOwnAccountRequest.setMsisdn(ussdSession.getMsisdn());
		walletToOwnAccountRequest.setWalletProvider(ussdSession.getWalletProvider());
		walletToOwnAccountRequest.setTestFlag(applicationProperties.getTestFlag());
		walletToOwnAccountRequest.setTransactionId(TransactionIdGenerator.nextId().toString());
		return walletToOwnAccountRequest;
	}

	public WalletToOtherAccountRequest prepareWalletToOtherAccountRequest(UssdSession ussdSession,
			UssdRequest ussdRequest) {
		WalletToOtherAccountRequest walletToOtherAccountRequest = new WalletToOtherAccountRequest();
		walletToOtherAccountRequest.setAmount(ussdSession.getAmount());
		walletToOtherAccountRequest.setMsisdn(ussdSession.getMsisdn());
		walletToOtherAccountRequest.setWalletProvider(ussdRequest.getOperator());
		walletToOtherAccountRequest.setTestFlag(applicationProperties.getTestFlag());
		walletToOtherAccountRequest.setTransactionId(TransactionIdGenerator.nextId().toString());
		walletToOtherAccountRequest.setDestinationAccount(ussdSession.getCreditAccountNumber());
		return walletToOtherAccountRequest;
	}

	public UssdResponse askUserToEnterPin(UssdSession ussdSession, UssdRequest ussdRequest, String userInput) {
		UssdResponse ussdResponse = new UssdResponse();
		if (StringUtils.isNumeric(userInput)) {
			BigDecimal amount = new BigDecimal(userInput);
			ussdSession.setAmount(amount);
		} else {
			ussdResponse.setMessage(menuProperties.getEnterValidAmount());
			ussdResponse.setClientState(ClientState.ENTER_AMOUNT);
		}
		
		ussdResponse.setType(MessageType.RESPONSE);
		ussdResponse.setMessage(menuProperties.getEnterPin());
		ussdResponse.setClientState(ClientState.ENTER_PIN);

		ussdSession.setClientState(ClientState.ENTER_PIN);
		redisUssdSessionRepository.put(ussdSession);
		return ussdResponse;
	}

	public UssdResponse checkPin(UssdSession ussdSession, UssdRequest ussdRequest) {
		UssdResponse ussdResponse = new UssdResponse();
		String pin = ussdRequest.getMessage();
		// Boolean isPinValid = customerServices.isPinValid(pin,
		// ussdRequest.getMobile());
		Boolean isPinValid = true;

		ussdResponse.setType(MessageType.RELEASE);
		ussdResponse.setClientState(ClientState.FINAL);

		if (isPinValid) {
			MobileMoneyOperation mobileMoneyOperation = ussdSession.getMobileMoneyOperation();
			String message = JsonUtility.toJson(ussdSession);
			switch (mobileMoneyOperation) {
			case BANK_TO_WALLET: {
				// BankToWalletRequest bankToWalletRequest = new
				// BankToWalletRequest(transactionId, amount, accountLabel,
				// mobile, walletProvider, pin,
				// applicationProperties.getTestFlag());
				// String bankToWalletJson =
				// JsonUtility.toJson(bankToWalletRequest);
				bankToWalletPublisher.publish(message);
				ussdResponse.setMessage(menuProperties.getTransactionProcessing());
			}
				break;
			case WALLET_TO_OWN_ACCOUNT: {
				// WalletToOwnAccountRequest walletToOwnAccountRequest = new
				// WalletToOwnAccountRequest(transactionId,
				// amount, accountLabel, mobile, walletProvider, pin,
				// applicationProperties.getTestFlag());
				// String walletToBankJson =
				// JsonUtility.toJson(walletToOwnAccountRequest);
				walletToBankPublisher.publish(message);
				ussdResponse.setMessage(menuProperties.getTransactionProcessing());
			}
				break;
			case WALLET_TO_OTHER_ACCOUNT: {
				walletToBankPublisher.publish(message);
				ussdResponse.setMessage(menuProperties.getTransactionProcessing());
			}
				break;
			}
			return ussdResponse;
		} else {
			ussdResponse.setMessage(menuProperties.getIncorrectPin());
			return ussdResponse;
		}
	}
}
