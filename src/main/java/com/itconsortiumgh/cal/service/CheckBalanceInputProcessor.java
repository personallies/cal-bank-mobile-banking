package com.itconsortiumgh.cal.service;


import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.itconsortiumgh.cal.model.ClientState;
import com.itconsortiumgh.cal.model.CustomerAccountsContainer;
import com.itconsortiumgh.cal.model.MessageType;
import com.itconsortiumgh.cal.model.TransactionType;
import com.itconsortiumgh.cal.model.UssdResponse;
import com.itconsortiumgh.cal.model.UssdSession;
import com.itconsortiumgh.cal.model.logs.TransactionLogs;
import com.itconsortiumgh.cal.redis.queue.BalancePublisher;
import com.itconsortiumgh.cal.repository.CustomerRepository;
import com.itconsortiumgh.cal.repository.redis.RedisUssdSessionRepository;
import com.itconsortiumgh.cal.utils.GeneralUtils;
import com.itconsortiumgh.cal.utils.JsonUtility;
import com.itconsortiumgh.cal.utils.TransactionIdGenerator;
import com.itconsortiumgh.cal.utils.properties.ApplicationProperties;
import com.itconsortiumgh.cal.utils.properties.MenuProperties;

import lombok.extern.slf4j.Slf4j;
@Service
@Slf4j
public class CheckBalanceInputProcessor {
	@Autowired
	MenuProperties menuProperties;
	@Autowired
	CustomerServices subscriberServices;
	@Autowired
	CustomerRepository subscriberRepository;
	@Autowired
	RedisUssdSessionRepository redisUssdSessionRepository;
	@Autowired
	CoreBankingFacade coreBankingFacade;
	@Autowired
	CustomerServices customerServices;
	@Autowired
	GeneralUtils generalUtils;
	@Autowired
	CustomerRepository customerRepository;
	@Autowired
	ApplicationProperties applicationProperties;
	@Autowired
	BalancePublisher balancePublisher;

	public UssdResponse askForChooseAccount(UssdSession ussdSession,String mobileNumber)  {
		UssdResponse ussdResponse = new UssdResponse();
		ussdResponse = generalUtils.selectAcountLabel(mobileNumber, ussdSession);
		ussdSession.setMsisdn(mobileNumber);
		redisUssdSessionRepository.put(ussdSession);
		return ussdResponse;

	}

	public UssdResponse askForPin(UssdSession ussdSession,String userInput,String mobileNumber){
		UssdResponse ussdResponse = new UssdResponse();
		CustomerAccountsContainer customerAccountsContainer = customerServices
				.retrieveCustomerAccounts(mobileNumber);
		Map<String, String> accountsMap = customerAccountsContainer.getAccountsMap();
		String accountLabel = accountsMap.get(userInput);
		log.info("label  >>> {}", accountLabel);
		ussdSession.setDebitAccountLabel(accountLabel);

		ussdResponse.setType(MessageType.RESPONSE);
		ussdResponse.setMessage(menuProperties.getEnterPin());
		ussdResponse.setClientState(ClientState.ENTER_PIN);
		ussdSession.setClientState(ClientState.ENTER_PIN);
		redisUssdSessionRepository.put(ussdSession);
		return ussdResponse;
	}


	public UssdResponse checkPin(UssdSession ussdSession,String userInput){
		UssdResponse ussdResponse = new UssdResponse();
		ussdResponse.setClientState(ClientState.FINAL);
		ussdResponse.setType(MessageType.RELEASE);
		String transactionId = TransactionIdGenerator.nextId().toString();
		ussdSession.setTransactionId(transactionId);
		ussdSession.setTransactionType(TransactionType.BALANCE);
		Boolean isPinValid=true;
		if(isPinValid)//customerServices.isPinValid(userInput, ussdSession.getMsisdn()) {
			{
			//TransactionLogs transactionLogs = new TransactionLogs(ussdSession);
			ussdSession.setPin(userInput);
			String message = JsonUtility.toJson(ussdSession);
			log.info("about to publish balance request {}", message);
			balancePublisher.publish(message);
			log.info("balance request published");
			ussdResponse.setMessage(menuProperties.getProcessingRequest());
			return ussdResponse;
		}
		else{ 
			ussdResponse.setMessage(menuProperties.getIncorrectPin());
			return ussdResponse;
		}
	}


	//	HttpResponse<String> response = Unirest.get("https://api.smsgh.com/v3/messages/send?From=CAL%20BANK&To=233243576745&Content=The%20balance%20on%20your%20account%20is%20&ClientId=lisdkzep&ClientSecret=gamxmpcm")
	//			.header("cache-control", "no-cache")
	//			.header("postman-token", "7bfbc9d4-820d-8804-875f-e5daaf4c9a16")
	//			.asString();



}


