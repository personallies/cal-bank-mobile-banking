package com.itconsortiumgh.cal.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.itconsortiumgh.cal.model.ClientState;
import com.itconsortiumgh.cal.model.UssdRequest;
import com.itconsortiumgh.cal.model.UssdResponse;
import com.itconsortiumgh.cal.model.UssdSession;

@Service
public class BillPaymentBusinessLogic {
	@Autowired
	BillPaymentUserInputProcessor billPaymentUserInputProcessor;
	
	public UssdResponse processRequest(UssdRequest ussdRequest, UssdSession ussdSession){
		UssdResponse ussdResponse = new UssdResponse();
		String mobileNumber = ussdRequest.getMobile();
		String userInput = ussdRequest.getMessage();
		String clientState = ussdRequest.getClientState();
		
		switch(clientState){
		case ClientState.ROOT:
			ussdResponse = billPaymentUserInputProcessor.askUserToChooseAccount(ussdSession, ussdRequest, mobileNumber);
			return ussdResponse;
		case ClientState.CHOOSE_ACCOUNT:
			ussdResponse = billPaymentUserInputProcessor.askUserToChooseMerchant(ussdSession, userInput, mobileNumber);
			return ussdResponse;
		case ClientState.CHOOSE_MERCHANT:
			ussdResponse = billPaymentUserInputProcessor.askUserToEnterCustomerNumber(ussdSession, userInput);
			return ussdResponse;
		case ClientState.ENTER_CUSTOMER:
			ussdResponse = billPaymentUserInputProcessor.displayCustomerNumber(ussdSession, userInput);
			return ussdResponse;
		case ClientState.DISPLAY_CUSTOMER:
			ussdResponse = billPaymentUserInputProcessor.askUserToEnterAmount(ussdSession);
			return ussdResponse;
		case ClientState.CHOOSE_INVOICE_PERIOD:
			ussdResponse=billPaymentUserInputProcessor.askUserForInvoicePeriod(ussdSession, userInput);
			return ussdResponse;
		case ClientState.ENTER_AMOUNT:
			ussdResponse = billPaymentUserInputProcessor.displayConfirmMessage(ussdSession, userInput, mobileNumber);
			return ussdResponse;
			
		case ClientState.CONFIRM_MESSAGE:
			ussdResponse = billPaymentUserInputProcessor.askForPin(ussdSession, userInput);
			return ussdResponse;
		case ClientState.ENTER_PIN:
			ussdResponse = billPaymentUserInputProcessor.checkPin(ussdSession,userInput);
			return ussdResponse;
		}
		return ussdResponse;
	}

}
