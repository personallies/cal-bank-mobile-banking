package com.itconsortiumgh.cal.service;

import java.math.BigDecimal;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.itconsortiumgh.cal.model.AirtimePurchaseRequest;
import com.itconsortiumgh.cal.model.ResponseCode;
import com.itconsortiumgh.cal.model.TransactionResponse;
import com.itconsortiumgh.cal.utils.JsonUtility;
import com.itconsortiumgh.cal.utils.properties.ApplicationProperties;
import com.itconsortiumgh.cal.utils.properties.BrokerProperties;
//import com.itconsortiumgh.cal.utils.properties.BrokerProperties;
//import com.smsgh.AirtimeNetwork;
//import com.smsgh.ApiHost;
//import com.smsgh.BasicAuth;
//import com.smsgh.BrokerApi;
//import com.smsgh.BrokerResponse;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import lombok.extern.slf4j.Slf4j;
@Service
@Slf4j
public class BrokerApiFacade {
	public static void main(String []args){
		BrokerApiFacade facade = new BrokerApiFacade();
		AirtimePurchaseRequest request = new AirtimePurchaseRequest();
		request.setAirtimeNetwork(com.itconsortiumgh.cal.model.AirtimeNetwork.Glo);
		request.setAmount(new BigDecimal("0.5"));
		request.setMobile("0243576745");
//		request.setApiToken("0624d0ca-e124-48a3-8e75-8502b9c99e1e");
//		request.setClientId("lisdkzep");
//		request.setClientSecret("gamxmpcm");
//		log.info("logging response {}", facade.purchaseAirtime(request));
	}
//	
	@Autowired
	ApplicationProperties applicationProperties;
	@Autowired
	BrokerProperties brokerProperties;


	
	public TransactionResponse purchaseAirtime(AirtimePurchaseRequest airtimePurchaseRequest)
	{
		TransactionResponse transactionResponse=new TransactionResponse();

		OkHttpClient client = new OkHttpClient();
		String message = "{\"phone\":\""+airtimePurchaseRequest.getMobile()+"\", "
				+ "\"amount\":"+airtimePurchaseRequest.getAmount().toPlainString()+",  "
				+ "\"token\":\""+brokerProperties.getApiToken()+"\",  "
				+ "\"network\":\""+airtimePurchaseRequest.getAirtimeNetwork().toString()+"\"}";
		log.info("SMS PURCHASE REQUEST =========== {}", message);
		MediaType mediaType = MediaType.parse("application/json");
		RequestBody body = RequestBody.create(mediaType, message);
		Request request = new Request.Builder()
				.url(brokerProperties.getAirtimeEndpoint())
				.post(body)
				.addHeader("accept", "application/json")
				.addHeader("content-type", "application/json")
				.addHeader("authorization", "Basic bGlzZGt6ZXA6Z2FteG1wY20=")
				.addHeader("cache-control", "no-cache")
				.addHeader("postman-token", "e8472c5e-7965-4d57-f4f9-2dfb31369594")
				.build();
            
		Response response =null;
		try {
			response = client.newCall(request).execute();
			String responseString = response.body().string();
			log.info("responseString {}",responseString);
			Map<String, String>map= JsonUtility.jsonToMapKeyAsString(responseString);
			String errorCode = map.get("ErrorCode");
			String errorDescription = map.get("Description");
			String thirdPartyTransactionId = map.get("ProviderId");
			if(errorCode!=null){
				transactionResponse.setResponseCode(ResponseCode.CODE_100_GENERAL_FAILURE);
				transactionResponse.setResponseMessage(errorCode+": "+errorDescription);
				transactionResponse.setThirdPartyTransactionId(thirdPartyTransactionId);
			}else{
				transactionResponse.setResponseCode(ResponseCode.CODE_01_OK);
				transactionResponse.setResponseMessage("success");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return transactionResponse;
	}
}