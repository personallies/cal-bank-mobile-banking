package com.itconsortiumgh.cal.service;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.itconsortiumgh.cal.model.ClientState;
import com.itconsortiumgh.cal.model.CustomerAccountsContainer;
import com.itconsortiumgh.cal.model.FundsTransferOperation;
import com.itconsortiumgh.cal.model.MessageType;
import com.itconsortiumgh.cal.model.TransactionType;
import com.itconsortiumgh.cal.model.UssdResponse;
import com.itconsortiumgh.cal.model.UssdSession;
import com.itconsortiumgh.cal.redis.queue.FundsTransferPublisher;
import com.itconsortiumgh.cal.repository.redis.RedisUssdSessionRepository;
import com.itconsortiumgh.cal.utils.JsonUtility;
import com.itconsortiumgh.cal.utils.properties.ApplicationProperties;
import com.itconsortiumgh.cal.utils.properties.MenuProperties;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class FundsTransferUserInputProcessor {

	@Autowired
	MenuProperties menuProperties;
	@Autowired
	ApplicationProperties applicationProperties;
	@Autowired
	RedisUssdSessionRepository redisUssdSessionRepository;
	@Autowired
	CustomerServices customerServices;
	@Autowired
	FundsTransferPublisher transferPublisher;

	public UssdResponse askUserToChooseAccount(UssdSession ussdSession, String mobileNumber) {
		CustomerAccountsContainer customerAccountsContainer = customerServices.retrieveCustomerAccounts(mobileNumber);
		String message = menuProperties.getSelectAccount() + "\n" + customerAccountsContainer.getAccountsMenu();
		Map<String, String> accountsMap = customerAccountsContainer.getAccountsMap();

		UssdResponse ussdResponse = new UssdResponse();
		if (accountsMap.size() == 1) {
			ussdResponse.setType(MessageType.RESPONSE);
			ussdResponse.setClientState(ClientState.ENTER_DESTINATION_ACCOUNT);
			ussdResponse.setMessage(menuProperties.getEnterDestinationAccount());

			ussdSession.setFundsTransferOperation(FundsTransferOperation.TRANSFER_FOR_OTHER);
			ussdSession.setDebitAccountLabel(accountsMap.get("1"));
			ussdSession.setClientState(ClientState.ENTER_DESTINATION_ACCOUNT);
			redisUssdSessionRepository.put(ussdSession);
			return ussdResponse;
		} else if (accountsMap.size() > 1) {
			String accountMapJson = JsonUtility.mapToJson(customerAccountsContainer.getAccountsMap());

			ussdResponse.setMessage(message);
			ussdResponse.setType(MessageType.RESPONSE);
			ussdResponse.setClientState(ClientState.CHOOSE_ACCOUNT);

			ussdSession.setMsisdn(mobileNumber);
			ussdSession.setClientState(ClientState.CHOOSE_ACCOUNT);
			ussdSession.setAccountLabelMapJson(accountMapJson);
			redisUssdSessionRepository.put(ussdSession);
			return ussdResponse;
		}

		return ussdResponse;
	}

	public UssdResponse askUserToTransferFor(UssdSession ussdSession, String mobile, String userInput) {
		CustomerAccountsContainer customerAccountsContainer = customerServices.retrieveCustomerAccounts(mobile);
		Map<String, String> accountsMap = customerAccountsContainer.getAccountsMap();
		UssdResponse ussdResponse = new UssdResponse();

		ussdResponse.setType(MessageType.RESPONSE);
		ussdResponse.setMessage(menuProperties.getTransferTo());
		ussdResponse.setClientState(ClientState.TRANSFER_TO);

		ussdSession.setClientState(ClientState.TRANSFER_TO);
		ussdSession.setDebitAccountLabel(accountsMap.get(userInput));
		redisUssdSessionRepository.put(ussdSession);
		return ussdResponse;
	}

	public UssdResponse askUserToChooseAccount(UssdSession ussdSession, String mobileNumber, String userInput) {
		UssdResponse ussdResponse = new UssdResponse();
		CustomerAccountsContainer customerAccountsContainer = customerServices.retrieveCustomerAccounts(mobileNumber);
		String message = menuProperties.getSelectAccount() + "\n" + customerAccountsContainer.getAccountsMenu();
		Map<String, String> accountsMap = customerAccountsContainer.getAccountsMap();

		switch (new Integer(userInput)) {
		case 1:
			// At this stage the customer has multiple accounts
			String accountMapJson = JsonUtility.mapToJson(customerAccountsContainer.getAccountsMap());

			ussdResponse.setMessage(message);
			ussdResponse.setType(MessageType.RESPONSE);
			ussdResponse.setClientState(ClientState.CHOOSE_ACCOUNT);

			log.info("**********Transfer For Myself**********");
			ussdSession.setClientState(ClientState.CHOOSE_ACCOUNT);
			ussdSession.setFundsTransferOperation(FundsTransferOperation.TRANSFER_FOR_MYSELF);
			ussdSession.setAccountLabelMapJson(accountMapJson);
			redisUssdSessionRepository.put(ussdSession);
			return ussdResponse;
		case 2:
			ussdResponse.setType(MessageType.RESPONSE);
			ussdResponse.setMessage(menuProperties.getEnterDestinationAccount());
			ussdResponse.setClientState(ClientState.ENTER_DESTINATION_ACCOUNT);

			log.info("**********Transfer For Other**********");
			ussdSession.setClientState(ClientState.ENTER_DESTINATION_ACCOUNT);
			ussdSession.setFundsTransferOperation(FundsTransferOperation.TRANSFER_FOR_OTHER);
			redisUssdSessionRepository.put(ussdSession);
			return ussdResponse;
		}

		return ussdResponse;
	}

	public UssdResponse askUserForDestAccount(UssdSession ussdSession, String mobile, String userInput) {
		CustomerAccountsContainer customerAccountsContainer = customerServices.retrieveCustomerAccounts(mobile);
		Map<String, String> accountsMap = customerAccountsContainer.getAccountsMap();

		UssdResponse ussdResponse = new UssdResponse();
		ussdResponse.setType(MessageType.RESPONSE);
		ussdResponse.setMessage(menuProperties.getEnterDestinationAccount());
		ussdResponse.setClientState(ClientState.ENTER_DESTINATION_ACCOUNT);

		// String accountLabel = accountsMap.get(new Integer(userInput.trim()));

		log.info("=================================user input {}", userInput);
		log.info("=================================accountsMap {}", accountsMap);
		log.info("=============accounts Label {}", accountsMap.get(new Integer(userInput.trim())));
		ussdSession.setClientState(ClientState.ENTER_DESTINATION_ACCOUNT);
		redisUssdSessionRepository.put(ussdSession);
		return ussdResponse;
	}

	public UssdResponse askUserForAmount(UssdSession ussdSession, String userInput, String mobileNumber) {
		UssdResponse ussdResponse = new UssdResponse();
		CustomerAccountsContainer customerAccountsContainer = customerServices.retrieveCustomerAccounts(mobileNumber);
		Map<String, String> accountsMap = customerAccountsContainer.getAccountsMap();

		log.info("{}", ussdSession.getDebitAccountLabel());
		log.info("{}", ussdSession.getCreditAccountLabel());
		log.info("{}", accountsMap.get(userInput));

		if (ussdSession.getDebitAccountLabel().equalsIgnoreCase(accountsMap.get(userInput))) {
			ussdResponse.setType(MessageType.RELEASE);
			ussdResponse.setMessage(menuProperties.getSameAccountLabel());
			ussdResponse.setClientState(ClientState.FINAL);

			ussdSession.setClientState(ClientState.FINAL);
			return ussdResponse;
		} else {

			ussdResponse.setType(MessageType.RESPONSE);
			ussdResponse.setMessage(menuProperties.getEnterAmount());
			ussdResponse.setClientState(ClientState.ENTER_AMOUNT);

			if (ussdSession.getFundsTransferOperation() == FundsTransferOperation.TRANSFER_FOR_MYSELF) {
				ussdSession.setCreditAccountNumber(userInput);

			} else if (ussdSession.getFundsTransferOperation() == FundsTransferOperation.TRANSFER_FOR_OTHER) {
				ussdSession.setCreditAccountLabel(accountsMap.get(userInput));
			}

			ussdSession.setClientState(ClientState.ENTER_AMOUNT);
			redisUssdSessionRepository.put(ussdSession);
			return ussdResponse;
		}

	}

	public UssdResponse displayConfirmMessage(UssdSession ussdSession, String userInput) {
		UssdResponse ussdResponse = new UssdResponse();
		String confirmMessage = "";

		if (StringUtils.isNumeric(userInput)) {
			BigDecimal amount = new BigDecimal(userInput);
			ussdSession.setAmount(amount);
		} else {
			ussdResponse.setMessage(menuProperties.getEnterValidAmount());
			ussdResponse.setClientState(ClientState.ENTER_AMOUNT);
		}

		switch (ussdSession.getFundsTransferOperation()) {
		case TRANSFER_FOR_MYSELF:
			confirmMessage = MessageFormat.format(menuProperties.getConfirmFundTransferToSelf(),
					applicationProperties.getCurrency() + " " + ussdSession.getAmount(),
					ussdSession.getDebitAccountLabel(), ussdSession.getCreditAccountLabel());
		case TRANSFER_FOR_OTHER:
			confirmMessage = MessageFormat.format(menuProperties.getConfirmFundTransferToOther(),
					applicationProperties.getCurrency() + " " + ussdSession.getAmount(),
					ussdSession.getDebitAccountLabel(), ussdSession.getCreditAccountNumber());
		}

		ussdResponse.setType(MessageType.RESPONSE);
		ussdResponse.setMessage(confirmMessage);
		ussdResponse.setClientState(ClientState.CONFIRM_MESSAGE);

		ussdSession.setClientState(ClientState.CONFIRM_MESSAGE);

		return ussdResponse;

	}

	public UssdResponse askUserForPin(UssdSession ussdSession, String userInput) {
		UssdResponse ussdResponse = new UssdResponse();

		switch (userInput) {
		case "1": {
			ussdResponse.setType(MessageType.RESPONSE);
			ussdResponse.setMessage(menuProperties.getEnterPin());
			ussdResponse.setClientState(ClientState.ENTER_PIN);

			ussdSession.setClientState(ClientState.ENTER_PIN);
			redisUssdSessionRepository.put(ussdSession);
			return ussdResponse;
		}
		case "2": {
			ussdResponse.setType(MessageType.RELEASE);
			ussdResponse.setMessage(menuProperties.getCancelMessage());
			ussdResponse.setClientState(ClientState.FINAL);

			ussdSession.setClientState(ClientState.FINAL);
			return ussdResponse;
		}

		}
		return ussdResponse;
	}

	public UssdResponse checkPin(UssdSession ussdSession, String userInput) {
		UssdResponse ussdResponse = new UssdResponse();
		ussdResponse.setClientState(ClientState.FINAL);
		ussdResponse.setType(MessageType.RELEASE);
		Boolean isPinValid = true;
		// Boolean isPinValid = customerServices.isPinValid(userInput,
		// ussdSession.getMsisdn());
		if (isPinValid) {
			ussdSession.setTransactionType(TransactionType.TRANSFER);
			String message = JsonUtility.toJson(ussdSession);
			transferPublisher.publish(message);
			ussdSession.setPin(userInput);
			ussdResponse.setMessage(menuProperties.getProcessingRequest());
			return ussdResponse;
		} else {
			ussdResponse.setMessage(menuProperties.getIncorrectPin());
			return ussdResponse;
		}
	}

}
