package com.itconsortiumgh.cal.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.itconsortiumgh.cal.model.ClientState;
import com.itconsortiumgh.cal.model.FundsTransferOperation;
import com.itconsortiumgh.cal.model.SessionStatus;
import com.itconsortiumgh.cal.model.TransactionType;
import com.itconsortiumgh.cal.model.UssdRequest;
import com.itconsortiumgh.cal.model.UssdResponse;
import com.itconsortiumgh.cal.model.UssdSession;
import com.itconsortiumgh.cal.repository.redis.RedisUssdSessionRepository;
import com.itconsortiumgh.cal.utils.properties.MenuProperties;

@Service
public class FundsTransferBusinessLogic {

	@Autowired
	FundsTransferUserInputProcessor fundsTransferUserInputProcessor;
	@Autowired
	RedisUssdSessionRepository redisUssdSessionRepository;
	@Autowired
	MenuProperties menuProperties;

	public UssdResponse processRequest(UssdSession ussdSession, UssdRequest ussdRequest) {
		UssdResponse ussdResponse = new UssdResponse();
		String clientState = ussdRequest.getClientState();
		String userInput = ussdRequest.getMessage();
		String mobileNumber = ussdRequest.getMobile();

		switch (clientState) {
		case ClientState.ROOT:
			ussdResponse = fundsTransferUserInputProcessor.askUserToChooseAccount(ussdSession, mobileNumber);
			return ussdResponse;
		case ClientState.CHOOSE_ACCOUNT:
			if(FundsTransferOperation.TRANSFER_FOR_MYSELF.equals(ussdSession.getFundsTransferOperation())){
				ussdResponse = fundsTransferUserInputProcessor.askUserForAmount(ussdSession, userInput, mobileNumber);
				return ussdResponse;
			}else{
				ussdResponse = fundsTransferUserInputProcessor.askUserToTransferFor(ussdSession, mobileNumber, userInput);
				return ussdResponse;
			}
		case ClientState.TRANSFER_TO:
			ussdResponse = fundsTransferUserInputProcessor.askUserToChooseAccount(ussdSession, mobileNumber, userInput);
			return ussdResponse;
		case ClientState.ENTER_DESTINATION_ACCOUNT:
			ussdResponse = fundsTransferUserInputProcessor.askUserForAmount(ussdSession, userInput, mobileNumber);
			return ussdResponse;
		case ClientState.ENTER_AMOUNT:
			ussdResponse = fundsTransferUserInputProcessor.displayConfirmMessage(ussdSession, userInput);
			return ussdResponse;
		case ClientState.CONFIRM_MESSAGE:
			ussdResponse = fundsTransferUserInputProcessor.askUserForPin(ussdSession, userInput);
			return ussdResponse;
		case ClientState.ENTER_PIN:
			ussdResponse = fundsTransferUserInputProcessor.checkPin(ussdSession, userInput);
			return ussdResponse;
		default:
			ussdResponse.setMessage(menuProperties.getDefaultMessage());
		}

		return ussdResponse;
	}

}
