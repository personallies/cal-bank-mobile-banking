package com.itconsortiumgh.cal.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.itconsortiumgh.cal.model.ClientState;
import com.itconsortiumgh.cal.model.UssdRequest;
import com.itconsortiumgh.cal.model.UssdResponse;
import com.itconsortiumgh.cal.model.UssdSession;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class CheckBalanceBusinessLogic {
	@Autowired
	CheckBalanceInputProcessor checkBalanceInputProcessor;

	public UssdResponse processRequest(UssdSession ussdSession, UssdRequest ussdRequest) {
		UssdResponse ussdResponse = new UssdResponse();
		String mobileNumber = ussdRequest.getMobile();
		String userInput = ussdRequest.getMessage();

		String clientState = ussdRequest.getClientState();
		switch (clientState) {
		case ClientState.ROOT:
			ussdResponse = checkBalanceInputProcessor.askForChooseAccount(ussdSession, mobileNumber);
			return ussdResponse;

		case ClientState.CHOOSE_ACCOUNT:
			ussdResponse = checkBalanceInputProcessor.askForPin(ussdSession, userInput, mobileNumber);
			return ussdResponse;

		case ClientState.ENTER_PIN:
			ussdResponse = checkBalanceInputProcessor.checkPin(ussdSession, userInput);
			return ussdResponse;

		}

		return ussdResponse;
	}
}
